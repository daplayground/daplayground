# Import / Export

Only import/export an array of VSMs without the steps array.
These VSM objects should be easily writable by hand.

**Example:**
```json
{
  "type": "b+tree",
  "vizStateMutations": [
      {
        "type": "create",
        "size": 10,
        "steps": []
      },
      {
        "type": "insert",
        "value": 4,
        "steps": []
      }
  ]
}
```
