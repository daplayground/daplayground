let basePath = '/docs/';

if (process.env.PUBLIC_PATH) {
  basePath = `${process.env.PUBLIC_PATH}docs/`;
}

module.exports = {
  themeConfig: {
    logo: '/assets/img/favicon.png',
    repo: 'https://gitlab.com/daplayground/daplayground',
    docsDir: 'docs',
    editLinks: true,
    editLinkText: 'Help us improve this page!',
    lastUpdated: 'Last Updated',
    nextLinks: true,
    prevLinks: true,
    sidebar: [
      '/',
      '/algorithms-and-data-structures.md',
      '/visualization-and-animation.md',
      '/import-export.md',
      '/offline-installation.md',
    ],
    sidebarDepth: 1,
    displayAllHeaders: true,
  },
  base: basePath,
  title: 'Docs - DA Playground',
  head: [
    ['link', { rel: 'icon', href: '../favicon.ico' }]
  ],
  markdown: {
    lineNumbers: true,
  },
};
