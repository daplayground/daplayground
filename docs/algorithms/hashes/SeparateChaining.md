# Separate Chaining

The elements are distributed in the hash table accoring to a modulo (%) operation between an element and the table size.

The collision resolution allocates colliding elements into an array of buckets. In case of a dynamic hash table, a
treshold fill factor of 70% shall not be exceeded. If however this is the case, the table doubles in size and
reallocates the elements.

[Wikipedia](https://en.wikipedia.org/wiki/Hash_table#Separate_chaining)
