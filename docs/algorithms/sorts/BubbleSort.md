# BubbleSort

Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the
list, compares adjacent elements and swaps them if they are in the wrong order. The pass through the list is repeated
until the list is sorted. The algorithm, which is a comparison sort, is named for the way smaller or larger elements
"bubble" to the top of the list. [Wikipedia](https://en.wikipedia.org/wiki/Bubble_sort)

## Performance

Bubble sort has a worst-case and average complexity of О(n2), where n is the number of items being sorted. Most
practical sorting algorithms have substantially better worst-case or average complexity, often O(n log n). Even other
О(n2) sorting algorithms, such as insertion sort, generally run faster than bubble sort, and are no more complex.
Therefore, bubble sort is not a practical sorting algorithm.

The only significant advantage that bubble sort has over most other algorithms, even quicksort, but not insertion sort,
is that the ability to detect that the list is sorted efficiently is built into the algorithm. When the list is already
sorted (best-case), the complexity of bubble sort is only O(n). By contrast, most other algorithms, even those with
better average-case complexity, perform their entire sorting process on the set and thus are more complex. However, not
only does insertion sort share this advantage, but it also performs better on a list that is substantially sorted
(having a small number of inversions).

Bubble sort should be avoided in the case of large collections. It will not be efficient in the case of a
reverse-ordered collection. [Wikipedia](https://en.wikipedia.org/wiki/Bubble_sort)

## Legend

Actual values are marked green, when they do not need to be sorted (1), otherwise red (2).
Sorted values are marked gold (3), all others purple (4).

## Example

![Bubble Sort Example](https://upload.wikimedia.org/wikipedia/commons/c/c8/Bubble-sort-example-300px.gif "Bubble Sort Example")

An example of bubble sort. Starting from the beginning of the list, compare every adjacent pair, swap their position if
they are not in the right order (the latter one is smaller than the former one). After each iteration, one less element
(the last one) is needed to be compared until there are no more elements left to be compared.
[Wikipedia](https://en.wikipedia.org/wiki/Bubble_sort)
