# MergeSort

Merge sort (also commonly spelled mergesort) is an efficient, general-purpose, comparison-based sorting algorithm. Most
implementations produce a stable sort, which means that the order of equal elements is the same in the input and output.
Merge sort is a divide and conquer algorithm that was invented by John von Neumann in 1945. A detailed description and
analysis of bottom-up mergesort appeared in a report by Goldstine and von Neumann as early as 1948.
[Wikipedia](https://en.wikipedia.org/wiki/Merge_sort)

## Legend

 * The current element is green (1).
 * The minimum of the run is blue (2).
 * The element, which will be swapped at the run is red (3).
 * The sorted elements are gold (4).
 * All other elements are purple (5).
