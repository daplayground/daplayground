# QuickSort

Quicksort (sometimes called partition-exchange sort) is an efficient sorting algorithm. Developed by British computer
scientist Tony Hoare in 1959 and published in 1961, it is still a commonly used algorithm for sorting. When implemented
well, it can be about two or three times faster than its main competitors, merge sort and heapsort.

Quicksort is a divide-and-conquer algorithm. It works by selecting a 'pivot' element from the array and partitioning the
other elements into two sub-arrays, according to whether they are less than or greater than the pivot. The sub-arrays
are then sorted recursively. This can be done in-place, requiring small additional amounts of memory to perform the
sorting.

Quicksort is a comparison sort, meaning that it can sort items of any type for which a "less-than" relation (formally, a
total order) is defined. Efficient implementations of Quicksort are not a stable sort, meaning that the relative order
of equal sort items is not preserved.

Mathematical analysis of quicksort shows that, on average, the algorithm takes O(n log n) comparisons to sort n items.
In the worst case, it makes O(n^2) comparisons, though this behavior is rare.
[Wikipedia](https://en.wikipedia.org/wiki/Quicksort)

## Legend

 * Elements within the sorting range smaller than pivot are blue (1), greater are red (2), equal to and the pivot itself are purple (3).
 * Sorted elements are yellow (4).
 * Elements out of sorting range are grey (5).
