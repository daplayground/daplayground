let path = '/';

if (process.env.PUBLIC_PATH) {
  path = process.env.PUBLIC_PATH;
}

module.exports = {
  publicPath: path,
  transpileDependencies: [
    'vuetify',
  ],
};
