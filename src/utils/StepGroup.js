/**
 * StepGroup
 * Represents a number of steps that can be applied to a data structure, list, or graph.
 */
export default class StepGroup {
  /**
   * @type {{}[]}
   */
  steps;

  /**
   * The type of operation this step group applies
   * @type {string}
   */
  type;

  /**
   * The value that is inserted, deleted, etc.
   * @type {number}
   */
  value;

  /**
   * A copy of the data structure after the mutation given in 'type' was executed.
   * Only in the case that type is a mutation.
   * @type {{}}
   */
  dataStructure;

  oldDataStructure;

  /**
   * @param {string} type
   */
  constructor(type) {
    this.setType(type);
  }

  /**
   * @param {{}[]} steps
   */
  setSteps(steps) {
    this.steps = steps;
  }

  /**
   * @param {string} type
   */
  setType(type) {
    this.type = type;
  }

  /**
   * @param {number} value
   */
  setValue(value) {
    this.value = value;
  }

  /**
   * @param {{}} dataStructure
   */
  setDataStructure(dataStructure) {
    this.dataStructure = dataStructure;
  }

  setOldDataStructure(dataStructure) {
    this.oldDataStructure = dataStructure;
  }
}
