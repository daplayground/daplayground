/**
 * Returns either 0 or 1, both with a probability of 50%.
 * @return {number}
 */
export default function getRandomBit() {
  return Math.random() < 0.5 ? 0 : 1;
}
