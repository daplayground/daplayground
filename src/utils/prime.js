export function isPrime(n) {
  if (n % 2 === 0) return false;
  // if not, then just check the odds
  for (let i = 3; i * i <= n; i += 2) {
    if (n % i === 0) return false;
  }
  return true;
}

export function getPreviousPrime(n) {
  let prime = n;
  while (!isPrime(prime)) {
    prime -= 1;
  }
  return prime;
}

export function getNextPrime(n) {
  let prime = n;
  while (!isPrime(prime)) {
    prime += 1;
  }
  return prime;
}
