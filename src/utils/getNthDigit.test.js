import getNthDigit from './getNthDigit';

const cases = [
  [1234567890, 1, 0],
  [1234567890, 2, 9],
  [1234567890, 3, 8],
  [1234567890, 4, 7],
  [1234567890, 5, 6],
  [1234567890, 6, 5],
  [1234567890, 7, 4],
  [1234567890, 8, 3],
  [1234567890, 9, 2],
  [1234567890, 10, 1],
];

describe('getNthDigit.js', () => {
  test.each(cases)('should return the correct digit', (number, n, expectedOutput) => {
    expect(getNthDigit(number, n)).toEqual(expectedOutput);
  });
  // todo: test different bases
});
