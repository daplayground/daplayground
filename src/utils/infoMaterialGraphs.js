import ColorCodes from "./ColorCodes";

const infoMaterialData = {
  BreadthFirstSearch: {
    description: "The concept of BFS can be explained as follows:\n\nImagine pouring ink on the starting node, which represents the initial exploration point.\nThe ink spreads across all outgoing edges from the node, indicating the exploration of neighboring nodes.\nIn a breadth-first search approach, all possible edges from the current node are examined before moving on to the next node.\nThis process continues iteratively, exploring nodes at the same level (breadth) before delving deeper into subsequent levels (depth).\nA queue data structure is often used to keep track of the nodes that need to be visited next.\nThis iterative approach follows the principle of 'first in, first out,' ensuring that nodes are explored in the order they were added to the queue.\n\nThe analogy of pouring ink and its spreading provides a visualization of the breadth-first search concept. It captures the idea of exploring the graph by examining all immediate neighbors before moving on to nodes further away, similar to gaining an overview of a book by scanning the general information, hierarchical learning approaches, or selecting a wave-like exploration strategy. The breadth-first search algorithm emphasizes exploring nodes in a breadth-first manner before moving deeper into the graph's structure.",
    colorCode: ColorCodes.GraphSearch,
    source: ["ADS lecture slides"]
    
  },
  DepthFirstSearch: {
    description : `DFS (Depth-First Search) is a graph traversal algorithm that aims to explore a graph by visiting vertices and following their edges as deeply as possible before backtracking. It operates in a depth-first manner, hence the name.
    Key points about DFS include: DFS starts from a specific node and explores as far as possible along each branch before backtracking. It utilizes a stack or recursion to keep track of the visited nodes and their exploration order. DFS is often used to traverse or search a graph, as it can efficiently visit all nodes reachable from a starting point. The algorithm can be implemented recursively or iteratively using a stack data structure. DFS can be used to solve various graph-related problems, such as finding connected components, detecting cycles, or searching for paths. Advantages of DFS include its simplicity and memory efficiency for large graphs. However, it may not guarantee the shortest path in a weighted graph, as it does not necessarily explore all possibilities. In summary, DFS explores a graph by going as deep as possible along each branch before backtracking. It uses a stack or recursion and is widely used for traversing and searching graphs, providing flexibility and efficiency in various applications.`,
    colorCode: ColorCodes.GraphSearch,
    source: ["ADS lecture slides"]
  },
  Kruskal: {
    description:
      `Kruskal's algorithm is a greedy algorithm used to find the minimum spanning tree of a connected, weighted, undirected graph. It aims to select edges with the lowest weights while avoiding the formation of cycles in the resulting tree.
      The algorithm can be summarized as follows:
      Initialize an empty set of edges, which will eventually form the minimum spanning tree.
      Sort all the edges of the graph in non-decreasing order of their weights.
      Iterate through the sorted edges and add each edge to the tree if it does not create a cycle
      To determine if adding an edge creates a cycle, a disjoint-set data structure (such as Union-Find) is commonly used to keep track of the connected components.
      Continue this process until there are V-1 edges in the minimum spanning tree, where V is the number of vertices in the graph.
      By repeatedly adding edges with the lowest weights that do not form cycles, Kruskal's algorithm constructs a minimum spanning tree, which is a subgraph that connects all vertices of the original graph with the minimum total edge weight.
      Unlike Prim's algorithm, which grows the tree from a starting vertex, Kruskal's algorithm considers all edges individually and selects them based solely on their weights, without regard to a specific starting point.`,
    colorCode: ColorCodes.GraphMinimumSpanningTree,
    source: ["ADS lecture slides"]
  },
  Prim: {
    description: `The main idea of Prim's algorithm is to find the minimum spanning tree of a weighted, connected, undirected graph. It starts with an initial vertex and grows the minimum spanning tree by iteratively adding the edge with the lowest weight that connects the current tree to a new vertex. 
    The main idea of this algorithm is as follows: Given a set A, it forms a single tree T. The safe edge that is added is always the edge (u, x) with the lowest weight, connecting T to a node u that is not yet in T. To implement this idea, the algorithm follows these steps: For each node v that is not yet in T, store in the variable key[v] the weight of the "lightest" edge that connects v to T. The node u with the minimum key[u] value is the node that is not yet in T and (among all such nodes) has the edge with the lowest weight connecting it to T. The algorithm utilizes a greedy approach, selecting the edges based on their weights. The advantage is that there is no need to sort the edges by weight.`,

    colorCode: ColorCodes.GraphMinimumSpanningTree,
    source: ["ADS lecture slides"]
  },
  Dijkstra: {
    description:
      `Dijkstra is an algorithm for finding the shortest path from a given starting node (s) to all other nodes in a graph. It assumes that the graph only has positive edge weights.
      The main idea of the algorithm is as follows: Starting from the initial node, the algorithm searches for the shortest paths to all other nodes in the graph, beginning with the shortest path to a particular node. It uses a greedy approach similar to Prim's algorithm, where the shortest path to each node is determined by continuously selecting the next longest path to all other nodes. The algorithm keeps track of the shortest paths for each corresponding node as it progresses. When all edge weights are equal (e.g., all edges have a weight of 1), the order of node visits in Dijkstra's algorithm is the same as the order in breadth-first search (BFS). Therefore, the algorithm constructs a shortest-path tree with the starting node (s) as the root.
      In summary, the algorithm iteratively finds and updates the shortest paths from the starting node to all other nodes, ultimately constructing a shortest-path tree rooted at the starting node.`,
    colorCode: ColorCodes.Dijkstra,
    source: ["ADS lecture slides"]
  },
  TopologicalSort: {
    description:
      "A topological sort is a linear ordering of a directed graph and its vertices. Every direceted Edge UV says that vertex u comes before v. Lets take the example clothing. A vertice (Shoes) would represent a step of the whole task. Edges (Socks,Shoes) will represent constraints so that every step has to be done before antother can start. In the given example it would say that you first have to put on your socks and afterwards you can put an your shoes. A directed graph has to fulfill a criteria for topological sort. The graph has to be acyclic, a so called DAG (directed acyclic graph). Any DAG contains at least one topological ordering.",
    pseudocode: "Pseudocode of Graph 6...",
    colorCode: ColorCodes.ToplogicalSort,
    source: ["https://en.wikipedia.org/wiki/Topological_sorting",]
  }
  // Define info material data for other routes
};

export default infoMaterialData;
