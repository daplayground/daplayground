/**
 * Generate a random integer between min (inclusive) and max (inclusive).
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
export default function getRandomInt(min = 0, max = 10) {
  return Math.floor(Math.random() * (max - min) + min + 1);
}
