import ColorCodes from './ColorCodes';

const infoMaterialData = {
  BinarySearchTree: {
    description: "Binary search tree (BST), sometimes also called an ordered or sorted binary tree, is a node-based binary tree data structure where each node has a comparable key (and an associated value) and satisfies the restriction that the key in any node is larger than the keys in all nodes in that node's left subtree and smaller than the keys in all nodes in that node's right sub-tree. Each node has no more than two child nodes. Each child must either be a leaf node or the root of another binary search tree. The left sub-tree contains only nodes with keys less than the parent node; the right sub-tree contains only nodes with keys greater than the parent node. BSTs are also dynamic data structures, and the size of a BST is only limited by the amount of free memory in the operating system. The main advantage of binary search trees is that it remains ordered, which provides quicker search times than many other data structures. ",
    colorCode: ColorCodes.Tree,
    source: ['http://en.wikipedia.org/wiki/Binary_search_tree'],
  },
  BPlusTreeWebAD: {
    description: `The B+ Tree is a data structure commonly used in database systems. It has several important characteristics that make it suitable for efficient storage and retrieval of data: All leaf nodes have the same depth or path length from the root. This ensures that all leaf nodes can be reached in the same number of steps, making search operations efficient.The root node is either a leaf node itself or has a minimum of two and a maximum of 2k+1 children. This ensures that the tree remains balanced and allows for efficient traversal and search operations. Each internal node has a minimum of k and a maximum of 2k keys, resulting in a minimum of k+1 and a maximum of 2k+1 children. This balancing of keys and children ensures that the tree remains balanced and allows for efficient insertion and deletion of elements. The values stored in the left subtree of a node are always smaller than the key value of the node, while the values in the right subtree are greater than or equal to the key value. This property allows for efficient search operations by narrowing down the search space based on the key values.
    The B+ Tree is particularly well-suited for applications involving large amounts of data stored in external storage devices, such as hard drives or solid-state drives. Its balanced structure and efficient search operations make it ideal for indexing and searching large datasets in databases.
    In summary, the B+ Tree is a self-balancing search tree with a balanced distribution of keys and children. It provides efficient storage and retrieval of data, making it a valuable data structure in database systems.`,
    colorCode: ColorCodes.BPlusTree,
    source: ['ADS lecture slides'],
  },
  TwoThreeFourTree: {
    description: `A 2-3-4 Tree is a multiway tree with the following properties:

    1. Size Property: Every internal node in the tree has a minimum of 2 and a maximum of 4 children. This ensures a balanced distribution of keys and children throughout the tree.
    
    2. Depth Property: All the external nodes (leaves) in the tree have the same depth. This means that the distance from the root to any leaf node is consistent, resulting in efficient search operations.
    
    A 2-3-4 Tree with n internal nodes always has a height of O(log n). This logarithmic height guarantees efficient insertion, deletion, and search operations.
    
    The 2-3-4 Tree supports data management operations such as insertion and deletion, and it can handle an unlimited amount of data. It is commonly used in main memory-oriented models, where complex operations and range queries are supported. Additionally, the tree maintains a sorted order of its elements, making it useful for sorting operations as well.
    
    Overall, the 2-3-4 Tree is a balanced and versatile data structure that enables efficient data management, complex operations, range queries, and sorting. Its balanced nature and logarithmic height make it an effective choice for various applications requiring efficient storage and retrieval of data.`,
    colorCode: ColorCodes.TwoThreeFourTree,
    source: ['ADS lecture slides'],
  },
  MinHeap: {
    description: "A binary heap is a heap data structure that takes the form of a binary tree. Binary heaps are a common way of implementing priority queues.  The binary heap was introduced by J. W. J. Williams in 1964, as a data structure for heapsort. A binary heap is defined as a binary tree with two additional constraints: Shape property: a binary heap is a complete binary tree; that is, all levels of the tree, except possibly the last one (deepest) are fully filled, and, if the last level of the tree is not complete, the nodes of that level are filled from left to right. Heap property: the key stored in each node is either greater than or equal to (≥) or less than or equal to (≤) the keys in the node's children, according to some total order.",
    pseudocode: 'Pseudocode of Graph 1...',
    colorCode: ColorCodes.Heap,
    source: ['https://en.wikipedia.org/wiki/Binary_heap'],
  },
  MaxHeap: {
    description: "A binary heap is a heap data structure that takes the form of a binary tree. Binary heaps are a common way of implementing priority queues.  The binary heap was introduced by J. W. J. Williams in 1964, as a data structure for heapsort. A binary heap is defined as a binary tree with two additional constraints: Shape property: a binary heap is a complete binary tree; that is, all levels of the tree, except possibly the last one (deepest) are fully filled, and, if the last level of the tree is not complete, the nodes of that level are filled from left to right. Heap property: the key stored in each node is either greater than or equal to (≥) or less than or equal to (≤) the keys in the node's children, according to some total order.",
    colorCode: ColorCodes.Heap,
    source: ['https://en.wikipedia.org/wiki/Binary_heap'],
  },
  PrefixTree: {
    description: 'In computer science, a trie, also called digital tree and sometimes radix tree or prefix tree (as they can be searched by prefixes), is an ordered tree data structure that is used to store a dynamic set or associative array where the keys are usually strings. Unlike a binary search tree, no node in the tree stores the key associated with that node; instead, its position in the tree defines the key with which it is associated. All the descendants of a node have a common prefix of the string associated with that node, and the root is associated with the empty string. Values are not necessarily associated with every node. Rather, values tend only to be associated with leaves, and with some inner nodes that correspond to keys of interest.',
    colorCode: ColorCodes.PrefixTree,
    source:  ['https://en.wikipedia.org/wiki/Trie'],
  },

};

export default infoMaterialData;
