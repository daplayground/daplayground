/**
 * The n-th digit of a number can be obtained by the formula (number / base^(n-1)) % base
 * @param {number} number
 * @param {number} n The position of the digit that should be retrieved, from the right.
 * @param {number} base The base in which the calculation should be made
 * @return {number}
 */
export default function getNthDigit(number, n, base = 10) {
  return Math.trunc((number / (base ** (n - 1))) % base);
}
