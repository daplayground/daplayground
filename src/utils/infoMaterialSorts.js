import ColorCodes from "./ColorCodes";

const infoMaterialData = {
  BogoSort: {
    description:
      "In computer science, bogosort (also known as permutation sort, stupid sort, slowsort or bozosort) is a sorting algorithm based on the generate and test paradigm. The function successively generates permutations of its input until it finds one that is sorted. It is not considered useful for sorting, but may be used for educational purposes, to contrast it with more efficient algorithms.",
    colorCode: ColorCodes.BogoSort,
    source: ["https://en.wikipedia.org/wiki/Bogosort"]
  },
  BubbleSort: {
    description:
      'The Bubble Sort algorithm involves repeatedly traversing the vector. If two adjacent elements are out of order (with the larger element before the smaller one), they are swapped. This process causes the largest element to "bubble" up to the last position during the first pass, the second largest element to the second-to-last position during the second pass, and so on. The elements gradually rise to their correct positions, resembling bubbles ascending. The algorithm is an in-situ sorting algorithm that operates on the main memory. It performs the sorting task using its own dedicated data area and requires only a single temporary variable. The overall time complexity of the algorithm is typically O(n^2).',
    sourcecode: `void Vector::Bubblesort()
{
    int n = Length();
    int i, j;
    for (i = n - 1; i >= 1; i--)
        for (j = 1; j <= i; j++)
            if (a[j - 1] > a[j])
                swap(a[j - 1], a[j]);
}`,
    colorCode: ColorCodes.BubbleSort,
    source: ["ADS lecture slides"]
  },
  BucketSort: {
    description: `The Bucket Sort algorithm:
    * Divide the interval [0,1) into n equally-sized sub-intervals (buckets).
    * Distribute the n input elements into their respective buckets. Since the elements are evenly distributed, only a few elements will fall into the same bucket.
    * Sort the elements within each bucket using a simple sorting algorithm such as Selection Sort.
    * Visit the buckets along the interval in ascending order and output the sorted elements.
    The Bucket Sort algorithm offers a linear time complexity of O(n). This is because distributing the elements into the buckets takes linear time, and sorting within each bucket also takes linear time (depending on the number of elements in each bucket). Since the number of buckets is usually smaller than the total number of elements, the overall runtime remains linear.
    Bucket Sort is particularly efficient when the input elements are evenly distributed across the interval. This allows for a balanced distribution of elements into the buckets, minimizing the occurrence of collisions where multiple elements fall into the same bucket.`,
    colorCode: ColorCodes.TableHorizontal,
    source: ["https://en.wikipedia.org/wiki/Bucket_sort"]
  },
  CountingSort: {
    description: `Non-comparison-based sorting algorithms can sort a sequence of inputs in linear time, achieving a complexity of O(n). Examples of such algorithms include Counting Sort, Radix Sort, and Bucket Sort.

    The condition for these algorithms is that the elements to be sorted are integers in the range of 0 to k, where k is typically greater than n. If k is of the order of n (k = O(n)), the complexity of Counting Sort becomes O(n). The approach involves creating an exact permutation where each element is placed at the position corresponding to its value in a target vector.
    
    The idea behind Counting Sort is to determine, for each element x, the number of elements smaller than x in the vector. This information is then used to place element x in its correct position in the resulting vector.
    
    As a result, the overall complexity becomes O(n + k). In practice, we often have k = O(n), leading to a real complexity of O(n). It's important to note that no comparisons between values are performed; instead, the values themselves are used for placement (similar to hashing). Counting Sort is a stable sorting algorithm, guaranteed by the last loop that works from the back to the front. It is not an in-place sorting algorithm and requires three vectors of size O(n) for its implementation.`,
    sourcecode: `
    Entryvector A, Resultvector B, Helpvector C;
for (i = 1; i <= k; i++)
    C[i] = 0;
for (j = 1; j <= length(A); j++)
    C[A[J]] = C[A[J]] + 1;
for (i = 2; i <= k; i++)
    C[i] = C[i] + C[i - 1];
for (j = length(A); j >= 1; j--)
{
    B[C[A[j]]] = A[j];
    C[A[j]] = C[A[j]] - 1;
}
    `,
    colorCode: ColorCodes.TableHorizontal,
    source: ["ADS lecture slides"]
  },
  MergeSort: {
    description:
      "In computer science, merge sort (also commonly spelled mergesort) is an O(n log n) comparison-based sorting algorithm. Most implementations produce a stable sort, which means that the implementation preserves the input order of equal elements in the sorted output. Mergesort is a divide and conquer algorithm that was invented by John von Neumann in 1945. A detailed description and analysis of bottom-up mergesort appeared in a report by Goldstine and Neumann as early as 1948.",
    pseudocode: "Pseudocode of Graph 1...",
    source: ["https://en.wikipedia.org/wiki/Merge_sort"],
  },
  QuickSort: {
    description:
      `The Quicksort algorithm (developed by Hoare in 1962) involves dividing the vector into two parts based on a chosen pivot element. All elements to the left of the pivot are smaller, and all elements to the right are larger. This process is known as "divide and conquer," as the two sub-vectors are independently sorted using the quicksort algorithm recursively. The pivot element is placed in its final position, reducing the problem size to sort the remaining n-1 elements in the two sub-vectors.

      The choice of the pivot element is arbitrary. It is "sunk" into the vector from the left and right sides, sequentially swapping it with smaller elements from the left and larger elements from the right. When a blocking occurs (a smaller element on the left and a larger element on the right), these two elements are swapped.
      
      Quicksort is a main memory algorithm and an in-place algorithm, meaning it sorts the elements within its own data area and requires only a temporary auxiliary variable (stack), resulting in constant space complexity. On average, it has a time complexity of O(n*log(n)), but it can degenerate to O(n^2) in certain cases. It is sensitive to the choice of the pivot element. Achieving stability in quicksort is not straightforward. Implementing the recursive algorithm can be complex if recursion is not available.`,
    sourcecode: `
    void Vector::Quicksort()
{
    quicksort(0, Length() - 1);
}
void Vector:: quicksort (int 1, int r) (
int i, j; int pivot;
if(r > 1) {
    pivot = a[r];
    i = 1 - 1;
    j = r;
    for (;;)
    {
        while (a[++i] < pivot)
            ;
        while (a[--j] > pivot)
            if (j == 1)
                break;
        if (i >= j)
            break;
        swap(a[i], a[j]);
    }
    swap(a[i], a[r]);
    quicksort(1, i - 1);
    quicksort(i + 1, r);
}
    `,
    colorCode: ColorCodes.Quicksort,
    source : ["ADS lecture slides"]
  },
  BinaryQuickSort: {
    description:
      `Radix Sort examines the structure of the key values. The key values, with a length of b, are represented in a number system with a base of M (where M is the radix). For example, when M = 2, the keys are represented in binary form, and b = 4. The sorting process involves comparing the individual bits of the keys at the same bit position. This principle can also be extended to alphanumeric strings.
      Binary Quicksort Algorithm:
      Consider bits from left to right.
        1. Sort the data records based on the currently considered bit.
        2. Divide the data records into M independent groups, sorted in ascending order, and recursively sort the M groups, ignoring the bits that have already been considered.
      Binary Quicksort performs approximately N log N bit comparisons on average. The log N factor represents the encoding of the numerical value and is a constant factor. N represents the uncertainty in the number representation. Binary Quicksort is well-suited for small numbers and requires relatively less memory compared to other linear sorting algorithms. It is an unstable sorting algorithm, similar to Quicksort, but applicable only to positive integers.`,
    pseudocode: "Pseudocode of Graph 1...",
    colorCode: ColorCodes.TableHorizontal,
    source: ["ADS lecture slides"]
  },
  RadixSort: {
    description:
      `LSD-Radixsort Algorithm:
      - Consider bits from right to left (LSD - least significant digit).
      - Sort the data records in a stable manner based on the currently considered bit.
      
      What does "stable" mean?
      In the context of sorting algorithms, stability refers to maintaining the relative order of equal keys. A stable sorting algorithm ensures that elements with the same key value retain their original order after sorting. Stability guarantees the correctness of the algorithm. Counting Sort is a possible method to achieve stability.
      
      When applying a sorting algorithm with a runtime of O(n) (such as partitioning in Radix Exchange Sort or Counting Sort in Straight Radix Sort), the overall time complexity of Radix Sort becomes O(bn), where b is the number of bits and n is the number of elements. However, if we consider b as a constant, the time complexity reduces to O(n). In practical scenarios where the value range is limited and represented with b = log(m) bits (due to the limited representation capacity of computers), considering b as a constant is acceptable.
      LSD-Radixsort is not an in-place sorting algorithm and requires double the memory space. This leads to the practical usage of O(n log n) algorithms in such cases.`,
  
    colorCode: ColorCodes.RadixSort,
    source: ["ADS lecture slides"]
  },
  SelectionSort: {
    description:
      `Named Selection Sort or Minimum Search Algorithm. Find the smallest element in the vector and swap it with the element at the first position. Repeat this process for all elements not yet sorted. The algorithm is an in-situ sorting algorithm that operates on the main memory. It performs the sorting task using its own dedicated data area and requires only a single temporary variable. The overall time complexity of the algorithm is typically O(n^2).`,
    sourcecode: `
    void Vector:: Selectionsort () {
      int n = Length ();
      int i, j, min;
      for (i = 0; i < n; i++) {
          min = i;
          for (j = i+1; j < n; j++)
              if (a[j] < a[min]) min = j;
          swap (a[min], a[i]);
      }
  }
    `,
    colorCode: ColorCodes.SelectionSort,
    source: ["ADS lecture slides"]
  }
  // Define info material data for other routes
};

export default infoMaterialData;
