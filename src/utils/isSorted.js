/**
 * Checks if the given array is sorted in ascending order.
 *
 * @param {number[]} array
 * @returns {boolean}
 */
export default function isSorted(array) {
  return array.every((num, idx, arr) => ((num <= arr[idx + 1]) || (idx === arr.length - 1)));
}
