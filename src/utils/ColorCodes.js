import Color from './Color';

const ColorCodes = {
  // Sorting Algorithms
  BubbleSort: {
    'Base Color': Color.BASE_COLOR_BAR_CHART,
    'Comparison': Color.BUBBLE_SORT_COMPARISON,
    'Comparison True': Color.BUBBLE_SORT_COMPARISON_TRUE,
    'Comparison Wrong': Color.BUBBLE_SORT_COMPARISON_WRONG,
    'Element is sorted': Color.BUBBLE_SORT_ELEMENT_SORTED,
  },
  BogoSort: {
    'Base Color': Color.BASE_COLOR_BAR_CHART,
    'Comparison': Color.BUBBLE_SORT_COMPARISON,
    'Comparison True': Color.BUBBLE_SORT_COMPARISON_TRUE,
    'Comparison Wrong': Color.BUBBLE_SORT_COMPARISON_WRONG,
    'Element is sorted': Color.BUBBLE_SORT_ELEMENT_SORTED,
  },
  SelectionSort: {
    'Base Color': Color.BASE_COLOR_BAR_CHART,
    'Comparison Element': Color.SELECTION_SORT_COMPARISON,
    'Element to be compared with': Color.SELECTION_SORT_COMPARE_ELEMENT,
    'Elements to be swapped': Color.SELECTION_SORT_SWAP,
    'Element is sorted': Color.SELECTION_SORT_ELEMENT_SORTED,
  },
  Quicksort: {
    'Base Color': Color.BASE_COLOR_BAR_CHART,
    'Comparison': Color.QUICKSORT_COMPARISON,
    'Element is sorted': Color.QUICKSORT_ELEMENT_SORTED,
    'Elements to be swapped': Color.QUICKSORT_SWAP,
    'Pivot': Color. QUICKSORT_PIVOT,
  },
  TableHorizontal: {
    'Base Color': Color.BASE_COLOR_TABLE_HORIZONTAL,
    'Current Element': Color.TH_CURRENT_ELEMENT,
    'Element Sorted': Color.TH_ELEMENT_SORTED,
  },
  RadixSort: {
    'Base Color': Color.BASE_COLOR_BAR_CHART,
    'Current Element': Color.TH_CURRENT_ELEMENT,
    'Current Column Selected': Color.TH_CURRENT_COLUMN_SELECTED,
  },

  //Hashes
  HashTable: {
    'Base Color': Color.BASE_COLOR_HASH_TABLE,
    'Insert / Search succesful': Color.HASH_INSERT_SEARCH_COMPLETED,
    Collision: Color.HASH_COLLISION,
  },
  SeparateChaining: {
    'Base Color': Color.BASE_COLOR_HASH_TABLE,
    'Insert / Search succesful': Color.HASH_INSERT_SEARCH_COMPLETED,
  },
  LinearHashing: {
    'Base Color': Color.BASE_COLOR_HASH_TABLE,
    'Insert / Search succesful': Color.HASH_INSERT_SEARCH_COMPLETED,
    'Collision': Color.HASH_COLLISION,
  },
  ExtendibleHashing: {
    'Base Color': Color.BASE_COLOR_HASH_TABLE,
    'Insert / Search succesful': Color.HASH_INSERT_SEARCH_COMPLETED,
    'Collision': Color.HASH_COLLISION,
    'Nodes To Split': Color.HASH_COMPARE,
  },
  //Graphs
  Graph: {
    'Base Color': Color.BASE_COLOR_NODE,
    'Selected Node': Color.GRAPH_HIGHLIGHTED_NODE,
    'Current Node': Color.GRAPH_HIGHLIGHTED_NODE,
  },
  GraphSearch: {
    'Base Color': Color.BASE_COLOR_NODE,
    'Neighbour Nodes': Color.GRAPH_HIGHLIGHTED_NODE,
    'Visited Nodes': Color.GRAPH_RESULT_TRUE,
  },
  GraphMinimumSpanningTree: {
    'Base Color': Color.BASE_COLOR_NODE,
    'Minimum Spanning Tree Link': Color.GRAPH_HIGHLIGHTED_LINK,
  },
  Dijkstra: {
    'Base Color': Color.BASE_COLOR_NODE,
    'Visited Node': Color.GRAPH_HIGHLIGHTED_NODE,
    'Visited Link': Color.GRAPH_HIGHLIGHTED_LINK,
  },
  ToplogicalSort: {
    'Base Color': Color.BASE_COLOR_NODE,
    'Visited Node': Color.GRAPH_HIGHLIGHTED_NODE,
    'Visited Link': Color.GRAPH_HIGHLIGHTED_LINK,
  },
  //Trees
  Tree: {
    'Base Color': Color.BASE_COLOR_NODE,
    'Current Node Selected': Color.TREE_CURRENT_NODE_SELECTED,
    'Current Node Result True': Color.TREE_CURRENT_NODE_RESULT_TRUE,
    'Current Node Remove': Color.TREE_CURRENT_NODE_REMOVE,
  },
  PrefixTree: {
    'Base Color': Color.BASE_COLOR_TREE,
    'Current Node Selected': Color.TREE_CURRENT_NODE_SELECTED,
    'Current Node Remove': Color.TREE_CURRENT_NODE_REMOVE,
  },
  BPlusTree: {
    'Base Color': Color.BASE_COLOR_TREE,
    'Current Node Selected': Color.TREE_CURRENT_NODE_SELECTED,
    'Current Node Result True': Color.TREE_CURRENT_NODE_RESULT_TRUE,
  },
  TwoThreeFourTree: {
    'Base Color': Color.BASE_COLOR_TREE,
    'Current Node Selected': Color.TREE_CURRENT_NODE_SELECTED,
    'Current Node Result True': Color.TREE_CURRENT_NODE_RESULT_TRUE,
  },
  Heap: {
    'Base Color': Color.BASE_COLOR_NODE,
  },
};

export default ColorCodes;
