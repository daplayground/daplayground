/**
 * Fisher–Yates shuffle
 *
 * @param {number[]} elements
 * @returns {number[]}
 */
export default function shuffle(elements) {
  const randomElements = Array.from(elements);

  let m = randomElements.length;
  let t;
  let i;

  // While there remain elements to shuffle...
  while (m) {
    // Pick a remaining element...
    i = Math.floor(Math.random() * m);
    m -= 1;

    // And swap it with the current element.
    t = randomElements[m];
    randomElements[m] = randomElements[i];
    randomElements[i] = t;
  }

  return randomElements;
}
