import isSorted from './isSorted';

describe('isSorted.js', () => {
  test.each([
    // No input
    [[], true],
    // Sorted input
    [[1, 2, 3, 4, 5], true],
    // Reverse input
    [[5, 4, 3, 2, 1], false],
    // Only one number
    [[4], true],
    // Repeating numbers
    [[1, 2, 1], false],
    // Random input
    [[3, 8, 2, 7, 1], false],
  ])('should tell correctly if the input is sorted or not', (input, expected) => {
    expect(isSorted(input)).toBe(expected);
  });
});
