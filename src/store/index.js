import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import AnimationModule from './AnimationModule';
import InputModule from './InputModule';
import CurrentValuesModule from './CurrentValuesModule';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    AnimationModule,
    InputModule,
    CurrentValuesModule,
  },
  plugins: [
    // createPersistedState(),
  ],
});
