import ValueWrapper from '../utils/ValueWrapper';

export default {
  state: {
    valueType: '',
    values: [],
    k: 0,
    cellar: 0,
    index: 0,
    entry: 0,
    bucket: 0,
    block: 0,
  },
  mutations: {
    setValueType(state, valueType) {
      state.valueType = valueType;
    },
    addValue(state, value) {
      state.values.push(value);
    },
    removeValue(state, value) {
      const index = state.values.indexOf(value);
      if (index > -1) {
        state.values.splice(index, 1);
      }
    },
    setK(state, value) {
      state.k = value;
    },
    setCellar(state, value) {
      state.cellar = value;
    },
    clearValues(state) {
      state.values = [];
    },
    setCurrentValues(state, valueWrapper) {
      state.valueType = valueWrapper.type;
      state.values = valueWrapper.values;
      state.k = valueWrapper.k;
      state.cellar = valueWrapper.cellar;
      state.index = valueWrapper.index;
      state.block = valueWrapper.block;
      state.entry = valueWrapper.entry;
      state.bucket = valueWrapper.bucket;
    },
    clearCurrentValues(state) {
      state.valueType = '';
      state.values = [];
      state.k = 0;
      state.cellar = 0;
      state.index = 0;
      state.entry = 0;
      state.block = 0;
      state.bucket =0;
    },
    setValueList(state, values) {
      state.values = values;
    },
    setIndex(state, value) {
      state.index = value;
    },
    setEntry(state, value) {
      state.entry = value;
    },
    setBucket(state, value) {
      state.bucket = value;
    },
  },
  actions: {
    clearCurrentValues({commit}) {
      commit('clearCurrentValues');
    },
  },
  getters: {
    currentValue: (state) => {
      const valueWrapper = new ValueWrapper();
      valueWrapper.setValues(state.values);
      valueWrapper.setType(state.valueType);
      valueWrapper.setK(state.k);
      valueWrapper.setCellar(state.cellar);
      valueWrapper.setIndex(state.index);
      valueWrapper.setEntry(state.entry);
      valueWrapper.setBlock(state.block)
      valueWrapper.setBucket(state.bucket);
      return valueWrapper;
    },
  },
};
