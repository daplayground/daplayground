/**
 * AnimationModule
 * Store related to tape recorder etc.
 */
export default {
  state: {
    animationIntervalId: null,
    vizStateMutations: [],
    rawAnimationSpeed: 1,
    autoPlay: {
      onCreate: true,
      onInsert: true,
      onSearch: true,
      onRemove: true,
      onTraverse: true,
    },
  },
  mutations: {
    setAnimationIntervalId(state, animationIntervalId) {
      state.animationIntervalId = animationIntervalId;
    },
    addVizStateMutation(state, vizStateMutation) {
      state.vizStateMutations.push(vizStateMutation);
    },
    clearVizStateMutations(state) {
      state.vizStateMutations.length = 0;
    },
    setRawAnimationSpeed(state, rawAnimationSpeed) {
      state.rawAnimationSpeed = rawAnimationSpeed;
    },
    setAutoPlay(state, autoPlay) {
      state.autoPlay = autoPlay;
    },
  },
  actions: {
    setAnimationIntervalId({ commit }, animationIntervalId) {
      commit('setAnimationIntervalId', animationIntervalId);
    },
    addVizStateMutation({ commit }, vizStateMutation) {
      commit('addVizStateMutation', vizStateMutation);
    },
    clearVizStateMutations({ commit }) {
      commit('clearVizStateMutations');
    },
    setRawAnimationSpeed({ commit }, rawAnimationSpeed) {
      commit('setRawAnimationSpeed', rawAnimationSpeed);
    },
    resetRawAnimationSpeed({ commit }) {
      commit('setRawAnimationSpeed', 1);
    },
    setAutoPlay({ commit }, autoPlay) {
      commit('setAutoPlay', autoPlay);
    },
  },
  getters: {
    animationIntervalId: (state) => state.animationIntervalId,
    lastVizStateMutation: (state) => state.vizStateMutations[state.vizStateMutations.length - 1],
    animationSpeed: (state) => 2 ** state.rawAnimationSpeed,
    autoPlay: (state) => state.autoPlay,
  },
};
