import Action from '../../components/sorts/BarChartActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm SelectionSort
 *
 * Implementation based on pseudo code from Wikipedia.
 *
 * @author Bernhard Frick
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function SelectionSort(elements, testing = false) {
  const steps = [];

  const output = Array.from(elements);

  if (output.length === 0) {
    return testing ? output : steps;
  }

  // Step over insert positions
  for (let insertPos = 0; insertPos < output.length; insertPos += 1) {
    // highlight the first insert position
    steps.push({
      do: [{
        action: Action.SET_COLORS,
        elements: [
          {
            index: insertPos,
            color: Color.SELECTION_SORT_COMPARE_ELEMENT,
          },
        ],
        text: `Selecting ${insertPos} as insert position.`,
      }],
      undo: [{
        action: Action.SET_COLORS,
        elements: [
          {
            index: insertPos,
            color: Color.BASE_COLOR_BAR_CHART,
          },
        ],
        text: `Selecting ${insertPos} as insert position.`,
      }],
    });
    // need to find smallest element in remaining list
    let minPos = insertPos;
    // set the current minimum
    steps.push({
      do: [{
        action: Action.SET_LABELS,
        elements: [
          {
            index: minPos,
            text: 'min',
          },
        ],
        text: `Setting ${minPos} as the current minimum.`,
      }],
      undo: [{
        action: Action.SET_LABELS,
        elements: [
          {
            index: minPos,
            text: null,
          },
        ],
        text: `Setting ${minPos} as the current minimum.`,
      }],
    });
    for (let comparePos = insertPos + 1; comparePos < output.length; comparePos += 1) {
      // highlight the element to compare
      steps.push({
        do: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: comparePos,
              color: Color.SELECTION_SORT_COMPARISON,
            },
          ],
          text: `Comparing elements ${comparePos} and ${minPos}.`,
        }],
        undo: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: comparePos,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: `Comparing elements ${comparePos} and ${minPos}.`,
        }],
      });
      if (output[comparePos] < output[minPos]) {
        const previousMinPos = minPos;
        minPos = comparePos;
        // found a new min
        steps.push({
          do: [{
            action: Action.SET_LABELS,
            elements: [
              {
                index: minPos,
                text: 'min',
              },
              {
                index: previousMinPos,
                text: null,
              },
            ],
            text: `Setting ${minPos} as the new minimum.`,
          }],
          undo: [{
            action: Action.SET_LABELS,
            elements: [
              {
                index: minPos,
                text: null,
              },
              {
                index: previousMinPos,
                text: 'min',
              },
            ],
            text: `Setting ${minPos} as the new minimum.`,
          }],
        });
        // clear the highlights of the compared elements.
        steps.push({
          do: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: comparePos,
                color: Color.BASE_COLOR_BAR_CHART,
              },
              {
                index: minPos,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: 'Finished setting the new minimum.',
          }],
          undo: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: comparePos,
                color: Color.SELECTION_SORT_COMPARISON,
              },
              {
                index: minPos,
                color: Color.SELECTION_SORT_COMPARISON,
              },
            ],
            text: 'Finished setting the new minimum.',
          }],
        });
      } else {
        // did not find a new min, continue search.
        steps.push({
          do: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: comparePos,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: `Element ${minPos} is smaller than ${comparePos}, continuing search.`,
          }],
          undo: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: comparePos,
                color: Color.SELECTION_SORT_COMPARISON,
              },
            ],
            text: `Element ${minPos} is smaller than ${comparePos}, continuing search.`,
          }],
        });
      }
    }

    // remove min-label
    steps.push({
      do: [{
        action: Action.SET_LABELS,
        elements: [
          {
            index: minPos,
            text: null,
          },
        ],
        text: 'Clear minimum.',
      }],
      undo: [{
        action: Action.SET_LABELS,
        elements: [
          {
            index: minPos,
            text: 'min',
          },
        ],
        text: 'Clear minimum.',
      }],
    });

    // Exchange values
    if (minPos !== insertPos) {
      // highlight elements that should be exchanged
      steps.push({
        do: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: insertPos,
              color: Color.SELECTION_SORT_SWAP,
            },
            {
              index: minPos,
              color: Color.SELECTION_SORT_SWAP,
            },
          ],
          text: `Elements ${insertPos} and ${minPos} will be exchanged.`,
        }],
        undo: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: insertPos,
              color: Color.SELECTION_SORT_COMPARE_ELEMENT,
            },
            {
              index: minPos,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: `Elements ${insertPos} and ${minPos} will be exchanged.`,
        }],
      });

      // exchange elements
      steps.push({
        do: [{
          action: Action.EXCHANGE,
          elements: [insertPos, minPos],
          text: `Moving minimum element ${minPos} to ${insertPos}.`,
        }],
        undo: [{
          action: Action.EXCHANGE,
          elements: [insertPos, minPos],
          text: `Moving minimum element ${minPos} to ${insertPos}.`,
        }],
      });

      const tmp = output[insertPos];
      output[insertPos] = output[minPos];
      output[minPos] = tmp;

      // set inserted element as sorted, reset other element
      steps.push({
        do: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: insertPos,
              color: Color.SELECTION_SORT_ELEMENT_SORTED,
            },
            {
              index: minPos,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: `Element ${insertPos} is now sorted.`,
        }],
        undo: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: insertPos,
              color: Color.SELECTION_SORT_SWAP,
            },
            {
              index: minPos,
              color: Color.SELECTION_SORT_SWAP,
            },
          ],
          text: `Element ${insertPos} is now sorted.`,
        }],
      });
    } else {
      // minPos is already sorted
      steps.push({
        do: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: insertPos,
              color: Color.SELECTION_SORT_ELEMENT_SORTED,
            },
          ],
          text: `Element ${insertPos} is now sorted.`,
        }],
        undo: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: insertPos,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: `Element ${insertPos} is now sorted.`,
        }],
      });
    }
  }

  return testing ? output : steps;
}
