/* eslint-disable no-param-reassign */

/**
 * Sorting Algorithm MergeSort
 *
 * Implementation based on pseudocode from Wikipedia.
 *
 * @author Bernhard Frick
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function MergeSort(elements, testing = false) {
  const steps = [];

  const input = Array.from(elements);

  if (input.length === 0) {
    return testing ? input : steps;
  }

  /**
   * @param {number[]} leftArr
   * @param {number[]} rightArr
   * @return {number[]}
   */
  function merge(leftArr, rightArr) {
    const sortedArr = [];

    while (leftArr.length && rightArr.length) {
      if (leftArr[0] <= rightArr[0]) {
        sortedArr.push(leftArr[0]);
        leftArr = leftArr.slice(1);
      } else {
        sortedArr.push(rightArr[0]);
        rightArr = rightArr.slice(1);
      }
    }

    while (leftArr.length) {
      sortedArr.push(leftArr.shift());
    }

    while (rightArr.length) {
      sortedArr.push(rightArr.shift());
    }

    return sortedArr;
  }

  /**
   * @param {number[]} arr
   * @return {number[]}
   */
  function mergesort(arr) {
    if (arr.length < 2) {
      return arr;
    }
    const midpoint = Math.trunc(arr.length / 2);
    const leftArr = arr.slice(0, midpoint);
    const rightArr = arr.slice(midpoint, arr.length);
    return merge(mergesort(leftArr), mergesort(rightArr));
  }

  const output = mergesort(input);

  return testing ? output : steps;
}
