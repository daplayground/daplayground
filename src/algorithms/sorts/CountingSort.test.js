import CountingSort from './CountingSort';
import getRandomInt from '../../utils/getRandomInt';
import isSorted from '../../utils/isSorted';

describe('CountingSort.js', () => {
  it('should sort the given random input', () => {
    const numTests = 1000;
    const inputSize = 50;
    for (let i = 0; i < numTests; i += 1) {
      const randomNumbers = new Array(inputSize)
        .fill(null)
        .map((_) => getRandomInt(0, 100));
      const output = CountingSort(randomNumbers, true);
      expect(isSorted(output)).toBe(true);
    }
  });
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 2, 3, 4, 5], [
      {
        do: [{ action: 'clear' }],
        undo: [],
      },
      {
        do: [
          {
            action: 'createTable', id: 'inputTable', title: 'Input', rows: 5, cols: 1,
          },
          {
            action: 'createTable', id: 'countsTable', title: 'Counts', rows: 5, cols: 2,
          },
          {
            action: 'createTable', id: 'outputTable', title: 'Output', rows: 5, cols: 1,
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'inputTable' },
          { action: 'deleteTable', id: 'countsTable' },
          { action: 'deleteTable', id: 'outputTable' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 1,
            col: 0,
            value: 2,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 2,
            col: 0,
            value: 3,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 3,
            col: 0,
            value: 4,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 4,
            col: 0,
            value: 5,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 4,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 0,
            value: 2,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 0,
            value: 3,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 0,
            value: 4,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 0,
            value: 5,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 5,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 1,
            col: 0,
            value: 2,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 1,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 2,
            col: 0,
            value: 3,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 2,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 3,
            col: 0,
            value: 4,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 3,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 4,
            col: 0,
            value: 5,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 4,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#4CAF50',
          },
        ],
      },
    ]],
    // Reverse input
    [[5, 4, 3, 2, 1], [
      {
        do: [{ action: 'clear' }],
        undo: [],
      },
      {
        do: [
          {
            action: 'createTable', id: 'inputTable', title: 'Input', rows: 5, cols: 1,
          },
          {
            action: 'createTable', id: 'countsTable', title: 'Counts', rows: 5, cols: 2,
          },
          {
            action: 'createTable', id: 'outputTable', title: 'Output', rows: 5, cols: 1,
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'inputTable' },
          { action: 'deleteTable', id: 'countsTable' },
          { action: 'deleteTable', id: 'outputTable' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: 5,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 1,
            col: 0,
            value: 4,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 2,
            col: 0,
            value: 3,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 3,
            col: 0,
            value: 2,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 4,
            col: 0,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 4,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 0,
            value: 2,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 0,
            value: 3,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 0,
            value: 4,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 0,
            value: 5,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 5,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 1,
            col: 0,
            value: 2,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 1,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 2,
            col: 0,
            value: 3,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 2,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 3,
            col: 0,
            value: 4,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 3,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 3,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 4,
            col: 0,
            value: 5,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 4,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 4,
            color: '#4CAF50',
          },
        ],
      },
    ]],
    // Only one number
    [[4], [
      {
        do: [{ action: 'clear' }],
        undo: [],
      },
      {
        do: [
          {
            action: 'createTable', id: 'inputTable', title: 'Input', rows: 1, cols: 1,
          },
          {
            action: 'createTable', id: 'countsTable', title: 'Counts', rows: 1, cols: 2,
          },
          {
            action: 'createTable', id: 'outputTable', title: 'Output', rows: 1, cols: 1,
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'inputTable' },
          { action: 'deleteTable', id: 'countsTable' },
          { action: 'deleteTable', id: 'outputTable' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: 4,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: 4,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: 4,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
      },
    ]],
    // Repeating numbers
    [[1, 2, 1], [
      {
        do: [{ action: 'clear' }],
        undo: [],
      },
      {
        do: [
          {
            action: 'createTable', id: 'inputTable', title: 'Input', rows: 3, cols: 1,
          },
          {
            action: 'createTable', id: 'countsTable', title: 'Counts', rows: 2, cols: 2,
          },
          {
            action: 'createTable', id: 'outputTable', title: 'Output', rows: 3, cols: 1,
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'inputTable' },
          { action: 'deleteTable', id: 'countsTable' },
          { action: 'deleteTable', id: 'outputTable' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 1,
            col: 0,
            value: 2,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 2,
            col: 0,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 2,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 0,
            value: 2,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 1,
            value: 2,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 1,
            value: 1,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 1,
            col: 0,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 1,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 2,
            col: 0,
            value: 2,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 2,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
          },
        ],
      },
    ]],
    // Random input
    [[3, 8, 2, 7, 1], [
      {
        do: [{ action: 'clear' }],
        undo: [],
      },
      {
        do: [
          {
            action: 'createTable', id: 'inputTable', title: 'Input', rows: 5, cols: 1,
          },
          {
            action: 'createTable', id: 'countsTable', title: 'Counts', rows: 8, cols: 2,
          },
          {
            action: 'createTable', id: 'outputTable', title: 'Output', rows: 5, cols: 1,
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'inputTable' },
          { action: 'deleteTable', id: 'countsTable' },
          { action: 'deleteTable', id: 'outputTable' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: 3,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 1,
            col: 0,
            value: 8,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 2,
            col: 0,
            value: 2,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 3,
            col: 0,
            value: 7,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 4,
            col: 0,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'inputTable',
            row: 4,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 0,
            value: 2,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 0,
            value: 3,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 0,
            value: 4,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 0,
            value: 5,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 5,
            col: 0,
            value: 6,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 6,
            col: 0,
            value: 7,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 7,
            col: 0,
            value: 8,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 5,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 6,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 7,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 3,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 7,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 7,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 7,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 8,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 7,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 7,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 2,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 2,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 6,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 6,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 6,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 7,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 6,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 3,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 6,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 0,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'countsTable',
            row: 1,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#ffffff',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
            text: '',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'inputTable',
            row: 4,
            color: '#4CAF50',
            text: '',
          },
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 0,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 0,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 1,
            col: 0,
            value: 2,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 1,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 1,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 2,
            col: 0,
            value: 3,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 2,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 2,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 6,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 6,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 3,
            col: 0,
            value: 7,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 3,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 6,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 6,
            color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 7,
            color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 7,
            color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 4,
            col: 0,
            value: 8,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'outputTable',
            row: 4,
            col: 0,
            value: null,
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 7,
            color: '#ffffff',
          },
        ],
        undo: [
          {
            action: 'setRowColor',
            id: 'countsTable',
            row: 7,
            color: '#4CAF50',
          },
        ],
      },
    ]],
  ])('should return the expected steps, input %#: %p', (input, expectedOutput) => {
    const actualOutput = CountingSort(input);
    expect(actualOutput.length).toEqual(expectedOutput.length);
    expectedOutput.forEach((element, index) => {
      expect(actualOutput[index]).toEqual(element);
    });
  });
});
