import Action from '../../components/sorts/BarChartActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm TemplateSort
 *
 * @author <your name>
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function TemplateSort(elements, testing = false) {
  const steps = [];

  const output = Array.from(elements);

  if (output.length === 0) {
    return testing ? output : steps;
  }

  steps.push({
    do: [
      {
        action: Action.SET_COLORS,
        elements: [
          {
            index: 1,
            color: Color.BASE_COLOR_BAR_CHART,
          },
        ],
        text: 'Setting blue color on element 1.',
      },
    ],
    undo: [
      {
        action: Action.SET_COLORS,
        elements: [
          {
            index: 1,
            color: Color.WHITE,
          },
        ],
        text: 'Undo setting blue color on element 1.',
      },
    ],
  });

  return testing ? output : steps;
}
