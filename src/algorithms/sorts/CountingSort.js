import Action from '../../components/sorts/TablesHorizontalActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm CountingSort
 *
 * Implementation based on pseudocode from Wikipedia.
 *
 * @author Bernhard Frick
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function CountingSort(elements, testing = false) {
  const steps = [];

  if (elements.length === 0) {
    // nothing to sort
    return testing ? Array.from(elements) : steps;
  }

  steps.push({
    do: [{
      action: Action.CLEAR,
    }],
    undo: [],
  });

  // get min and max from input for length of counts table
  const min = Math.min(...elements);
  const max = Math.max(...elements);

  const inputTableSize = elements.length;
  const countsTableSize = max - min + 1;
  const outputTableSize = elements.length;

  // create input, counts, output tables
  steps.push({
    do: [
      {
        action: Action.CREATE_TABLE,
        id: 'inputTable',
        title: 'Input',
        rows: inputTableSize,
        cols: 1,
      },
      {
        action: Action.CREATE_TABLE,
        id: 'countsTable',
        title: 'Counts',
        rows: countsTableSize,
        cols: 2,
      },
      {
        action: Action.CREATE_TABLE,
        id: 'outputTable',
        title: 'Output',
        rows: outputTableSize,
        cols: 1,
      },
    ],
    undo: [
      {
        action: Action.DELETE_TABLE,
        id: 'inputTable',
      },
      {
        action: Action.DELETE_TABLE,
        id: 'countsTable',
      },
      {
        action: Action.DELETE_TABLE,
        id: 'outputTable',
      },
    ],
  });

  // fill input table
  let doActions = [];
  let undoActions = [];
  for (let i = 0; i < inputTableSize; i += 1) {
    doActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'inputTable',
      row: i,
      col: 0,
      value: elements[i],
    });
    undoActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'inputTable',
      row: i,
      col: 0,
      value: null,
    });
  }
  steps.push({
    do: doActions,
    undo: undoActions,
  });

  // fill countsTable values (first col)
  doActions = [];
  undoActions = [];
  for (let i = 0; i < countsTableSize; i += 1) {
    doActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'countsTable',
      row: i,
      col: 0,
      value: min + i,
    });
    undoActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'countsTable',
      row: i,
      col: 0,
      value: null,
    });
  }
  steps.push({
    do: doActions,
    undo: undoActions,
  });

  const counts = new Map();

  // fill countsTable counts (second col)
  for (let i = 0; i < inputTableSize; i += 1) {
    // highlight input
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        id: 'inputTable',
        row: i,
        color: Color.TH_CURRENT_ELEMENT,
        text: '',
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        id: 'inputTable',
        row: i,
        color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        text: '',
      }],
    });

    // highlight count
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        id: 'countsTable',
        row: elements[i] - min,
        color: Color.TH_CURRENT_ELEMENT,
        text: '',
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        id: 'countsTable',
        row: elements[i] - min,
        color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        text: '',
      }],
    });

    // increase count
    if (counts.has(elements[i])) {
      counts.set(elements[i], counts.get(elements[i]) + 1);
      steps.push({
        do: [{
          action: Action.SET_CELL_VALUE,
          id: 'countsTable',
          row: elements[i] - min,
          col: 1,
          value: counts.get(elements[i]),
        }],
        undo: [{
          action: Action.SET_CELL_VALUE,
          id: 'countsTable',
          row: elements[i] - min,
          col: 1,
          value: counts.get(elements[i]) - 1,
        }],
      });
    } else {
      counts.set(elements[i], 1);
      steps.push({
        do: [{
          action: Action.SET_CELL_VALUE,
          id: 'countsTable',
          row: elements[i] - min,
          col: 1,
          value: 1,
        }],
        undo: [{
          action: Action.SET_CELL_VALUE,
          id: 'countsTable',
          row: elements[i],
          col: 1,
          value: null,
        }],
      });
    }

    // clear both highlights
    steps.push({
      do: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'inputTable',
          row: i,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
          text: '',
        },
        {
          action: Action.SET_ROW_COLOR,
          id: 'countsTable',
          row: elements[i] - min,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
          text: '',
        },
      ],
      undo: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'inputTable',
          row: i,
          color: Color.TH_CURRENT_ELEMENT,
          text: '',
        },
        {
          action: Action.SET_ROW_COLOR,
          id: 'countsTable',
          row: elements[i] - min,
          color: Color.TH_CURRENT_ELEMENT,
          text: '',
        },
      ],
    });
  }

  // sort the counts (view is already sorted)
  const countsSorted = new Map([...counts.entries()].sort(([va], [vb]) => Math.sign(va - vb)));

  // loop over counts to fill outputTable
  const sorted = [];
  let sortedPointer = 0;
  countsSorted.forEach((count, value) => {
    // highlight count for 'value' in countsTable
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        id: 'countsTable',
        row: value - min,
        color: Color.TH_CURRENT_ELEMENT,
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        id: 'countsTable',
        row: value - min,
        color: Color.BASE_COLOR_TABLE_HORIZONTAL,
      }],
    });

    // insert values into output
    for (let i = 0; i < count; i += 1) {
      sorted.push(value);
      steps.push({
        do: [{
          action: Action.SET_CELL_VALUE,
          id: 'outputTable',
          row: sortedPointer,
          col: 0,
          value,
        }],
        undo: [{
          action: Action.SET_CELL_VALUE,
          id: 'outputTable',
          row: sortedPointer,
          col: 0,
          value: null,
        }],
      });

      steps.push({
        do: [{
          action: Action.SET_ROW_COLOR,
          id: 'outputTable',
          row: sortedPointer,
          col: 0,
          color: Color.TH_ELEMENT_SORTED,
        }],
        undo: [{
          action: Action.SET_ROW_COLOR,
          id: 'outputTable',
          row: sortedPointer,
          col: 0,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        }],
      });
      sortedPointer += 1;
    }

    // clear highlight count for 'value' in countsTable
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        id: 'countsTable',
        row: value - min,
        color: Color.BASE_COLOR_TABLE_HORIZONTAL,
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        id: 'countsTable',
        row: value - min,
        color: Color.TH_CURRENT_ELEMENT,
      }],
    });
  });

  return testing ? sorted : steps;
}
