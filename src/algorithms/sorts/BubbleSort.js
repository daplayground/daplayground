import Action from '../../components/sorts/BarChartActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm BubbleSort
 *
 * Implementation based on pseudo code from Wikipedia.
 *
 * @author Bernhard Frick
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function BubbleSort(elements, testing = false) {
  const steps = [];

  const output = Array.from(elements);

  if (output.length === 0) {
    return testing ? output : steps;
  }

  let j = output.length - 1;

  // as long as there is an unsorted partition of the data...
  while (j > 0) {
    // loop over the unsorted partition of the data
    for (let i = 0; i < j; i += 1) {
      // compare elements i and i+1
      steps.push({
        do: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: i,
              color: Color.BUBBLE_SORT_COMPARISON,
            },
            {
              index: i + 1,
              color: Color.BUBBLE_SORT_COMPARISON,
            },
          ],
          text: '',
        }],
        undo: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: i,
              color: Color.BASE_COLOR_BAR_CHART,
            },
            {
              index: i + 1,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: '',
        }],
      });
      if (output[i] > output[i + 1]) {
        // elements need to be exchanged
        steps.push({
          do: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: i,
                color: Color.BUBBLE_SORT_COMPARISON_WRONG,
              },
              {
                index: i + 1,
                color: Color.BUBBLE_SORT_COMPARISON_WRONG,
              },
            ],
            text: '',
          }],
          undo: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: i,
                color: Color.BUBBLE_SORT_COMPARISON,
              },
              {
                index: i + 1,
                color: Color.BUBBLE_SORT_COMPARISON,
              },
            ],
            text: '',
          }],
        });
        // exchange elements
        const temp = output[i];
        output[i] = output[i + 1];
        output[i + 1] = temp;
        // create an exchange action
        steps.push({
          do: [{
            action: Action.EXCHANGE,
            elements: [i, i + 1],
            text: '',
          }],
          undo: [{
            action: Action.EXCHANGE,
            elements: [i, i + 1],
            text: '',
          }],
        });
      } else {
        // no exchange action required.
        steps.push({
          do: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: i,
                color: Color.BUBBLE_SORT_COMPARISON_TRUE,
              },
              {
                index: i + 1,
                color: Color.BUBBLE_SORT_COMPARISON_TRUE,
              },
            ],
            text: '',
          }],
          undo: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: i,
                color: Color.BUBBLE_SORT_COMPARISON,
              },
              {
                index: i + 1,
                color: Color.BUBBLE_SORT_COMPARISON,
              },
            ],
            text: '',
          }],
        });
      }

      // compare/exchange done
      steps.push({
        do: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: i,
              color: Color.BASE_COLOR_BAR_CHART,
            },
            {
              index: i + 1,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: '',
        }],
        undo: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: i,
              color: Color.BUBBLE_SORT_COMPARISON_WRONG,
            },
            {
              index: i + 1,
              color: Color.BUBBLE_SORT_COMPARISON_WRONG,
            },
          ],
          text: '',
        }],
      });
    }
    // after one iteration, the last element of the list is sorted
    steps.push({
      do: [{
        action: Action.SET_COLORS,
        elements: [
          {
            index: j,
            color: Color.BUBBLE_SORT_ELEMENT_SORTED,
          },
        ],
        text: `Element ${j} is sorted.`,
      }],
      undo: [{
        action: Action.SET_COLORS,
        elements: [
          {
            index: j,
            color: Color.BASE_COLOR_BAR_CHART,
          },
        ],
        text: `Element ${j} is sorted.`,
      }],
    });
    j -= 1;
  }

  // finally highlight the very last element, as it is also sorted.
  steps.push({
    do: [{
      action: Action.SET_COLORS,
      elements: [
        {
          index: j,
          color: Color.BUBBLE_SORT_ELEMENT_SORTED,
        },
      ],
      text: `Element ${j} is sorted.`,
    }],
    undo: [{
      action: Action.SET_COLORS,
      elements: [
        {
          index: j,
          color: Color.BASE_COLOR_BAR_CHART,
        },
      ],
      text: `Element ${j} is sorted.`,
    }],
  });

  return testing ? output : steps;
}
