import QuickSort from './QuickSort';
import getRandomInt from '../../utils/getRandomInt';
import isSorted from '../../utils/isSorted';

describe('QuickSort.js', () => {
  it('should sort the given random input', () => {
    const numTests = 1000;
    const inputSize = 50;
    for (let i = 0; i < numTests; i += 1) {
      const randomNumbers = new Array(inputSize)
        .fill(null)
        .map((_) => getRandomInt(0, 100));
      const sortedOutput = QuickSort(randomNumbers, true);
      expect(isSorted(sortedOutput)).toBe(true);
      break;
    }
  });
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 2, 3, 4, 5], [
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 3, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 3, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 1, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 1, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 2, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L' }, { index: 2, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 3, text: 'L/R' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: 'L/R' }, { index: 3, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: null }, { index: 4, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: 'L' }, { index: 4, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFEB3B' }],
            text: 'Comparing element 4 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Comparing element 4 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 and the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 and the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FF9800' }],
            text: "Element 4 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: "Element 3 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 2, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 2, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 with the pivot element 4.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 with the pivot element 4.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 1, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 1, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 4.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 4.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 2, text: 'L/R' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L/R' }, { index: 2, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 with the pivot element 4.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 with the pivot element 4.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 3, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: 'L' }, { index: 3, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 with the pivot element 4.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 with the pivot element 4.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 and the pivot element 4.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 and the pivot element 4.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FF9800' }],
            text: "Element 3 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: "Element 2 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 1, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 1, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 with the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 with the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 1, text: 'L/R' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L/R' }, { index: 1, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 2, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L' }, { index: 2, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 with the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 with the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 and the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 and the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FF9800' }],
            text: "Element 2 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: "Element 1 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 0, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 0, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 with the pivot element 2.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 with the pivot element 2.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 1, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 1, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 2.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 2.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 and the pivot element 2.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 and the pivot element 2.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FF9800' }],
            text: "Element 1 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: "Element 0 is now at it's sorted position.",
          },
        ],
      },
    ]],
    // Reverse input
    [[5, 4, 3, 2, 1], [
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 3, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 3, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 with the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 with the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: null }, { index: 2, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 3, text: 'R' }, { index: 2, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 1, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 2, text: 'R' }, { index: 1, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 0, text: 'L/R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 1, text: 'L/R' }, { index: 0, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: -1, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 0, text: 'R' }, { index: -1, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: -1, color: '#FFEB3B' }],
            text: 'Comparing element -1 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: -1, color: '#2196F3' }],
            text: 'Comparing element -1 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: -1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: -1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FF9800' }],
            text: "Element 0 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: "Element -1 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L' }, { index: 3, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 3, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 2, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L' }, { index: 2, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 3, text: 'L/R' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: 'L/R' }, { index: 3, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: null }, { index: 4, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: 'L' }, { index: 4, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFEB3B' }],
            text: 'Comparing element 4 with the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Comparing element 4 with the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 and the pivot element 5.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 and the pivot element 5.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FF9800' }],
            text: "Element 4 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: "Element 3 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L' }, { index: 2, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 2, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 2.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 2.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 and the pivot element 2.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 and the pivot element 2.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 1, text: 'L/R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 2, text: 'L/R' }, { index: 1, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 and the pivot element 2.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 and the pivot element 2.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 0, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 1, text: 'R' }, { index: 0, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 and the pivot element 2.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 and the pivot element 2.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FF9800' }],
            text: "Element 1 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: "Element 0 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: 'L' }, { index: 2, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 2, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 with the pivot element 4.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 with the pivot element 4.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 3, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: 'L' }, { index: 3, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 with the pivot element 4.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 with the pivot element 4.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 and the pivot element 4.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 and the pivot element 4.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FF9800' }],
            text: "Element 3 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: "Element 2 is now at it's sorted position.",
          },
        ],
      },
    ]],
    // Only one number
    [[4], []],
    // Repeating numbers
    [[1, 2, 1], [
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 1, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 1, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 with the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 with the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 0, text: 'L/R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 1, text: 'L/R' }, { index: 0, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FF9800' }],
            text: "Element 0 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: "Element -1 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L' }, { index: 1, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 1, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 0, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 1, text: 'R' }, { index: 0, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FF9800' }],
            text: "Element 1 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: "Element 0 is now at it's sorted position.",
          },
        ],
      },
    ]],
    // Random input
    [[3, 8, 2, 7, 1], [
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: 'L' }, { index: 3, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: 3, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 with the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 with the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: null }, { index: 2, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 3, text: 'R' }, { index: 2, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 1, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 2, text: 'R' }, { index: 1, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 0, text: 'L/R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 1, text: 'L/R' }, { index: 0, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Comparing element 0 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Comparing element 0 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 0, text: null }, { index: -1, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 0, text: 'R' }, { index: -1, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: -1, color: '#FFEB3B' }],
            text: 'Comparing element -1 and the pivot element 1.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: -1, color: '#2196F3' }],
            text: 'Comparing element -1 and the pivot element 1.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: -1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: -1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FF9800' }],
            text: "Element 0 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: "Element -1 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L' }, { index: 3, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 3, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 and the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 and the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: null }, { index: 2, text: 'R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 3, text: 'R' }, { index: 2, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 and the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 and the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#9C27B0' },
              { index: 2, color: '#9C27B0' },
            ],
            text: 'The elements L and R will be exchanged.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: 'The elements L and R will be exchanged.',
          },
        ],
      },
      {
        do: [
          {
            action: 'exchange',
            elements: [1, 2],
            text: 'Exchanging L (element 1) and R (element 2).',
          },
        ],
        undo: [
          {
            actions: 'exchange',
            elements: [1, 2],
            text: 'Exchanging L (element 1) and R (element 2).',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: 'Unsetting the exchange colors.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#9C27B0' },
              { index: 2, color: '#9C27B0' },
            ],
            text: 'Unsetting the exchange colors.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 2, text: null }, { index: 1, text: 'L/R' }],
            text: 'Moving R by 1 to the left.',
          },
        ],
        undo: {
          action: 'setLabels',
          elements: [{ index: 2, text: 'L/R' }, { index: 1, text: null }],
          text: 'Moving R by 1 to the left.',
        },
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 with the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 with the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: null }, { index: 2, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 1, text: 'L' }, { index: 2, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Comparing element 2 with the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Comparing element 2 with the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Comparing element 1 and the pivot element 3.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Comparing element 1 and the pivot element 3.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FF9800' }],
            text: "Element 2 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: "Element 1 is now at it's sorted position.",
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#E91E63' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'highlight pivot, the rightmost element',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: 'L' }, { index: 3, text: 'R' }],
            text: 'Set labels L and R.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: null }, { index: 3, text: null }],
            text: 'Set labels L and R.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 with the pivot element 8.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 with the pivot element 8.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: null }, { index: 4, text: 'L' }],
            text: 'Moving L by 1 to the right.',
          },
        ],
        undo: [
          {
            action: 'setLabels',
            elements: [{ index: 3, text: 'L' }, { index: 4, text: null }],
            text: 'Moving L by 1 to the right.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFEB3B' }],
            text: 'Comparing element 4 with the pivot element 8.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Comparing element 4 with the pivot element 8.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Comparing element 3 and the pivot element 8.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Comparing element 3 and the pivot element 8.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Unset comparison.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFEB3B' }],
            text: 'Unset comparison.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FF9800' }],
            text: "Element 4 is now at it's sorted position.",
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: "Element 3 is now at it's sorted position.",
          },
        ],
      },
    ]],
  ])('should return the expected steps, input %#: %p', (input, expectedOutput) => {
    const actualOutput = QuickSort(input);
    expect(actualOutput.length).toEqual(expectedOutput.length);
    expectedOutput.forEach((element, index) => {
      expect(actualOutput[index]).toEqual(element);
    });
  });
});
