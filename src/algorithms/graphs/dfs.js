import GraphAction from '../../utils/GraphAction';
import Stack from '../trekhleb/src/data-structures/stack/Stack';
import Color from '../../utils/Color';

export default function dfs(graph, startVertex) {
  const stack = new Stack();
  const visited = [];
  const actions = [];
  stack.push(startVertex);

  actions.push({
    do: {
      action: GraphAction.HIGHLIGHT_NODE,
      color: Color.GRAPH_HIGHLIGHTED_NODE,
      elements: startVertex.getKey(),
    },
    undo: {
      action: GraphAction.HIGHLIGHT_NODE,
      color: Color.BASE_COLOR_NODE,
      elements: startVertex.getKey(),
    },
  });

  while (!stack.isEmpty()) {
    const action = [];
    const currentVertex = stack.pop();

    action.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.GRAPH_RESULT_TRUE,
        elements: currentVertex.getKey(),
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.GRAPH_HIGHLIGHTED_NODE,
        elements: currentVertex.getKey(),
      },
    });

    if (!(visited.includes(currentVertex))) {
      visited.push(currentVertex);
    }

    currentVertex.getNeighbors().filter((item) => !(visited.includes(item))).forEach((item) => {
      stack.push(item);
      action.push({
        do: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.GRAPH_HIGHLIGHTED_NODE,
          elements: item.getKey(),
        },
        undo: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.BASE_COLOR_NODE,
          elements: item.getKey(),
        },
      });
    });
    actions.push(action);
  }
  return actions;
}
