/**
 * DataBlock
 * @param {number} localDepth
 * @param {number} dataBlockSize
 * @constructor
 */
function DataBlock(localDepth, dataBlockSize) {
  this.localDepth = localDepth;
  this.elements = new Array(dataBlockSize);
}

/**
 * The hash function.
 * @param key
 * @param globalDepth
 * @returns {number}
 */
function hash(key, globalDepth) {
  // BitMask consists of <globalDepth> 1s.
  const bitMask = (2 ** globalDepth) - 1; // Sum of 2^x series: 1 + 2 + 4 ...
  // only retain the rightmost <globalDepth> bits
  // eslint-disable-next-line no-bitwise
  return key & bitMask;
}

export default class ExtendibleHashing {
  /**
   * ?
   * @type {number}
   */
  globalDepth;

  /**
   * The Hash Table consists of a list of DataBlocks that hold the values.
   * @type {DataBlock[]}
   */
  dataBlocks;

  /**
   * Number of elements within a DataBlock
   * @type {number}
   */
  dataBlockSize;

  /**
   * Initialize an empty hash table.
   * @param {number} globalDepth
   * @param {number} dataBlockSize
   */
  init(globalDepth = 1, dataBlockSize = 4) {
    this.globalDepth = globalDepth;
    this.dataBlockSize = dataBlockSize;
    this.dataBlocks = [];

    // Initially, every index shall reference its own personal DataBlock, thus
    // localDepth = globalDepth for all DataBlocks.
    for (let address = 0; address < 2 ** this.globalDepth; address += 1) {
      this.dataBlocks[address] = new DataBlock(this.globalDepth, this.dataBlockSize);
    }
  }

  /**
   * Insert a value into the hash table.
   * @param value
   */
  insert(value) {
    let inserted = false;
    while (!inserted) {
      const address = hash(value, this.globalDepth);

      // 1) Add value
      let freeElementIndex;
      for (let i = 0; i < this.dataBlockSize; i += 1) {
        if (this.dataBlocks[address].elements[i] === value) {
          // Value exists
          inserted = true;
          break;
        }

        if (freeElementIndex === undefined && this.dataBlocks[address].elements[i] === undefined) {
          // Remember the first free slot
          freeElementIndex = i;
        }

        // Insert value in a free slot
        if (i === (this.dataBlockSize - 1) && freeElementIndex !== undefined) {
          this.dataBlocks[address].elements[freeElementIndex] = value;
          inserted = true;
          break;
        }
      }

      // temporary until split is moved out:
      if (inserted) {
        break;
      }

      // not inserted, overflow, split!

      if (this.dataBlocks[address].localDepth < this.globalDepth) {
        // if localDepth < globalDepth: split the DataBlock

        // Save old DataBlock elements
        const oldElements = this.dataBlocks[address].elements;
        // create a new DataBlock with increased localDepth at the current address
        this.dataBlocks[address] = new DataBlock(
          this.dataBlocks[address].localDepth + 1,
          this.dataBlockSize,
        );

        // Move oldElements to new DataBlock if necessary (???)
        let freePosNew = 0;
        // loop over old elements
        for (let i = 0; i < this.dataBlockSize; i += 1) {
          const newAddress = hash(oldElements[i], this.dataBlocks[address].localDepth);
          if (newAddress === address) {
            this.dataBlocks[address].elements[freePosNew] = oldElements[i];
            freePosNew += 1;
            oldElements[i] = undefined;
          }
        }
        // expanded, try to insert in next iteration
      }

      if (this.dataBlocks[address].localDepth === this.globalDepth) {
        // if localDepth == globalDepth: Expand Index

        // Double the size of the DataBlock array and point the new fields to the
        // old ones sequentially from 0 to the last field of the "old" array
        const dataBlockArrayLengthOld = this.dataBlocks.length;
        for (let i = dataBlockArrayLengthOld; i < 2 * dataBlockArrayLengthOld; i += 1) {
          this.dataBlocks[i] = this.dataBlocks[i - dataBlockArrayLengthOld];
        }
        this.globalDepth += 1;
        // expanded, try to insert in next iteration
      }
    }

    if (inserted) {
      // inserted
    } else {
      // not inserted
    }
  }

  /**
   * Search the given value in the hash table.
   * @param value
   */
  search(value) {
    const address = hash(value, this.globalDepth);

    let found = false;
    for (let i = 0; i < this.dataBlocks[address].elements.length; i += 1) {
      if (this.dataBlocks[address].elements[i] === value) {
        found = true;
        break;
      }
    }

    if (found) {
      // found
    } else {
      // not found
    }
  }

  /**
   * Remove the given value from the hash table.
   * @param value
   */
  remove(value) {
    const address = hash(value, this.globalDepth);

    let removed = false;
    for (let i = 0; i < this.dataBlocks[address].elements.length; i += 1) {
      if (this.dataBlocks[address].elements[i] === value) {
        this.dataBlocks[address].elements[i] = undefined;
        removed = true;
        break;
      }
    }

    if (removed) {
      // removed
    } else {
      // not found, not removed
    }
  }
}
