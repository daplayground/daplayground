import { v4 as uuidv4 } from 'uuid';

export default class Node {
    keys;

    pointers;

    is_leaf;

    next;

    parent;

    id;

    constructor() {
      this.keys = [];
      this.pointers = [];
      this.is_leaf = true;
      this.next = null; // for the linked list of leaves
      this.parent = undefined; // parent pointer
      this.id = `N${uuidv4()}`;
    }
}
