import BPlusTree from './BPlusTree';
import Node from './BPlusTreeNode';
import isSorted from '../../utils/isSorted';

describe('BPlusTree.js', () => {
  it('Creates an empty B+Tree', () => {
    const N = 4;
    const tree = new BPlusTree(N);
    expect(tree.size()).toEqual(0);
    expect(tree.N).toEqual(N);
    expect(tree.root).toBeInstanceOf(Node);
    expect(tree.root.values.length).toEqual(2 * N);
    expect(tree.root.children.length).toEqual(2 * N + 1);
  });

  it('Correctly inserts a value', () => {
    const value = 5;
    const tree = new BPlusTree(4);
    tree.insert(value);
    expect(tree.size()).toEqual(1);
    expect(tree.root.used).toEqual(1);
    expect(tree.root.values[0]).toEqual(value);
    tree.root.children.forEach((childNode) => {
      expect(childNode).toBeUndefined();
    });
  });

  it('Correctly deletes a value', () => {
    const value = 3;
    const tree = new BPlusTree(4);
    tree.insert(value);
    const deleted = tree.delete(value);
    expect(deleted).toEqual(1);
    expect(tree.size()).toEqual(0);
    expect(tree.root.used).toEqual(0);
    tree.root.children.forEach((childNode) => {
      expect(childNode).toBeUndefined();
    });
  });

  it('Finds an existing value', () => {
    const value = 3;
    const tree = new BPlusTree(4);
    tree.insert(value);
    const result = tree.search(value);
    expect(result).toEqual(value);
  });

  it('Returns undefined when trying to find a non-existing value', () => {
    const value = 3;
    const tree = new BPlusTree(4);
    const result = tree.search(value);
    expect(result).toEqual(undefined);
  });

  it('Correctly splits the Root (Leaf) Node on overflow', () => {
    //
  });

  it('Correctly splits an Internal Node on overflow', () => {
    //
  });

  it('Correctly fixes an underflow in a Leaf Node after a deletion', () => {
    //
  });

  it('Correctly collapses the root node on underflow in root node after deletion', () => {
    //
  });

  it('should return the correct elements when calling the iterator', () => {
    const k = 3;
    const tree = new BPlusTree(k);

    tree.insert(10);
    tree.insert(5);
    tree.insert(4);
    tree.insert(7);
    tree.insert(15);
    tree.insert(12);
    tree.insert(17);
    tree.insert(20);
    tree.insert(1);
    tree.insert(11);
    tree.insert(3);
    tree.insert(30);
    tree.insert(2);
    tree.insert(43);
    tree.insert(16);
    tree.insert(55);

    const elements = [...tree.iterator()];

    expect(elements.length).toBe(16);

    expect(isSorted(elements)).toBe(true);

    [1, 2, 3, 4, 5, 7, 10, 11, 12, 15, 16, 17, 20, 30, 43, 55]
      .forEach((element, index) => {
        expect(elements[index]).toBe(element);
      });
  });

  it('Issue #28: seemingly inconsistent state with the given input', () => {
    const N = 2;
    const tree = new BPlusTree(N);

    tree.insert(10);
    tree.insert(5);
    tree.insert(4);
    tree.insert(7);
    tree.insert(15);
    tree.insert(12);
    tree.insert(17);
    tree.insert(20);

    // the tree itself
    expect(tree.size()).toEqual(8);
    expect(isSorted([...tree.iterator()])).toBe(true);

    // root node
    expect(tree.root.used).toEqual(2);
    expect(tree.root.values[0]).toEqual(7);
    expect(tree.root.values[1]).toEqual(12);

    // leaf nodes
    const childNode0 = tree.root.children[0];
    const childNode1 = tree.root.children[1];
    const childNode2 = tree.root.children[2];
    expect(childNode0.used).toEqual(2);
    expect(childNode1.used).toEqual(2);
    expect(childNode2.used).toEqual(4);
    expect(childNode0.values[0]).toEqual(4);
    expect(childNode0.values[1]).toEqual(5);
    expect(childNode1.values[0]).toEqual(7);
    expect(childNode1.values[1]).toEqual(10);
    expect(childNode2.values[0]).toEqual(12);
    expect(childNode2.values[1]).toEqual(15);
    expect(childNode2.values[2]).toEqual(17);
    expect(childNode2.values[3]).toEqual(20);

    // linear linking
    expect(childNode0.previous).toBe(undefined);
    expect(childNode0.next).toBe(childNode1);
    expect(childNode1.previous).toBe(childNode0);
    expect(childNode1.next).toBe(childNode2);
    expect(childNode2.previous).toBe(childNode1);
    expect(childNode2.next).toBe(undefined);
  });
});
