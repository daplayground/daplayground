/* eslint-disable no-param-reassign,prefer-destructuring */

import Node from './BPlusTreeNode';
import TreeUtils from '../../utils/TreeUtils';
import GraphAction from '../../utils/GraphAction';

/**
 * B+Tree by Bernhard Frick
 *
 * Based on my own C++ Implementation (https://gitlab.com/cs-univie/ads-bplustree).
 *
 * Algorithms and Data Structures
 * Faculty of Computer Science
 * University of Vienna
 *
 * Parameters:
 * N: The order of the Tree. Used to calculate the Size of one Node.
 */
export default class BPlusTree {
  /**
   * Order of the Tree.
   * Node size is 2*N.
   */
  N;

  /**
   * Number of Keys held in the B+Tree
   * @type {number}
   */
  elements;

  /**
   * The Root-Node of the B+Tree
   * @type {Node}
   */
  root;

  /**
   * @type {{}[]}
   */
  actions;

  /**
   * Constructor
   * @param {number} N
   */
  constructor(N) {
    this.N = N;
    this.elements = 0;
    this.root = new Node(Node.LEAF_NODE, this.N);
    this.actions = [];
  }

  /**
   * Destructor
   * JS does not have destructors, so we have to implement and call them ourselves.
   */
  destructor() {
    this.root.destructor();
    delete this.root;
  }

  // -----------------------------------------------------------------------------------------------

  // "Private" methods:

  /**
   * Recursively inserts the given Key into the B+Tree.
   * This Method resolves overflows in Internal- and Leaf-Nodes.
   * @param {Node} node The Node in which to insert the given key
   * @param {number} key The Key to insert
   * @return {Node} A newly created Node, if a Node-split occurred, undefined otherwise
   */
  add(node, key) {
    this.actions.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: 'red',
        elements: node.id,
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: '69b3a2',
        elements: node.id,
      },
    });
    if (node.type === Node.INTERNAL_NODE) {
      /** Finding the Position to insert */
      let insertPos = 0;
      for (; insertPos < node.used; insertPos += 1) {
        if (key < node.values[insertPos]) {
          /** pos found */
          break;
        }
      }

      /** Calling add() for Child-Node and keeping the return-Value */
      const retNode = this.add(node.children[insertPos], key);

      /** Stopping if there is no returned Node to handle */
      if (retNode === undefined) {
        return undefined;
      }

      /** A Node was returned, insert retNode into node */

      if (node.used === 2 * this.N) {
        /** InternalNode is full - split */

        /** Creating a new temporary Value-Array (size: 2*N+1) */
        /** @type {number[]} */
        const tempV = new Array(2 * this.N + 1);

        /** Creating a new temporary Node-Array (size: 2*N+2) */
        /** @type {Node[]} */
        const tempN = new Array(2 * this.N + 2);

        /** Moving Values from node to tempV */
        for (let i = 0; i < 2 * this.N; i += 1) {
          tempV[i] = node.values[i];
        }

        /** Moving Node from node to tempN */
        for (let i = 0; i < 2 * this.N + 1; i += 1) {
          tempN[i] = node.children[i];
        }
        node.used = 0;

        /** Finding Insert-Position and inserting Reference-Value in tempV */
        let i = 2 * this.N;
        for (; i > 0 && retNode.values[0] < tempV[i - 1]; i -= 1) {
          tempV[i] = tempV[i - 1];
        }
        tempV[i] = retNode.values[0];

        /** Inserting retNode at insertPos in tempN */
        let j = 2 * this.N + 1;
        for (; j > i + 1; j -= 1) {
          tempN[j] = tempN[j - 1];
        }
        tempN[j] = retNode;

        /** Creating a new Neighbor Internal Node */
        /** @type {Node} */
        const r = new Node(Node.INTERNAL_NODE, this.N);

        /** Inserting left half of the temporary Arrays in node and updating node.used */
        for (i = 0; i < this.N; i += 1) { /** Values */
          node.values[i] = tempV[i];
          node.used += 1;
        }
        for (i = 0; i <= this.N; i += 1) { /** Nodes */
          node.children[i] = tempN[i];
        }

        /** Inserting right half of the temporary Arrays in r and updating r.used */
        for (i = this.N; i < 2 * this.N + 1; i += 1) { /** Values */
          r.values[r.used] = tempV[i];
          r.used += 1;
        }
        let c = 0;
        for (i = this.N + 1; i < 2 * this.N + 2; i += 1) { /** Nodes */
          r.children[c] = tempN[i];
          c += 1;
        }

        /** Removing Reference Value from retNode, if retNode is an Internal Node */
        if (retNode.type === Node.INTERNAL_NODE) {
          for (i = 0; i < retNode.used; i += 1) {
            retNode.values[i] = retNode.values[i + 1];
          }
          retNode.used -= 1;
        }

        /** Returning newly created Node */
        return r;
      }
      /** InternalNode is not full - insert */

      /** Finding Position and moving Values to create a gap for the Value to hoist */
      insertPos = node.used;
      for (; insertPos > 0 && retNode.values[0] < node.values[insertPos - 1]; insertPos -= 1) {
        node.values[insertPos] = node.values[insertPos - 1];
      }

      /** Hoisting Reference Value */
      node.values[insertPos] = retNode.values[0];
      node.used += 1;

      /** Moving Pointers to create a gap for retNode */
      for (let i = node.used; i > insertPos; i -= 1) {
        node.children[i] = node.children[i - 1];
      }

      /** Inserting retNode in node */
      node.children[insertPos + 1] = retNode;

      /** Removing Reference Value from retNode, if retNode is an Internal Node */
      if (retNode.type === Node.INTERNAL_NODE) {
        for (let i = 0; i < retNode.used - 1; i += 1) {
          retNode.values[i] = retNode.values[i + 1];
        }
        retNode.used -= 1;
      }

      return undefined;
    }
    /** Node is LeafNode */

    /** Finding the Position to insert */
    /** Also, if a duplicate is found, Insertion can be stopped. */
    let insertPos = 0;
    for (; insertPos < node.used; insertPos += 1) {
      if (node.values[insertPos] === key) {
        /** duplicate */
        return undefined;
      }
      if (key < node.values[insertPos]) {
        /** pos found */
        break;
      }
    }

    if (node.used === 2 * this.N) {
      /** LeafNode is full - split */

      /** Creating a new temporary Value-Array (size: 2*N+1) */
      /** @type {number[]} */
      const tempV = new Array(2 * this.N + 1);

      /** Moving Values from node to the temporary Array */
      for (let i = 0; i < 2 * this.N; i += 1) {
        tempV[i] = node.values[i];
      }
      node.used = 0;

      /** Inserting new Value into the temporary Array */
      for (let i = 2 * this.N; i > insertPos; i -= 1) {
        tempV[i] = tempV[i - 1];
      }
      tempV[insertPos] = key;

      /** Creating a new Neighbor Leaf Node */
      /** @type {Node} */
      const r = new Node(Node.LEAF_NODE, this.N);

      /** Inserting left half of the temporary Array in node and updating node.used */
      for (let i = 0; i < this.N; i += 1) {
        node.values[i] = tempV[i];
        node.used += 1;
      }

      /** Inserting right half of the temporary Array in r and updating r.used */
      for (let i = this.N; i < 2 * this.N + 1; i += 1) {
        r.values[r.used] = tempV[i];
        r.used += 1;
      }

      /** Updating chaining of Leaf Nodes */
      r.next = node.next;
      r.previous = node;
      if (node.next) {
        node.next.previous = r;
      }
      node.next = r;

      /** Incrementing Counter */
      this.elements += 1;

      /** Returning newly created Node */
      return r;
    }
    /** LeafNode is not full - insert */

    /** Moving values to the right to create a gap for the new Value */
    for (let i = node.used; i > 0 && key < node.values[i - 1]; i -= 1) {
      node.values[i] = node.values[i - 1];
    }

    /** Inserting new Value */
    node.values[insertPos] = key;
    node.used += 1;

    /** Incrementing Counter */
    this.elements += 1;

    return undefined;
  }

  /**
   * Inserts the given Key into the Root-Node of the B+Tree using add().
   * This method resolves overflows that occur inside the root-Node.
   * @param key   The Key to insert
   * @param actions
   * @return void
   */
  addToRootNode(key, actions = []) {
    /** Inserting the given key into the Root-Node of the B+Tree */
    /** @type {Node} */
    const retNode = this.add(this.root, key);

    /** Checking for Root-Overflow */
    if (retNode === undefined) {
      return;
    }

    /** Is Root-Node LeafNode or InternalNode? */
    if (this.root.type === Node.LEAF_NODE) {
      /** Creating a new Root Node */
      /** @type {Node} */
      const r = new Node(Node.INTERNAL_NODE, this.N);

      /** Linking the old Root Node and the new Node in the new Root Node */
      r.children[0] = this.root;
      r.children[1] = retNode;

      /** Updating the Reference Value inside the new Root Node */
      r.values[0] = retNode.values[0];
      r.used += 1;

      /** Linking the new Root Node in this */
      this.root = r;
    } else if (this.root.type === Node.INTERNAL_NODE) {
      /** Creating a new Internal Root Node */
      /** @type {Node} */
      const r = new Node(Node.INTERNAL_NODE, this.N);

      /** Updating Reference Value in new Root */
      r.values[0] = retNode.values[0];
      r.used += 1;

      /** Removing first Value from retNode, because retNode is an Internal Node */
      for (let i = 0; i < retNode.used - 1; i += 1) {
        retNode.values[i] = retNode.values[i + 1];
      }
      retNode.used -= 1;

      /** Linking old Root-Node and retNode in new Root */
      r.children[0] = this.root;
      r.children[1] = retNode;

      /** Linking new Root in BPlusTree */
      this.root = r;
    }
  }

  /**
   * Resolves an underflow that occurred after removing a value from a node.
   * An underflow is resolved by shifting a Value from or merging with a
   * Neighbor-Node that has the same Parent-Node. In order to allow access
   * to the Parent-Node, the underflow-resolution-algorithm is designed to
   * resolve an underflow from the scope of the Parent-Node.
   * @param node          The Parent-Node of a Node in which an Underflow occurred
   * @param underflowPos  The Position of the Underflow-Node in node
   */
  fixUnderflow(node, underflowPos) {
    if (underflowPos > 0 && node.children[underflowPos - 1].used > this.N) {
      /** Shift from Left Neighbor */

      /** Defining Shift-Source and Target */
      /** @type {Node} */
      const source = node.children[underflowPos - 1];
      /** @type {Node} */
      const target = node.children[underflowPos];

      /** Moving all Values and Pointers in center by 1 to the right */
      for (let i = target.used + 1; i > 0; i -= 1) {
        target.values[i] = target.values[i - 1]; /** Moving Values */
        target.children[i] = target.children[i - 1]; /** Moving Pointers */
      }
      target.used += 1;

      if (target.type === Node.LEAF_NODE) {
        /** Copying Value from source to target */
        target.values[0] = source.values[source.used - 1];

        /** Updating Reference Value */
        node.values[underflowPos - 1] = target.values[0];
      } else {
        /** Pulling Reference-Value down */
        target.values[0] = node.values[underflowPos - 1];

        /** Moving Reference-Value up */
        node.values[underflowPos - 1] = source.values[source.used - 1];

        /** Shifting Child-Node */
        target.children[0] = source.children[source.used];
      }

      /** Removing copied value from source */
      source.used -= 1;
    } else if (underflowPos < node.used && node.children[underflowPos + 1].used > this.N) {
      /** Shift from Right Neighbor */

      /** Defining Shift-Source and Target */
      /** @type {Node} */
      const target = node.children[underflowPos];
      /** @type {Node} */
      const source = node.children[underflowPos + 1];

      if (target.type === Node.INTERNAL_NODE) {
        /** Pulling down Reference Value from node to target */
        target.values[target.used] = node.values[underflowPos];
        target.used += 1;

        /** Moving Reference Value up from source to node */
        node.values[underflowPos] = source.values[0];

        /** Moving Pointer from source to target */
        target.children[target.used] = source.children[0];

        /** Closing Gap in source */
        let i = 0;
        for (; i < source.used - 1; i += 1) {
          source.values[i] = source.values[i + 1];
          source.children[i] = source.children[i + 1];
        }
        source.used -= 1;

        /** Moving the missing last Pointer */
        source.children[i] = source.children[i + 1];
      } else {
        /** Child-Node is LeafNode */

        /** Moving Reference Value from source to target */
        target.values[target.used] = source.values[0];
        target.used += 1;

        /** Closing Gap in source */
        for (let i = 0; i < source.used - 1; i += 1) {
          source.values[i] = source.values[i + 1];
        }
        source.used -= 1;

        /** Updating Reference in node */
        node.values[underflowPos] = source.values[0];
      }
    } else {
      /** Merge Nodes */

      /** Always merge with right Neighbor, this simplifies the merge-algorithm */
      let targetPos = underflowPos;
      if (targetPos === node.used) {
        targetPos -= 1;
      }
      const sourcePos = targetPos + 1;

      /** Defining Shift-Source and Target */

      /** @type {Node} */
      const target = node.children[targetPos];
      /** @type {Node} */
      const source = node.children[sourcePos];

      /** Pull down reference value, if child Node is InternalNode */
      if (target.type === Node.INTERNAL_NODE) {
        target.values[target.used] = node.values[sourcePos - 1];
        target.used += 1;
      }

      /** Moving all Values and Pointers from source Node to target Node */
      /** Important: writing at the same pos in values and children
       * because of pulled-down reference */
      let i;
      for (i = 0; i < source.used; i += 1) {
        target.values[target.used] = source.values[i];
        target.children[target.used] = source.children[i];
        target.used += 1;
      }

      /** Add last Pointer from source, that is missing after the loop above */
      target.children[target.used] = source.children[i];

      /** Updating node-chaining */
      target.next = source.next;
      if (source.next) {
        source.next.previous = target;
      }

      /** Deleting empty source Node */
      source.type = Node.LEAF_NODE; /** Prevent Deletion of Child-Nodes */
      source.destructor();

      /** Closing Gap in node */
      for (i = targetPos; i < node.used - 1; i += 1) {
        node.values[i] = node.values[i + 1];
        node.children[i + 1] = node.children[i + 2];
      }
      node.used -= 1;
    }
    /*
    this.actions.push({
      do: {
        action: TreeUtils.UPDATE_TREE,
        elements: this.root,
      },
      undo: {
        action: TreeUtils.UPDATE_TREE,
      },
    });
    */
  }

  /**
   * Resolves an underflow that occurred inside the Root-Node. The Root-Node can
   * have at least 1 value and 2 Child-Nodes. If there is an Underflow, the
   * height of the B+Tree decreases.
   */
  fixRootUnderflow() {
    /** The only Child-Node of the Root-Node becomes the new Root-Node */
    const newRoot = this.root.children[0];

    /** Delete the old Root-Node */
    this.root.type = Node.LEAF_NODE; /** Prevent Deletion of Child-Nodes */
    delete this.root;

    /** Setting the new Root-Node */
    this.root = newRoot;
    /*
    this.actions.push({
      do: {
        action: TreeUtils.UPDATE_TREE,
        elements: this.root,
      },
      undo: {
        action: TreeUtils.UPDATE_TREE,
      },
    });
    */
  }

  /**
   * Recursively removes the given value from the given node.
   * @param {Node} node The Node, from which value should be removed
   * @param {number} value The value to be removed
   * @return {boolean} True if value was removed, false otherwise (if
   *                   value was not there in the first place)
   */
  remove(node, value) {
    /*
    this.actions.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: 'red',
        elements: node.id,
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: '69b3a2',
        elements: node.id,
      },
    });
    */
    /** Calculating the position for recursion. */
    let recurPos = 0;
    for (; recurPos < node.used; recurPos += 1) {
      if (value < node.values[recurPos]) {
        /** Position for recursion found */
        break;
      }
    }

    if (node.children[recurPos].type === Node.INTERNAL_NODE) {
      /** Removing value from children */
      /** @type {boolean} */
      const removed = this.remove(node.children[recurPos], value);

      /** Fixing underflow in child -- Child is always Internal */
      if (node.children[recurPos].used < this.N) {
        this.fixUnderflow(node, recurPos);
      }

      /** Returning Removal-Information */
      return removed;
    }
    /** Node is LeafNode */

    /** Calculating the position of the value to delete */
    let deletePos = 0;
    for (; deletePos < node.children[recurPos].used; deletePos += 1) {
      if (node.children[recurPos].values[deletePos] < value) {
        /** Key not yet found */
        // eslint-disable-next-line no-continue
        continue;
      }
      if (value === node.children[recurPos].values[deletePos]) {
        /** deletePos found */
        break;
      }
      if (value < node.children[recurPos].values[deletePos]) {
        /** Stop if value does not exist */
        return false;
      }
    }

    /** Stop if loop ran over the end */
    if (deletePos === node.children[recurPos].used) {
      return false;
    }

    /** Deleting Key from LeafNode at deletePos */
    for (let i = deletePos; i < node.children[recurPos].used - 1; i += 1) {
      node.children[recurPos].values[i] = node.children[recurPos].values[i + 1];
    }
    node.children[recurPos].used -= 1;
    this.elements -= 1;

    /** Fixing underflow in child -- Child is always Leaf */
    if (node.children[recurPos].used < this.N) {
      this.fixUnderflow(node, recurPos);
    }

    /** Returning Removal-Information */
    return true;
  }

  /**
   * Removes the given Key from the Root-Node of the B+Tree.
   * Because a Root-Node has at least 1 Value and 2 Child-Nodes, removal is
   * slightly different and therefore handled separately.
   * @param {number} value The Key to remove from the B+Tree
   * @return {number} 1 if value was removed, 0 otherwise (if value was not
   *                  there in the first place)
   */
  removeFromRoot(value) {
    /** Stop if the B+Tree is empty */
    if (this.elements === 0) return 0;

    if (this.root.type === Node.INTERNAL_NODE) {
      /** Calling remove on root */
      /** @type {boolean} */
      const removed = this.remove(this.root, value);

      /** Fixing Underflow in root */
      if (this.root.used === 0) {
        this.fixRootUnderflow();
      }

      /** Returning Removal-Information */
      return removed ? 1 : 0;
    }

    /** Node is LeafNode */

    /** Calculating the position of the value to delete */
    let deletePos = 0;
    for (; deletePos < this.root.used; deletePos += 1) {
      if (this.root.values[deletePos] < value) {
        /** Key not yet found */
        // eslint-disable-next-line no-continue
        continue;
      }
      if (value === this.root.values[deletePos]) {
        /** deletePos found */
        break;
      }
      if (value < this.root.values[deletePos]) {
        /** Stop if value does not exist */
        return 0;
      }
    }

    /** Stop if loop ran over the end */
    if (deletePos === this.root.used) {
      return 0;
    }

    /** Deleting Key from RootLeafNode */
    for (let i = deletePos; i < this.root.used - 1; i += 1) {
      this.root.values[i] = this.root.values[i + 1];
    }
    this.root.used -= 1;
    this.elements -= 1;

    /** Returning Removal-Information */
    return 1;
  }

  /**
   * Recursively searches the given Node for the given Key.
   * @param {Node} node The Node to search in
   * @param {number} key The Key to be searched
   * @return {number} The found value or End-Iterator (undefined)
   */
  find(node, key) {
    this.actions.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: 'red',
        elements: node.id,
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: '69b3a2',
        elements: node.id,
      },
    });
    if (node.type === Node.INTERNAL_NODE) { /** Node is InternalNode */
      /** Finding Position for recursive Call */
      let i = 0;
      for (; i < node.used; i += 1) {
        if (key < node.values[i]) {
          break;
        }
      }

      /** Call search() at recurPos */
      return this.find(node.children[i], key);
    } /** Node is LeafNode */
    /** Loop over all Values in the Leaf and return Iterator to key */
    for (let i = 0; i < node.used; i += 1) {
      if (key === node.values[i]) {
        this.actions.push({
          do: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: 'blue',
            elements: node.id,
          },
          undo: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: 'red',
            elements: node.id,
          },
        });
        return node.values[i];
        // previously, an iterator was returned.
      }
    }
    /** Key was not found, return End-Iterator */
    return this.end();
  }

  /**
   * Finds the first Leaf-Node inside the given node
   * @param {Node} node The Node to search in
   * @return {Node} A Pointer to the first Leaf-Node
   */
  first(node) {
    if (node.type === Node.LEAF_NODE) {
      return node;
    }
    return this.first(node.children[0]);
  }

  /**
   * "End-Iterator"
   * @return {undefined}
   */
  // eslint-disable-next-line class-methods-use-this
  end() {
    return undefined;
  }

  // -----------------------------------------------------------------------------------------------

  // "Public" methods:

  /**
   * Insert value into the B+Tree.
   * @param {number} value The Value to insert.
   * @return {boolean} True if value was inserted, false otherwise (if it already existed).
   */
  insert(value) {
    /** Value was not yet inserted */
    let inserted = false;

    /** Searching for value */
    let it = this.search(value);
    this.actions = [];

    /** If Value was not found... */
    if (it === this.end()) {
      /** Inserting Value */
      this.addToRootNode(value);
      const helpActions = this.getActions();
      /** Updating Iterator */
      it = this.search(value);
      /** Value was inserted */
      inserted = true;
      this.actions = helpActions;
    }

    this.actions.push({
      do: {
        action: TreeUtils.UPDATE_TREE,
        elements: this.root,
      },
      undo: {
        action: TreeUtils.UPDATE_TREE,
      },
    });
    /** Returning Insertion-Data */
    return inserted;
    // 'it' was used previously to return an iterator.
  }

  /**
   * Finds a Value equivalent to the given key.
   * @param {number} value The Key to search for
   * @return {number} Value. If no Value is found, past-the-end (see end()) iterator is returned.
   */
  search(value) {
    this.actions = [];
    return this.find(this.root, value);
  }

  /**
   * Removes the value equivalent to the given value (if one exists).
   * @param {number} value The Value to remove
   * @return {number} The Number of Values removed ( 0 or 1)
   */
  delete(value) {
    this.actions = [];
    const help = this.removeFromRoot(value);

    this.actions.push({
      do: {
        action: TreeUtils.UPDATE_TREE,
        elements: this.root,
      },
      undo: {
        action: TreeUtils.UPDATE_TREE,
      },
    });

    return help;
  }

  /**
   * Retrieves the number of Values stored in the B+Tree
   * @return {number} The number of Values
   */
  size() {
    return this.elements;
  }

  /**
   * Removes all Values from the B+Tree.
   */
  clear() {
    /** Deleting all Nodes */
    this.root.destructor();
    delete this.root;

    /** Resetting Element-Count */
    this.elements = 0;

    /** Creating a new Root-Node */
    this.root = new Node(Node.LEAF_NODE, this.N);
  }

  /**
   * Recursively displays the Structure of the B+Tree.
   * @param {Node} node A Node that should be displayed
   * @param {number} level The recursion-Depth, used to
   *                 represent the Structure of the B+Tree
   *                 with indentation
   */
  print(node, level) {
    if (node.type === Node.LEAF_NODE) {
      /** Indenting */
      for (let j = 0; j < level; j += 1) process.stdout.write('    ');

      /** Printing Node Information */
      process.stdout.write(`LeafNode ${node.previous} -- ${node} -- ${node.next} - ${node.used} Values:`);

      /** Printing Values */
      for (let i = 0; i < node.used; i += 1) process.stdout.write(`${node.values[i]} `);

      /** Line-Break */
      process.stdout.write('\n');
    } else if (node.type === Node.INTERNAL_NODE) {
      /** Indenting */
      for (let j = 0; j < level; j += 1) process.stdout.write('    ');

      /** Printing Node Information */
      process.stdout.write(`InternalNode (${node}) - ${node.used} Values: `);

      /** Printing Values */
      for (let i = 0; i < node.used; i += 1) process.stdout.write(`${node.values[i]} `);

      process.stdout.write(`- ${node.used + 1} Children: `);

      /** Printing Children */
      for (let i = 0; i < node.used + 1; i += 1) process.stdout.write(`${node.children[i]} `);

      /** Line-Break */
      process.stdout.write('\n');

      /** Calling Print for all Child-Nodes */
      for (let i = 0; i < node.used + 1; i += 1) this.print(node.children[i], level + 1);
    }
  }

  getActions() {
    return this.actions;
  }
}
