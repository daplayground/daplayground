import * as d3 from 'd3';
import BPlusTree from './BPlusWebAD1';

describe('BPlusTree.js', () => {
  it('Print a B+Tree', () => {
    const tree = new BPlusTree(1);
    const arr = [1, 2, 3, 4, 5, 6, 7];
    for (let i = 0; i < arr.length; ++i) {
      tree.addFixed(arr[i]);
      console.log(i);
      tree.print();
    }
    tree.print(tree.root, 0);
    console.log('AFTER DELETE');
    tree.remove(5);
    tree.remove(1);
    tree.print(tree.root, 0);
  });

  it('Test d3 Hierachy working with WEBAD Tree', () => {
    const tree = new BPlusTree(1);
    const arr = [1, 2, 3, 4, 5, 6, 7];
    for (let i = 0; i < arr.length; ++i) {
      tree.addFixed(arr[i]);
      console.log(i);
      tree.print();
    }
    tree.print(tree.root, 0);
    console.log('AFTER DELETE');
    tree.remove(5);
    const hierarchy = d3.hierarchy(tree.root, (d) => d.pointers);
    const treeLayout = d3.tree()
      .size([1000, 300]);
    console.log(hierarchy === undefined);
    const btree = treeLayout(hierarchy);
    console.log(btree.descendants());
  });
});
