import Node from './BPlusTreeNode';

describe('BPlusTreNode.js', () => {
  it('Creates a Leaf Node', () => {
    const node = new Node(Node.LEAF_NODE, 4);
    expect(node.type).toEqual(Node.LEAF_NODE);
  });

  it('Creates an Internal Node', () => {
    const node = new Node(Node.INTERNAL_NODE, 4);
    expect(node.type).toEqual(Node.INTERNAL_NODE);
  });

  it('Creates the correct size value and children arrays', () => {
    const N = 4;
    const node = new Node(Node.INTERNAL_NODE, N);
    expect(node.values.length).toEqual(2 * N);
    expect(node.children.length).toEqual(2 * N + 1);
    node.values.forEach((value) => expect(value).toBeUndefined());
    node.children.forEach((child) => expect(child).toBeUndefined());
  });

  it('InternalNode: destructor deletes all child nodes', () => {
    const N = 4;
    const node = new Node(Node.INTERNAL_NODE, N);
    node.used = 2 * N;
    for (let i = 0; i < node.children.length; i += 1) {
      node.children[i] = new Node(Node.LEAF_NODE, N);
    }
    node.destructor();
    node.children.forEach((childNode) => expect(childNode).toBeUndefined());
  });

  it('LeafNode: destructor does nothing', () => {
    const N = 4;
    const node = new Node(Node.LEAF_NODE, N);
    node.used = 2 * N;
    for (let i = 0; i < node.children.length; i += 1) {
      node.children[i] = new Node(Node.LEAF_NODE, N);
    }
    node.destructor();
    node.children.forEach((childNode) => expect(childNode).toBeInstanceOf(Node));
  });
});
