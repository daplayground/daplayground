import { v4 as uuidv4 } from 'uuid';

/**
 * A Node within the B+Tree
 */
export default class Node {
  /**
   * Node Type: Internal Node
   * Does not hold any data, just pointers to child nodes.
   * @type {number}
   */
  static INTERNAL_NODE = 1;

  /**
   * Node Type: Leaf Node
   * Holds the data, has no child nodes.
   * @type {number}
   */
  static LEAF_NODE = 2;

  /**
   * Node-Type (Internal Node, Leaf Node)
   * @type {number}
   */
  type;

  /**
   * Number of used Elements in Node
   * @type {number}
   */
  used;

  /**
   * Value-Array
   * size is 2*N
   * @type {number[]}
   */
  values = [];

  /**
   * ChildNode-Array
   * size is 2*N+1
   * @type {Node[]}
   */
  children = [];

  /**
   * Previous Leaf-Pointer
   * @type {Node}
   */
  previous;

  /**
   * Next Leaf-Pointer
   * @type {Node}
   */
  next;

  /**
   * @type {String}
   */
  id;

  /**
   * Constructor
   * @param {number} type Internal or Leaf Node
   * @param {number} N Order of the Tree
   */
  constructor(type, N) {
    this.type = type;
    this.used = 0;
    this.values = new Array(2 * N);
    this.children = new Array(2 * N + 1);
    this.previous = undefined;
    this.next = undefined;
    this.id = `N${uuidv4()}`;
  }

  /**
   * Destructor
   * Delete all child nodes.
   * JS does not have destructors, so we have to implement and call them ourselves.
   */
  destructor() {
    if (this.type === Node.INTERNAL_NODE) {
      for (let i = 0; i < this.used + 1; i += 1) {
        this.children[i].destructor();
        delete this.children[i];
      }
    }
  }
}
