/*
 Software License Agreement (BSD License)
 http://wwwlab.cs.univie.ac.at/~a1100570/webAD/
 Copyright (c), Volodimir Begy
 All rights reserved.
 Redistribution and use of this software in source and binary forms, with or without modification, are permitted provided that the following condition is met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
import Node from './BPlusNodeWebAD';
import TreeUtils from '../../utils/TreeUtils';
import GraphAction from '../../utils/GraphAction';
import Color from '../../utils/Color';
import TreeStructure from './TreeStructure';

export default class BPlusTree extends TreeStructure {
  root;

  order;

  actions;

  constructor(order) {
    super();
    this.root = new Node();
    this.order = order;
    this.actions = [];
  }

  remove(val) {
    // empty tree:
    if (this.root == undefined) {

    }

    // if root exists
    else {
      // find target leaf node first:
      let actNode = this.root;
      while (!actNode.is_leaf) {
        // find index of pointer leading to target:
        // assume its on first place
        var index = 0;
        // if not, iterate
        if (val >= actNode.keys[0]) {
          for (var i = 0; i < actNode.keys.length; i++) {
            if (val >= actNode.keys[i] && (actNode.keys[i + 1] == undefined || val < actNode.keys[i + 1])) {
              index = i + 1; // because pointer.length+1 == keys.length
              break;
            }
          }
        }
        actNode = actNode.pointers[index];
      }

      // if element not there
      let found = false;
      for (var i = 0; i < actNode.keys.length; i++) {
        if (actNode.keys[i] == val) found = true;
      }
      if (!found) return;

      // if no underflow will occur
      if (actNode == this.root) {
        for (var i = 0; i < actNode.keys.length; i++) {
          if (actNode.keys[i] == val) {
            actNode.keys.splice(i, 1); break;
          }
        }
      } else if (actNode.keys.length > this.order) {
        var indexVal = 0;
        for (var i = 0; i < actNode.keys.length; i++) {
          if (actNode.keys[i] == val) {
            indexVal = i; break;
          }
        }

        // if the same key is referenced by parent(=its first in the node) replace by next in the parent
        if (actNode.keys[0] == val && actNode != actNode.parent.pointers[0]) {
          var index = 1;
          for (var i = 1; i < actNode.parent.pointers.length; i++) {
            if (actNode.parent.pointers[i] == actNode) {
              index = i; break;
            }
          }
          actNode.parent.keys[index - 1] = actNode.keys[1];
        }

        var index = 0;
        for (var i = 0; i < actNode.keys.length; i++) {
          if (actNode.keys[i] == val) {
            index = i; break;
          }
        }
        actNode.keys.splice(index, 1);

        // if its most left value: replace till root
        if (actNode == actNode.parent.pointers[0] && indexVal == 0) {
          var gp = actNode.parent;
          while (gp != undefined) {
            for (var i = 0; i < gp.keys.length; i++) {
              if (gp.keys[i] == val) {
                gp.keys[i] = actNode.keys[0];
              }
            }
            gp = gp.parent;
          }
        }
      }

      // underflow
      else {
        // borrowing
        var index = 0;
        for (var i = 0; i < actNode.parent.pointers.length; i++) {
          if (actNode.parent.pointers[i] == actNode) {
            index = i; break;
          }
        }
        // try borrowing from left:
        if (index != 0 && actNode.parent.pointers[index - 1].keys.length > this.order) {
          // delete:
          var indexVal = 0;
          for (var i = 0; i < actNode.keys.length; i++) {
            if (actNode.keys[i] == val) {
              indexVal = i; break;
            }
          }
          actNode.keys.splice(indexVal, 1);

          // borrow the rightmost
          actNode.keys.splice(0, 0, actNode.parent.pointers[index - 1].keys[actNode.parent.pointers[index - 1].keys.length - 1]);
          actNode.keys.join();

          actNode.parent.pointers[index - 1].keys.splice(actNode.parent.pointers[index - 1].keys.length - 1, 1);
          actNode.parent.keys[index - 1] = actNode.keys[0];
        }

        // try borrowing from right:
        else if (index != actNode.parent.pointers.length - 1 && actNode.parent.pointers[index + 1].keys.length > this.order) {
          // delete:
          var indexVal = 0;
          for (var i = 0; i < actNode.keys.length; i++) {
            if (actNode.keys[i] == val) {
              indexVal = i; break;
            }
          }
          actNode.keys.splice(indexVal, 1);

          // borrow the leftmost
          actNode.keys.push(actNode.parent.pointers[index + 1].keys[0]);
          actNode.parent.pointers[index + 1].keys.splice(0, 1);

          actNode.parent.keys[index] = actNode.parent.pointers[index + 1].keys[0];
          actNode.parent.keys[index - 1] = actNode.keys[0];

          // if its most left value: replace till root
          if (actNode == actNode.parent.pointers[0] && indexVal == 0) {
            var gp = actNode.parent;
            while (gp != undefined) {
              for (var i = 0; i < gp.keys.length; i++) {
                if (gp.keys[i] == val) {
                  gp.keys[i] = actNode.keys[0];
                }
              }
              gp = gp.parent;
            }
          }
        }

        // merging
        else {
          // delete:
          var indexVal = 0;
          for (var i = 0; i < actNode.keys.length; i++) {
            if (actNode.keys[i] == val) {
              indexVal = i; break;
            }
          }
          actNode.keys.splice(indexVal, 1);

          // if act is most left
          if (actNode == actNode.parent.pointers[0]) {
            for (var i = 0; i < actNode.parent.pointers[1].keys.length; i++) {
              actNode.keys.push(actNode.parent.pointers[1].keys[i]);
              // no pointers to push into leaf
            }
            actNode.parent.keys.splice(0, 1);
            actNode.parent.pointers.splice(1, 1);

            // if its most left value: replace till root
            if (indexVal == 0) {
              var gp = actNode.parent;
              while (gp != undefined) {
                for (var i = 0; i < gp.keys.length; i++) {
                  if (gp.keys[i] == val) {
                    gp.keys[i] = actNode.keys[0];
                  }
                }
                gp = gp.parent;
              }
            }
          } else {
            var index = 0;
            for (var i = 0; i < actNode.parent.pointers.length; i++) {
              if (actNode.parent.pointers[i] == actNode) {
                index = i; break;
              }
            }

            for (var i = 0; i < actNode.keys.length; i++) {
              actNode.parent.pointers[index - 1].keys.push(actNode.keys[i]);
            }
            actNode.parent.keys.splice(index - 1, 1);
            actNode.parent.pointers.splice(index, 1);
            actNode = actNode.parent.pointers[index - 1];
          }

          if (actNode.parent == this.root && actNode.parent.keys.length == 0) {
            this.root = actNode; actNode.parent = undefined; return;
          }
          if ((actNode.parent != this.root && actNode.parent.keys.length >= this.order) || actNode.parent == this.root) {
            return;
          }

          actNode = actNode.parent;

          while (actNode.keys.length < this.order && actNode != this.root) {
            // borrowing from left
            var index = 0;
            for (var i = 0; actNode.parent.pointers.length; i++) {
              if (actNode.parent.pointers[i] == actNode) { index = i; break; }
            }

            if (index != 0 && actNode.parent.pointers[index - 1].keys.length > this.order) {
              actNode.keys.splice(0, 0, actNode.parent.keys[index - 1]);
              actNode.pointers.splice(0, 0, actNode.parent.pointers[index - 1].pointers[actNode.parent.pointers[index - 1].pointers.length - 1]);

              actNode.parent.keys[index - 1] = actNode.parent.pointers[index - 1].keys[actNode.parent.pointers[index - 1].keys.length - 1];

              actNode.parent.pointers[index - 1].keys.splice(actNode.parent.pointers[index - 1].keys.length - 1, 1);
              actNode.parent.pointers[index - 1].pointers[actNode.parent.pointers[index - 1].pointers.length - 1].parent = actNode;
              actNode.parent.pointers[index - 1].pointers.splice(actNode.parent.pointers[index - 1].pointers.length - 1, 1);
            }

            // borrowing from right
            else if (index != actNode.parent.pointers.length - 1 && actNode.parent.pointers[index + 1].keys.length > this.order) {
              actNode.keys.push(actNode.parent.keys[index]);
              actNode.pointers.push(actNode.parent.pointers[index + 1].pointers[0]);
              actNode.parent.pointers[index + 1].pointers[0].parent = actNode;

              actNode.parent.keys[index] = actNode.parent.pointers[index + 1].keys[0];

              actNode.parent.pointers[index + 1].keys.splice(0, 1);
              actNode.parent.pointers[index + 1].pointers.splice(0, 1);
            }

            // merge to left
            else if (index == 0) {
              // First the appropriate key from the parent is moved down to the left node
              actNode.keys.push(actNode.parent.keys[0]);
              actNode.parent.keys.splice(0, 1);

              // then all keys and pointers are moved from the right node
              for (var i = 0; i < actNode.parent.pointers[1].pointers.length; i++) {
                actNode.parent.pointers[1].pointers[i].parent = actNode;
              }
              actNode.keys = actNode.keys.concat(actNode.parent.pointers[1].keys);
              actNode.pointers = actNode.pointers.concat(actNode.parent.pointers[1].pointers);

              // Finally the pointer to the right node is removed
              actNode.parent.pointers.splice(1, 1);
            } else {
              // First the appropriate key from the parent is moved down to the left node
              actNode.parent.pointers[index - 1].keys.push(actNode.parent.keys[index - 1]);
              actNode.parent.keys.splice(index - 1, 1);

              // then all keys and pointers are moved from the right node
              for (var i = 0; i < actNode.pointers.length; i++) {
                actNode.pointers[i].parent = actNode.parent.pointers[index - 1];
              }
              actNode.parent.pointers[index - 1].keys = actNode.parent.pointers[index - 1].keys.concat(actNode.keys);
              actNode.parent.pointers[index - 1].pointers = actNode.parent.pointers[index - 1].pointers.concat(actNode.pointers);

              // Finally the pointer to the right node is removed
              actNode.parent.pointers.splice(index, 1);
            }
            actNode = actNode.parent;
            if (actNode == this.root && actNode.keys.length == 0) {
              this.root = actNode.pointers[0]; actNode = this.root; actNode.parent = undefined;
            }
          }
        }
      }
    }
  }

  addFixed(val) {
    // empty tree:
    if (this.root == undefined) {
      const root = new Node();
      root.keys[0] = val;
      this.root = root;
    }

    // if root exists
    else {
      // find target leaf node first:
      let actNode = this.root;
      while (!actNode.is_leaf) {
        // find index of pointer leading to target:
        // assume its on first place
        var index = 0;
        // if not, iterate
        if (val >= actNode.keys[0]) {
          for (var i = 0; i < actNode.keys.length; i++) {
            if (val >= actNode.keys[i] && (actNode.keys[i + 1] == undefined || val < actNode.keys[i + 1])) {
              index = i + 1; // because pointer.length+1 == keys.length
              break;
            }
          }
        }
        actNode = actNode.pointers[index];
      }

      for (var i = 0; i < actNode.keys.length; i++) {
        if (actNode.keys[i] == val) return;
      }

      // the target node has available space for one more key.
      if (actNode.keys.length < this.order * 2) {
        actNode.keys.push(val);
        actNode.keys.sort((a, b) => a - b);
      }

      // the target node is full and its root
      else if (actNode.keys.length == this.order * 2 && actNode == this.root) {
        // sibling leaf
        var rightSibling = new Node();

        // get all values to sort and split siblings
        var values = actNode.keys;
        values.push(val);
        values.sort((a, b) => a - b);

        var valuesLeftSibling = [];
        for (var i = 0; i < this.order; i++) {
          valuesLeftSibling.push(values[i]);
        }
        actNode.keys = valuesLeftSibling;

        var valuesRightSibling = [];
        for (var i = this.order; i <= this.order * 2; i++) {
          valuesRightSibling.push(values[i]);
        }
        rightSibling.keys = valuesRightSibling;

        var newRoot = new Node();
        newRoot.is_leaf = false;
        newRoot.keys.push(values[this.order]);
        newRoot.pointers[0] = actNode;
        actNode.parent = newRoot;
        newRoot.pointers[1] = rightSibling;
        rightSibling.parent = newRoot;
        this.root = newRoot;
      }

      // the target node is full, but its parent has space for one more key.
      else if (actNode.keys.length == this.order * 2 && actNode.parent != undefined && actNode.parent.keys.length < this.order * 2) {
        // window.alert("in3");
        // sibling leaf
        var rightSibling = new Node();

        // get all values to sort and split siblings
        var values = actNode.keys;
        values.push(val);
        values.sort((a, b) => a - b);

        var valuesLeftSibling = [];
        for (var i = 0; i < this.order; i++) {
          valuesLeftSibling.push(values[i]);
        }
        actNode.keys = valuesLeftSibling;

        var valuesRightSibling = [];
        for (var i = this.order; i <= this.order * 2; i++) {
          valuesRightSibling.push(values[i]);
        }
        rightSibling.keys = valuesRightSibling;

        // add middle key to parent: find index, then splice
        var index = 0;
        for (var i = 0; i < actNode.parent.keys.length; i++) {
          if (values[this.order] < actNode.parent.keys[i]) {
            index = i;
            break;
          } else if (i + 1 == actNode.parent.keys.length) {
            index = i + 1;
          }
        }

        actNode.parent.keys.push(values[this.order]);
        actNode.parent.keys.sort((a, b) => a - b);

        // update the right
        actNode.parent.pointers.splice([index + 1], 0, rightSibling);

        // set parent for new sibling
        rightSibling.parent = actNode.parent;
      }

      // the target node and its parent are both full
      else if (actNode.keys.length == this.order * 2 && actNode.parent != undefined && actNode.parent.keys.length == this.order * 2) {
        const finished = false;

        let oldLeft;
        let oldRight;
        var left = false;

        while (!finished) {
          // sibling leaf
          var rightSibling = new Node();
          // window.alert(actNode.keys);
          // get all values to sort and split siblings
          var values = actNode.keys;
          values.push(val);
          values.sort((a, b) => a - b);

          var valuesLeftSibling = [];
          for (var i = 0; i < this.order; i++) {
            valuesLeftSibling.push(values[i]);
          }
          actNode.keys = valuesLeftSibling;

          var valuesRightSibling = [];
          for (var i = this.order; i <= this.order * 2; i++) {
            // for internal split do not use the middle value for right sibling
            if (oldLeft != undefined) {
              if (i > this.order) valuesRightSibling.push(values[i]);
            } else valuesRightSibling.push(values[i]);
          }

          rightSibling.keys = valuesRightSibling;

          // adapt pointers
          if (oldLeft != undefined) {
            rightSibling.is_leaf = false;
            // update pointers to old & old
            // split pointers in half and add pointer to new sibling to the needed side
            const { pointers } = actNode;
            var pointersLeftParent = [];
            var pointersRightParent = [];

            // if middle leaf is split
            if (pointers[this.order] == oldLeft) {
              for (var i = 0; i <= this.order; i++) {
                pointersLeftParent.push(pointers[i]);
              }
              pointersRightParent.push(oldRight);
              oldRight.parent = rightSibling;
              for (var i = this.order + 1; i < pointers.length; i++) {
                pointersRightParent.push(pointers[i]);
                pointers[i].parent = rightSibling;
              }
            } else {
              var left = false;

              for (var i = 0; i < this.order; i++) {
                if (pointers[i] == oldLeft) left = true;
              }

              if (left) {
                for (var i = 0; i < this.order; i++) {
                  pointersLeftParent.push(pointers[i]);
                }
                var index = 0;
                for (var i = 0; i < pointersLeftParent.length; i++) {
                  if (pointersLeftParent[i] == oldLeft) { index = i; break; }
                }
                pointersLeftParent.splice(index + 1, 0, oldRight);
                pointersLeftParent.join();
                oldRight.parent = actNode;

                for (var i = this.order; i < pointers.length; i++) {
                  pointersRightParent.push(pointers[i]);
                  pointers[i].parent = rightSibling;
                }
              } else {
                for (var i = 0; i <= this.order; i++) {
                  pointersLeftParent.push(pointers[i]);
                }
                for (var i = this.order + 1; i < pointers.length; i++) {
                  pointersRightParent.push(pointers[i]);
                  pointers[i].parent = rightSibling;
                }

                var index = 0;
                for (var i = 0; i < pointersRightParent.length; i++) {
                  if (pointersRightParent[i] == oldLeft) {
                    index = i; break;
                  }
                }
                pointersRightParent.splice(index + 1, 0, oldRight);
                pointersRightParent.join();
                oldRight.parent = rightSibling;
              }
            }

            actNode.pointers = pointersLeftParent;
            rightSibling.pointers = pointersRightParent;
          }

          // if parent=root. cant enter here in 1st iteration!
          if (actNode == this.root) {
            actNode.pointers = pointersLeftParent;
            rightSibling.pointers = pointersRightParent;

            // make new root
            var newRoot = new Node();
            newRoot.is_leaf = false;

            // set pointers from new root to act and its sibling
            newRoot.keys.push(values[this.order]);
            newRoot.pointers.push(actNode);
            newRoot.pointers.push(rightSibling);

            actNode.parent = newRoot;
            rightSibling.parent = newRoot;

            this.root = newRoot;

            return;
          }

          // or if parent has free space for new index. cant enter here in 1st interation!
          if (actNode.parent != undefined && actNode.parent.keys.length < this.order * 2) {
            // insert into parent the middle + pointer
            actNode.parent.keys.push(values[this.order]);
            actNode.parent.keys.sort((a, b) => a - b);

            var index = 0;
            for (var i = 0; i < actNode.parent.pointers.length; i++) {
              if (actNode.parent.pointers[i] == actNode) {
                index = i + 1; break;
              } else if (i == actNode.parent.pointers.length - 1) {
                index = i + 1; break;
              }
            }
            actNode.parent.pointers.splice(index, 0, rightSibling);
            rightSibling.parent = actNode.parent;

            return;
          }

          // if parent!=root and has no free space
          oldLeft = actNode;
          oldRight = rightSibling;

          val = values[this.order];
          actNode = actNode.parent;
        }
      }
    }
  }

  /*
  search() {
    if (this.order < 0) {
      window.alert("Create the tree first!"); return;
    }
    //empty tree:
    if (this.root == undefined) {
      return;
    }

    //if root exists
    else {
      var val = parseInt(prompt("Search for:"));
      if (isNaN(val))
        return;
      //find target leaf node first:
      var actNode = this.root;
      actNode.color = "#FF8000";
      this.draw();

      function whileLoop(tree, actNode) {
        setTimeout(function () {
          //find index of pointer leading to target:
          //assume its on first place
          var index = 0;
          //if not, iterate
          if (val >= actNode.keys[0]) {
            for (var i = 0; i < actNode.keys.length; i++) {
              if (val >= actNode.keys[i] && (actNode.keys[i + 1] == undefined || val < actNode.keys[i + 1])) {
                index = i + 1; // because pointer.length+1 == keys.length
                break;
              }
            }
          }
          actNode.neededKid = index;
          actNode = actNode.pointers[index];
          actNode.color = "#FF8000";

          actNode.parent.color = "#ADFF2F";
          tree.draw();
          actNode.parent.neededKid = undefined;
          if (!actNode.is_leaf)
            whileLoop(tree, actNode);
          else {
            function notFound(tree) {
              setTimeout(function () {
                actNode.color = "#ADFF2F";
                actNode.parent.neededKid = undefined;
                tree.draw();
                return;
              }, 1000);
            }
            notFound(tree);
          }

        }, 1000);
      }

      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      function notFound(tree) {
        setTimeout(function () {
          actNode.color = "#ADFF2F";
          tree.draw();
          return;
        }, 1000);
      }

      if (!actNode.is_leaf)
        whileLoop(tree, actNode);
      else
        notFound(this);

    }
  }
*/
  search(value, onlyHighlightLeaf = false) {
    this.actions = [];
    if (this.root == undefined) {

    } else {
      this.addHighlight(this.root, onlyHighlightLeaf);
      if (this.root.keys.includes(value)) {
        this.addHighlight(this.root, onlyHighlightLeaf, Color.TREE_CURRENT_NODE_RESULT_TRUE);
      }
      if (this.root.pointers.length == 0) {
        return;
      }
      let currentNode = this.findNext(this.root, value);
      while (currentNode) {
        this.addHighlight(currentNode, onlyHighlightLeaf);
        if (currentNode.keys.includes(value)) {
          this.addHighlight(currentNode, onlyHighlightLeaf, Color.TREE_CURRENT_NODE_RESULT_TRUE);
        }
        if (currentNode.is_leaf) {
          return;
        }
        currentNode = this.findNext(currentNode, value);
      }
    }
  }

  findNext(node, value) {
    for (let i = 0; i < node.keys.length; i++) {
      if (value < node.keys[i]) {
        return node.pointers[i];
      }
      if (i == node.keys.length - 1 && node.pointers[i + 1] != undefined) {
        return node.pointers[i + 1];
      }
    }
    return undefined;
  }

  getActions() {
    return this.actions;
  }

  addHighlight(node, onlyHighlightLeaf, highlightColor = Color.TREE_CURRENT_NODE_SELECTED) {
    if (!onlyHighlightLeaf || (node.is_leaf && highlightColor !== Color.TREE_CURRENT_NODE_SELECTED)) {
      this.actions.push({
        do: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: highlightColor,
          elements: node.id,
        },
        undo: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.BASE_COLOR_TREE,
          elements: node.id,
        },
      });
    }
  }

  print(node = this.root, level = 0) {
    if (!node.isLeaf) {
      for (let i = 0; i < node.pointers.length; i++) {
        this.print(node.pointers[i], level + 1);
      }
    }
  }

  getAllNodes(rootNode) {
    const nodes = [];

    function traverse(node) {
      nodes.push(node);
      if (!node.is_leaf) {
        for (let i = 0; i < node.pointers.length; i++) {
          traverse(node.pointers[i]);
        }
      }
    }

    traverse(rootNode);
    return nodes;
  }

  getHeight(node = this.root) {
    if (node == undefined || node.is_leaf) {
      return 0;
    }
    return 1 + this.getHeight(node.pointers[0]);
  }
}

// [35]
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
