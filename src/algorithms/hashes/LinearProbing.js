import Action from '../../components/hashes/HashTableActions';
import Color from '../../utils/Color';
import HashStructure from './HashStructure';

/**
 * @param array
 * @returns {number}
 */
function getFillFactor(array) {
  return array.filter((e) => e.occupied).length / array.length;
}

/**
 * One Row of the hash table. Holds exactly 1 value.
 * @constructor
 */
function Row() {
  /**
   * The value currently stored in the Row.
   * @type {undefined}
   */
  this.value = undefined;

  /**
   * Indicator if there is currently a value stored in this row or not.
   * @type {boolean}
   */
  this.occupied = false;

  /**
   * Indicator if there has been a value that is now removed, to
   * continue later searches past an empty row.
   * @type {boolean}
   */
  this.extraCheck = false;
}

/**
 * LinearProbing is a hash collision resolution strategy where the first free position after the
 * determined position is used to insert the element.
 *
 * This implementation features lazy deletion, where after the removal of a key the hash table is
 * not reorganized, but instead a flag is set that at the position there was a deleted value and
 * later searches should continue past this empty slot.
 * Extending the table then reorganizes the values to get rid of all deleted values that still
 * occupy space in the hash table.
 */
export default class LinearProbing extends HashStructure {
  /**
   * At what fill factor should the hash table be extended to accommodate more values?
   * @type {number}
   */
  static MAX_FILL_FACTOR = 0.7;

  /**
   * By how much should an existing hash table be extended if the max fill factor is reached?
   * @type {number}
   */
  static EXTEND_FACTOR = 1.3;

  /**
   * An array of row objects representing the current state of the hash table.
   * @type {Row[]}
   */
  rows = [];

  /**
   * The current fill factor of the hash table.
   * @type {number}
   */
  fillFactor = 0;

  /**
   * A dynamic hash table extends itself automatically if the specified fill factor is reached.
   * Use init(size, false) to disable and thus provoke more hash collisions.
   * @type {boolean}
   */
  dynamic = true;

  /**
   * Initialize the hash table.
   * @param {number} size
   * @param {boolean} dynamic
   */
  init(size = 9, dynamic = false) {
    const steps = [];

    // initialize all rows with a new Row-Object
    this.rows = [];
    for (let i = 0; i < size; i += 1) {
      this.rows.push(new Row());
    }
    this.fillFactor = 0;
    this.dynamic = dynamic;

    steps.push({
      do: [{
        action: Action.CREATE_LINEAR_LIST,
        size,
      }],
      undo: [{
        action: Action.DELETE_LINEAR_LIST,
      }],
    });
    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: 'Address = value % length = ?',
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: 'Address = value % length = ?',
      }],
    });
    steps.push({
      do: [{
        action: Action.SET_FILL_FACTOR,
        value: 0,
      }],
      undo: [{
        action: Action.SET_FILL_FACTOR,
        value: 'N/A',
      }],
    });

    return steps;
  }

  /**
   * The hash function that is used to determine the position of a given value.
   * @param value
   * @returns {number}
   */
  hash(value) {
    return value % this.rows.length;
  }

  /**
   * Extend the hash table to accommodate more values.
   */
  extend() {
    const steps = [];

    const oldValues = this.rows
      .filter((e) => e.occupied)
      .map((e) => e.value);

    const newSize = Math.trunc(this.rows.length * LinearProbing.EXTEND_FACTOR);

    steps.push({
      do: [{
        action: Action.UNSET_EXTRA_CHECK,
        index: null,
      }],
      undo: [{
        action: Action.SET_EXTRA_CHECK,
      }],
    });

    steps.push({
      do: [{
        action: Action.DELETE_LINEAR_LIST,
        size: newSize,
      }],
      undo: [{
        action: Action.CREATE_LINEAR_LIST,
        size: this.rows.length,
      }],
    });

    steps.push({
      do: [{
        action: Action.CREATE_LINEAR_LIST,
        size: newSize,
      }],
      undo: [{
        action: Action.DELETE_LINEAR_LIST,
        size: this.rows.length,
      }],
    });

    steps.push({
      do: [{
        action: Action.SET_FILL_FACTOR,
        value: 0,
      }],
      undo: [{
        action: Action.SET_FILL_FACTOR,
        value: this.fillFactor,
      }],
    });

    this.rows = [];
    for (let i = 0; i < newSize; i += 1) {
      this.rows.push(new Row());
    }

    this.fillFactor = 0;

    oldValues.forEach((oldValue) => {
      const insertSteps = this.insert(oldValue);
      steps.push(...insertSteps);
    });

    return steps;
  }

  /**
   * Insert the given value into the hash table.
   * @param value
   * @return {Object[]}
   */
  insert(value) {
    const steps = [];

    if (this.rows.length === 0) {
      // No table created
      steps.push({
        do: [{
          action: Action.NOOP,
          text: 'Create a new hash table to insert a value.',
        }],
        undo: [{
          action: Action.NOOP,
          text: 'Create a new hash table to insert a value.',
        }],
      });
      return steps;
    }

    if (this.fillFactor === 1) {
      // Table full and static
      steps.push({
        do: [{
          action: Action.NOOP,
          text: 'Table is full and static, cannot insert the given value.',
        }],
        undo: [{
          action: Action.NOOP,
          text: 'Table is full and static, cannot insert the given value.',
        }],
      });
      return steps;
    }

    /** @type {number} */
    let address = this.hash(value);

    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = ${value} % ${this.rows.length} = ${address}`,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = value % ${this.rows.length} = ?`,
      }],
    });

    steps.push({
      do: [{
        action: Action.SET_POINTER,
        index: address,
        text: `Set pointer at address ${address}.`,
      }],
      undo: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
    });

    let inserted = false;
    while (!inserted) {
      if (this.rows[address].occupied) {
        // position is occupied.
        const undoAction = this.rows[address].extraCheck
          ? Action.SET_EXTRA_CHECK
          : Action.UNSET_EXTRA_CHECK;

        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_COLLISION,
            text: `Highlight collision at address ${address}.`,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });

        steps.push({
          do: [{
            action: Action.SET_EXTRA_CHECK,
            index: address,
          }],
          undo: [{
            action: undoAction,
            index: address,
          }],
        });

        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
            text: `Unhighlight at address ${address}.`,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });

        this.rows[address].extraCheck = true;

        // linear probe:
        const prevAddress = address; // needed for undo action
        address += 1;
        if (address === this.rows.length) {
          // roll over
          address = 0;
        }
        // We only need to check whether we reached the initial position if the list is full.
        // Because we extend at 70% fill rate already, the hash table is never full
        // and we won't ever reach the initial position.

        steps.push({
          do: [{
            action: Action.MOVE_POINTER,
            index: address,
          }],
          undo: [{
            action: Action.MOVE_POINTER,
            index: prevAddress,
          }],
        });
      } else {
        // found a position - insert the value.

        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
            text: `Found empty position at address ${address}.`,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });

        steps.push({
          do: [{
            action: Action.SET_VALUE,
            index: address,
            value,
            text: `Insert value ${value} at address ${address}.`,
          }],
          undo: [{
            action: Action.SET_VALUE,
            index: address,
            value: this.rows[address].value,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
            text: `Unhighlight inserted element at ${address}.`,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
        });

        this.rows[address].value = value;
        this.rows[address].occupied = true;
        inserted = true;
        steps.push({
          do: [{
            action: Action.UNSET_POINTER,
            index: address,
          }],
          undo: [{
            action: Action.SET_POINTER,
            index: address,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_FILL_FACTOR,
            value: getFillFactor(this.rows),
          }],
          undo: [{
            action: Action.SET_FILL_FACTOR,
            value: this.fillFactor,
          }],
        });
        this.fillFactor = getFillFactor(this.rows);
        if (this.fillFactor > LinearProbing.MAX_FILL_FACTOR && this.dynamic) {
          steps.push({
            do: [{
              action: Action.NOOP,
              text: 'Maximum fill factor reached, need to extend hash table.',
            }],
            undo: [{
              action: Action.NOOP,
              text: 'Maximum fill factor reached, need to extend hash table.',
            }],
          });
          const extendSteps = this.extend();
          steps.push(...extendSteps);
        }
      }
    }

    return steps;
  }

  /**
   * Search for the given value in the hash table.
   * @param {number} value
   * @return {Object[]}
   */
  search(value) {
    const steps = [];

    if (this.rows.length === 0) {
      // no table created
      steps.push({
        do: [{
          action: Action.NOOP,
          text: 'Create a new hash table to search for a value.',
        }],
        undo: [{
          action: Action.NOOP,
          text: 'Create a new hash table to search for a value.',
        }],
      });
      return steps;
    }

    const initialAddress = this.hash(value);
    let address = initialAddress;

    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = ${value} % ${this.rows.length} = ${address}`,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = value % ${this.rows.length} = ?`,
      }],
    });

    steps.push({
      do: [{
        action: Action.SET_POINTER,
        index: address,
        text: `Set pointer at address ${address}.`,
      }],
      undo: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
    });

    let found = false;
    while (!found) {
      if (this.rows[address].occupied && this.rows[address].value === value) {
        // found value, done.
        found = true;
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });
        break;
      }

      if (this.rows[address].extraCheck === false) {
        // not occupied or wrong value, and no indication to continue search
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_COLLISION,
            text: 'No extra check.',
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
            text: 'No extra check.',
          }],
        });
        break;
      }

      // needed for undo action
      const prevAddress = address;
      address += 1;

      if (address === this.rows.length) {
        // wrap around
        address = 0;
      }

      if (address === initialAddress) {
        // reached beginning, not found
        steps.push({
          do: [{
            action: Action.NOOP,
            text: 'Value not found - Searched the entire hash table.',
          }],
          undo: [{
            action: Action.NOOP,
          }],
        });
        break;
      }

      steps.push({
        do: [{
          action: Action.MOVE_POINTER,
          index: address,
        }],
        undo: [{
          action: Action.MOVE_POINTER,
          index: prevAddress,
        }],
      });
    }

    steps.push({
      do: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
      undo: [{
        action: Action.SET_POINTER,
        index: address,
      }],
    });

    if (found) {
      // found
      steps.push({
        do: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.BASE_COLOR_HASH_TABLE,
          text: `The value ${value} was found in the hash table at address ${address}.`,
        }],
        undo: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.HASH_INSERT_SEARCH_COMPLETED,
          text: `The value ${value} was found in the hash table at address ${address}.`,
        }],
      });
    } else {
      // not found
      steps.push({
        do: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.BASE_COLOR_HASH_TABLE,
          text: `The value ${value} was not fund in the hash table.`,
        }],
        undo: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.HASH_COLLISION,
          text: `The value ${value} was not fund in the hash table.`,
        }],
      });
    }

    return steps;
  }

  /**
   * Remove the given value from the hash table.
   * @param value
   * @return {{}[]}
   */
  remove(value) {
    const steps = [];

    if (this.rows.length === 0) {
      // no table created
      steps.push({
        do: [{
          action: Action.NOOP,
          text: 'Create a new hash table to delete a value.',
        }],
        undo: [{
          action: Action.NOOP,
          text: 'Create a new hash table to delete a value.',
        }],
      });
      return steps;
    }

    const initialAddress = this.hash(value);
    let address = initialAddress;

    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = ${value} % ${this.rows.length} = ${address}`,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = value % ${this.rows.length} = ?`,
      }],
    });

    steps.push({
      do: [{
        action: Action.SET_POINTER,
        index: address,
        text: `Set pointer at address ${address}.`,
      }],
      undo: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
    });

    let found = false;
    while (!found) {
      if (this.rows[address].occupied && this.rows[address].value === value) {
        // found value, remove, done.
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,

          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_VALUE,
            index: address,
            value: null,
          }],
          undo: [{
            action: Action.SET_VALUE,
            index: address,
            value,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });
        this.rows[address].value = undefined;
        this.rows[address].occupied = false;
        this.rows[address].extraCheck = true;
        steps.push({
          do: [{
            action: Action.SET_FILL_FACTOR,
            value: getFillFactor(this.rows),
          }],
          undo: [{
            action: Action.SET_FILL_FACTOR,
            value: this.fillFactor,
          }],
        });
        this.fillFactor = getFillFactor(this.rows);
        found = true;
        break;
      }

      if (this.rows[address].extraCheck === false) {
        // not occupied or wrong value, and no indication to continue search
        steps.push({
          do: [{
            action: Action.NOOP,
            text: 'No extra check.',
          }],
          undo: [{
            action: Action.NOOP,
            text: 'No extra check.',
          }],
        });
        break;
      }

      const prevAddress = address; // needed for undo action
      address += 1;

      if (address === this.rows.length) {
        // wrap around
        address = 0;
      }

      if (address === initialAddress) {
        // reached beginning, not found
        steps.push({
          do: [{
            action: Action.NOOP,
            text: 'Searched the entire hash table.',
          }],
          undo: [{
            action: Action.NOOP,
            text: 'Searched the entire hash table.',
          }],
        });
        break;
      }

      steps.push({
        do: [{
          action: Action.MOVE_POINTER,
          index: address,
        }],
        undo: [{
          action: Action.MOVE_POINTER,
          index: prevAddress,
        }],
      });
    }

    steps.push({
      do: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
      undo: [{
        action: Action.SET_POINTER,
        index: address,
      }],
    });

    if (found) {
      // removed
      steps.push({
        do: [{
          action: Action.NOOP,
          text: `The value ${value} was found at address ${address} and removed from the hash table.`,
        }],
        undo: [{
          action: Action.NOOP,
          text: `The value ${value} was found at address ${address} and removed from the hash table.`,
        }],
      });
    } else {
      // not removed
      steps.push({
        do: [{
          action: Action.NOOP,
          text: `The value ${value} was not fund in the hash table and therefore not removed.`,
        }],
        undo: [{
          action: Action.NOOP,
          text: `The value ${value} was not fund in the hash table and therefore not removed.`,
        }],
      });
    }

    return steps;
  }
}
