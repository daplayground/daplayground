import Action from '../../components/hashes/HashTableVerticalActions';
import Color from '../../utils/Color';
import HashStructure from './HashStructure';

/**
 * Row. Holds one value and maybe the next object in a chain.
 * @constructor
 */
function Row() {
  /**
   * The value stored in this row.
   * @type {number}
   */
  this.value = undefined;

  /**
   * The next row in the chain.
   * @type {Row}
   */
  this.next = undefined;
}

/**
 * SeparateChaining
 *
 * SeparateChaining is a closed addressing scheme.
 *
 * input params: size
 */
export default class SeparateChaining extends HashStructure {
  /**
   * At what fill factor should the hash table be extended to accommodate more values?
   * @type {number}
   */
  static MAX_FILL_FACTOR = 2.5;

  /**
   * By how much should an existing hash table be extended if the max fill factor is reached?
   * @type {number}
   */
  static EXTEND_FACTOR = 1.3;

  /**
   * The hash function.
   * @param data
   * @returns {number}
   */
  hash(data) {
    return data % this.rows.length;
  }

  /**
   * Create a new, empty hash table.
   * Initialize the hash table.
   * @param size
   * @param dynamic
   */
  init(size = 10, dynamic = false) {
    const steps = [];

    // create an empty list
    this.numItems = 0; // number of items in the hash table
    this.calc = `value % ${size} = address`;
    this.dynamic = dynamic;
    steps.push({
      do: [
        {
          action: Action.CREATE_LIST,
          rows: size,
        },
        {
          action: Action.SET_FILL_FACTOR,
          value: 0,
        },
        {
          action: Action.SET_HASH_CALCULATION,
          text: this.calc,
        },
      ],
      undo: [
        {
          action: Action.DELETE_LIST,
        },
        {
          action: Action.SET_FILL_FACTOR,
          value: null,
        },
        {
          action: Action.SET_HASH_CALCULATION,
          text: null,
        },
      ],
    });

    // fill initial list with mod numbers
    this.rows = [];
    const doActions = [];
    const undoActions = [];
    for (let i = 0; i < size; i += 1) {
      this.rows.push(undefined);
      doActions.push({
        action: Action.SET_ROW_VALUE,
        row: i,
        value: `x % ${i}`,
      });
      undoActions.push({
        action: Action.SET_ROW_VALUE,
        row: i,
        value: null,
      });
    }
    steps.push({
      do: doActions,
      undo: undoActions,
    });

    return steps;
  }

  /**
   * Extend the hash table to accommodate more values.
   */
  extend() {
    const steps = [];

    const oldRows = Array
      .from(this.rows)
      .filter((r) => r !== undefined);

    // initialize a bigger empty hash table
    const initSteps = this.init(Math.ceil(this.rows.length * SeparateChaining.EXTEND_FACTOR));
    steps.push(...initSteps);

    // go along each chain and insert all values into the new hash table
    oldRows.forEach((row) => {
      let currentRow = row;
      let reachedEnd = false;
      while (!reachedEnd) {
        const insertSteps = this.insert(currentRow.value);
        steps.push(...insertSteps);
        if (currentRow.next !== undefined) {
          currentRow = currentRow.next;
        } else {
          reachedEnd = true;
        }
      }
    });

    return steps;
  }

  /**
   * Insert the given value into the hash table.
   * @param value
   */
  insert(value) {
    const steps = [];

    // if the hash table is nearly full, extend it
    if (this.dynamic && this.numItems + 1 > this.rows.length * SeparateChaining.MAX_FILL_FACTOR) {
      const extendSteps = this.extend();
      steps.push(...extendSteps);
    }

    // calculate the address for the given value
    const address = this.hash(value);
    const undoText = this.calc;
    this.calc = `${value} % ${this.rows.length} = ${address}`;
    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: this.calc,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: undoText,
      }],
    });

    // highlight the calculated row
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.HASH_INSERT_SEARCH_COMPLETED,
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    });

    // insert new row at the beginning of the given address
    const newRow = new Row();
    newRow.value = value;
    newRow.next = this.rows[address];
    this.rows[address] = newRow;
    steps.push({
      do: [{
        action: Action.INSERT_VALUE_AT_POS,
        row: address,
        pos: 0,
        value: newRow.value,
      }],
      undo: [{
        action: Action.REMOVE_VALUE_AT_POS,
        row: address,
        pos: 0,
      }],
    });

    // increase the total number of elements stored
    this.numItems += 1;
    steps.push({
      do: [{
        action: Action.SET_FILL_FACTOR,
        value: this.numItems / this.rows.length,
      }],
      undo: [{
        action: Action.SET_FILL_FACTOR,
        value: (this.numItems - 1) / this.rows.length,
      }],
    });

    // clear highlighting
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.HASH_INSERT_SEARCH_COMPLETED,
      }],
    });
    return steps;
  }

  /**
   * Search the given value in the hash table.
   * @param value
   */
  search(value) {
    const steps = [];

    // calculate the address for the given value
    const address = this.hash(value);
    const undoText = this.calc;
    this.calc = `${value} % ${this.rows.length} = ${address}`;

    // set hash calculation
    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: this.calc,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: undoText,
      }],
    });

    // highlight row
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.HASH_INSERT_SEARCH_COMPLETED,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        row: address,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    });
   

    let found = false;
    let currentRow = this.rows[address];
    let i = 0;
    if(currentRow === undefined) {
      steps.push(this.resetAddressColor(address));
      return steps;
    }

    // set border of first element in chain
    steps.push({
      do: [{
        action: Action.SET_CELL_COLOR,
        row: address,
        col: i,
        color: Color.HASH_COMPARE,
      }],
      undo: [{
        action: Action.SET_CELL_COLOR,
        row: address,
        col: i,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    });

    // walk along the chain of elements at the address of the given value
    while (!found) {
      if (currentRow.value === value) {
        found = true;
        // highlight found element
        steps.push({
          do: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
          undo: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });
        // reset color of found element
        steps.push({
          do: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
          undo: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
        });
      } else if (currentRow.next === undefined) {
        steps.push({
          do: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_COLLISION,
          }],
          undo: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_COMPARE,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
          undo: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_COLLISION,
          }],
        });
        break;
      } else {
        // go to next element of chain
        currentRow = currentRow.next;
        i += 1; // needed for viz
        steps.push({
          do: [
            {
              action: Action.SET_CELL_COLOR,
              row: address,
              col: i - 1,
              color: Color.BASE_COLOR_HASH_TABLE,
            },
            {
              action: Action.SET_CELL_COLOR,
              row: address,
              col: i,
              color: Color.HASH_COMPARE,
            },
          ],
          undo: [
            {
              action: Action.SET_CELL_COLOR,
              row: address,
              col: i - 1,
              color: Color.HASH_COMPARE,
            },
            {
              action: Action.SET_CELL_COLOR,
              row: address,
              col: i,
              color: Color.BASE_COLOR_HASH_TABLE,
            },
          ],
        });
      }
    }

    // reset cell stroke width
    steps.push({
      do: [{
        action: Action.SET_CELL_STROKE_WIDTH,
        row: address,
        col: i,
        value: 1,
      }],
      undo: [{
        action: Action.SET_CELL_STROKE_WIDTH,
        row: address,
        col: i,
        value: 3,
      }],
    });
    steps.push(this.resetAddressColor(address));
    return steps;
  }

  /**
   * Remove the given value from the hash table.
   * @param value
   */
  remove(value) {
    const steps = [];
    if (this.numItems === 0) {
      return steps;
    }
    // calculate the address for the given value
    const address = this.hash(value);
    const undoText = this.calc;
    this.calc = `${value} % ${this.rows.length} = ${address}`;

    // set hash calculation
    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: this.calc,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: undoText,
      }],
    });

    // highlight row
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.HASH_INSERT_SEARCH_COMPLETED,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        row: address,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    });

    let removed = false;
    let currentRow = this.rows[address];
    let previousRow;
    let i = 0;
    if (currentRow === undefined) {
      steps.push(this.resetAddressColor(address));
      return steps;
    }
    steps.push({
      do: [{
        action: Action.SET_CELL_COLOR,
        row: address,
        col: i,
        color: Color.HASH_COMPARE,
      }],
      undo: [{
        action: Action.SET_CELL_COLOR,
        row: address,
        col: i,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    });
    // walk along the chain of elements at the address of the given value
    while (!removed) {
      if (currentRow.value === value) {
        // found, remove
        steps.push({
          do: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
          undo: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });
        steps.push({
          do: [{
            action: Action.REMOVE_VALUE_AT_POS,
            row: address,
            pos: i,
          }],
          undo: [{
            action: Action.INSERT_VALUE_AT_POS,
            row: address,
            pos: i,
            value,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_FILL_FACTOR,
            value: (this.numItems - 1) / this.rows.length,
          }],
          undo: [{
            action: Action.SET_FILL_FACTOR,
            value: this.numItems / this.rows.length,
          }],
        });
        if (previousRow === undefined) {
          this.rows[address] = currentRow.next;
        } else {
          previousRow.next = currentRow.next;
        }
        this.numItems -= 1;
        removed = true;
        break;
      } else if (currentRow.next === undefined) {
        steps.push({
          do: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_COLLISION,
          }],
          undo: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_COMPARE,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
          undo: [{
            action: Action.SET_CELL_COLOR,
            row: address,
            col: i,
            color: Color.HASH_COLLISION,
          }],
        });
        break;
      } else {
        // go to next element of chain
        previousRow = currentRow;
        currentRow = currentRow.next;
        i += 1; // needed for viz
        steps.push({
          do: [
            {
              action: Action.SET_CELL_COLOR,
              row: address,
              col: i - 1,
              color: Color.BASE_COLOR_HASH_TABLE,
            },
            {
              action: Action.SET_CELL_COLOR,
              row: address,
              col: i,
              color: Color.HASH_COMPARE,
            },
          ],
          undo: [
            {
              action: Action.SET_CELL_COLOR,
              row: address,
              col: i - 1,
              color: Color.HASH_COMPARE,
            },
            {
              action: Action.SET_CELL_COLOR,
              row: address,
              col: i,
              color: Color.BASE_COLOR_HASH_TABLE,
            },
          ],
        });
      }
    }
    steps.push(this.resetAddressColor(address));
    return steps;
  }
  resetAddressColor(address) {
    return {
      do: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.HASH_INSERT_SEARCH_COMPLETED,
      }],
    };
  }
}
