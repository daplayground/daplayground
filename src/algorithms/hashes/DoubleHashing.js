import { getPreviousPrime, getNextPrime } from '../../utils/prime';
import Action from '../../components/hashes/HashTableActions';
import Color from '../../utils/Color';
import HashStructure from './HashStructure';

/**
 * @param {Array} array
 * @returns {number}
 */
function getFillFactor(array) {
  return array.filter((row) => row.occupied).length / array.length;
}

/**
 * One Row of the hash table. Holds exactly 1 value.
 * @constructor
 */
function Row() {
  /**
   * The value currently stored in the Row.
   * @type {undefined}
   */
  this.value = undefined;

  /**
   * Indicator if there is currently a value stored in this row or not.
   * @type {boolean}
   */
  this.occupied = false;

  /**
   * Indicator if there has been a value that is now removed, to
   * continue later searches past an empty row.
   * @type {boolean}
   */
  this.extraCheck = false;
}

/**
 * DoubleHashing.
 */
export default class DoubleHashing extends HashStructure {
  /**
   * At what fill factor should the hash table be extended to accommodate more values?
   * @type {number}
   */
  static MAX_FILL_FACTOR = 0.75;

  /**
   * @type {Row[]}
   */
  rows = [];

  /**
   * @type {number}
   */
  prime = undefined;

  /**
   * @type {boolean}
   */
  dynamic = true;

  /**
   * @type {number}
   */
  fillFactor = 0;

  /**
   * Initialize an empty hash table.
   * @param size
   * @param dynamic
   */
  init(size = 4, dynamic = false) {
    const steps = [];

    this.rows = [];
    this.dynamic = dynamic;
    this.fillFactor = 0;

    for (let i = 0; i < size; i += 1) {
      this.rows.push(new Row());
    }

    this.prime = getPreviousPrime(this.rows.length - 1);

    steps.push({
      do: [{
        action: Action.CREATE_LINEAR_LIST,
        size,
      }],
      undo: [{
        action: Action.DELETE_LINEAR_LIST,
      }],
    });
    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: 'Address = value % length = ?',
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: 'Address = value % length = ?',
      }],
    });
    steps.push({
      do: [{
        action: Action.SET_FILL_FACTOR,
        value: 0,
      }],
      undo: [{
        action: Action.SET_FILL_FACTOR,
        value: 'N/A',
      }],
    });

    return steps;
  }

  /**
   * The hash function.
   * @param {number} value
   * @param {undefined|number} index
   */
  hash(value, index = undefined) {
    // hash function 1: value mod (size of the hash table)
    if (index === undefined) {
      return value % this.rows.length;
    }

    // hash function 2: prime minus value mod prime
    // prime is always smaller than the length of the hash table
    return (index + (this.prime - (value % this.prime))) % this.rows.length;
  }

  /**
   * Extend the hash table to accommodate more values.
   * @return {{}[]}
   */
  extend() {
    const steps = [];

    const oldValues = this.rows
      .filter((row) => row.occupied)
      .map((row) => row.value);

    const newSize = getNextPrime(this.rows.length + 1);

    steps.push({
      do: [{
        action: Action.UNSET_EXTRA_CHECK,
        index: null,
      }],
      undo: [{
        action: Action.SET_EXTRA_CHECK,
      }],
    });

    steps.push({
      do: [{
        action: Action.DELETE_LINEAR_LIST,
        size: newSize,
      }],
      undo: [{
        action: Action.CREATE_LINEAR_LIST,
        size: this.rows.length,
      }],
    });

    steps.push({
      do: [{
        action: Action.CREATE_LINEAR_LIST,
        size: newSize,
      }],
      undo: [{
        action: Action.DELETE_LINEAR_LIST,
        size: this.rows.length,
      }],
    });

    steps.push({
      do: [{
        action: Action.SET_FILL_FACTOR,
        value: 0,
      }],
      undo: [{
        action: Action.SET_FILL_FACTOR,
        value: this.fillFactor,
      }],
    });

    this.rows = [];
    for (let i = 0; i < newSize; i += 1) {
      this.rows.push(new Row());
    }

    this.prime = getPreviousPrime(this.rows.length - 1);

    this.fillFactor = 0;

    oldValues.forEach((oldValue) => {
      const insertSteps = this.insert(oldValue);
      steps.push(...insertSteps);
    });

    return steps;
  }

  /**
   * Insert the given value into the hash table.
   * @param {number} value
   */
  insert(value) {
    const steps = [];

    if (this.fillFactor === 1) {
      // Table full and static
      steps.push({
        do: [{
          action: Action.NOOP,
          text: 'Hash table is full and static.',
        }],
        undo: [{
          action: Action.NOOP,
          text: 'Hash table is full and static.',
        }],
      });
      return steps;
    }

    if (this.rows.length === 0) {
      // No table created
      steps.push({
        do: [{
          action: Action.NOOP,
          text: 'No hash table. Create a hash table first before inserting.',
        }],
        undo: [{
          action: Action.NOOP,
          text: 'No hash table. Create a hash table first before inserting.',
        }],
      });
      return steps;
    }

    /** @type {number} */
    let address = this.hash(value);

    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = ${value} % ${this.rows.length} = ${address}`,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = value % ${this.rows.length} = ?`,
      }],
    });

    steps.push({
      do: [{
        action: Action.SET_POINTER,
        index: address,
      }],
      undo: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
    });

    /** @type {number[]} */
    const visitedIndexes = [];

    let inserted = false;
    while (!inserted) {
      if (this.rows[address].occupied) {
        // current address is occupied, continue search for next place.

        // check if we already visited the current address:
        // If we arrive at the same place, we are going to go in a circle forever, so we stop.
        if (visitedIndexes.includes(address)) {
          break;
        }

        // add current address to visited addresses
        visitedIndexes.push(address);

        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_COLLISION,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });

        // mark address
        const undoAction = this.rows[address].extraCheck
          ? Action.SET_EXTRA_CHECK
          : Action.UNSET_EXTRA_CHECK;
        steps.push({
          do: [{
            action: Action.SET_EXTRA_CHECK,
            index: address,
          }],
          undo: [{
            action: undoAction,
            index: address,
          }],
        });
        this.rows[address].extraCheck = true;

        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_COLLISION,
          }],
        });

        // needed for undo action
        const previousAddress = address;

        // The double hashing trick:
        // get a new address by adding the value of a
        // second hash function to the previous address
        address = this.hash(value, address);

        steps.push({
          do: [{
            action: Action.MOVE_POINTER,
            index: address,
          }],
          undo: [{
            action: Action.MOVE_POINTER,
            index: previousAddress,
          }],
        });
      } else {
        // found a place, insert, done.
        this.rows[address].value = value;
        this.rows[address].occupied = true;
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });
        steps.push({
          do: [{
            action: Action.UNSET_POINTER,
            index: address,
          }],
          undo: [{
            action: Action.SET_POINTER,
            index: address,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_VALUE,
            index: address,
            value,
          }],
          undo: [{
            action: Action.SET_VALUE,
            index: address,
            value: null,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
        });

        // needed for undo action
        const previousFillFactor = this.fillFactor;
        this.fillFactor = getFillFactor(this.rows);
        steps.push({
          do: [{
            action: Action.SET_FILL_FACTOR,
            value: this.fillFactor,
          }],
          undo: [{
            action: Action.SET_FILL_FACTOR,
            value: previousFillFactor,
          }],
        });
        if (this.dynamic && this.fillFactor > DoubleHashing.MAX_FILL_FACTOR) {
          steps.push({
            do: [{
              action: Action.NOOP,
              text: 'Maximum fill factor reached, need to extend hash table.',
            }],
            undo: [{
              action: Action.NOOP,
              text: 'Maximum fill factor reached, need to extend hash table.',
            }],
          });
          const extendSteps = this.extend();
          steps.push(...extendSteps);
        }
        inserted = true;
      }
    }

    return steps;
  }

  /**
   * Search for the given value in the hash table.
   * @param {number} value
   * @return {{}[]}
   */
  search(value) {
    const steps = [];

    if (this.rows.length === 0) {
      // No table created
      steps.push({
        do: [{
          action: Action.NOOP,
          text: 'No hash table. Create a hash table first before searching.',
        }],
        undo: [{
          action: Action.NOOP,
          text: 'No hash table. Create a hash table first before searching.',
        }],
      });
      return steps;
    }

    let address = this.hash(value);

    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = ${value} % ${this.rows.length} = ${address}`,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = value % ${this.rows.length} = ?`,
      }],
    });

    steps.push({
      do: [{
        action: Action.SET_POINTER,
        index: address,
      }],
      undo: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
    });

    const visitedAddresses = [];
    let counter = 0;
    let found = false;
    while (!found) {
      if (this.rows[address].occupied && this.rows[address].value === value) {
        found = true;
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });
        break;
      }

      // empty or wrong value, and no indication to continue search
      if (this.rows[address].extraCheck === false) {
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_COLLISION,
            text: 'No extra check.',
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
            text: 'No extra check.',
          }],
        });
        break;
      }

      // check if we already visited the current address:
      // If we arrive at the same place, we are going to go in a circle forever, so we stop.
      if (visitedAddresses.includes(address)) {
        steps.push({
          do: [{
            action: Action.NOOP,
            text: 'Searched all possible addresses in the hash table.',
          }],
          undo: [{
            action: Action.NOOP,
            text: 'Searched all possible addresses in the hash table.',
          }],
        });
        break;
      }

      // increase iteration counter
      counter += 1;
      if (counter === this.rows.length) {
        // we looked at the entire hash table and found nothing, stop.
        steps.push({
          do: [{
            action: Action.NOOP,
            text: 'Searched the entire hash table.',
          }],
          undo: [{
            action: Action.NOOP,
            text: 'Searched the entire hash table.',
          }],
        });
        break;
      }

      // add current address to visited addresses
      visitedAddresses.push(address);

      // needed for undo action
      const prevAddress = address;

      // the double hashing trick
      address = this.hash(value, address);

      steps.push({
        do: [{
          action: Action.SET_HASH_CALCULATION,
          text: `Address = (${prevAddress} + (${this.prime} - (${value} % ${this.prime}))) % ${this.rows.length} = ${address}`,
        }],
        undo: [{
          action: Action.SET_HASH_CALCULATION,
          text: `Address = ${value} % ${this.rows.length} = ${prevAddress}`,
        }],
      });

      steps.push({
        do: [{
          action: Action.MOVE_POINTER,
          index: address,
        }],
        undo: [{
          action: Action.MOVE_POINTER,
          index: prevAddress,
        }],
      });
    }

    steps.push({
      do: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
      undo: [{
        action: Action.SET_POINTER,
        index: address,
      }],
    });

    if (found) {
      // found
      steps.push({
        do: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.BASE_COLOR_HASH_TABLE,
          text: `The value ${value} was found in the hash table at address ${address}.`,
        }],
        undo: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.HASH_INSERT_SEARCH_COMPLETED,
          text: `The value ${value} was found in the hash table at address ${address}.`,
        }],
      });
    } else {
      // not found
      steps.push({
        do: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.BASE_COLOR_HASH_TABLE,
          text: `The value ${value} was not fund in the hash table.`,
        }],
        undo: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.HASH_COLLISION,
          text: `The value ${value} was not fund in the hash table.`,
        }],
      });
    }

    return steps;
  }

  /**
   * Positions where an element has been deleted have to be marked with a flag to indicate an
   * uninterrupted collision path for other elements and searches.
   * @param {number} value
   * @return {{}[]}
   */
  remove(value) {
    const steps = [];

    if (this.rows.length === 0) {
      // Table not created
      steps.push({
        do: [{
          action: Action.NOOP,
          text: 'Create a new hash table to delete a value.',
        }],
        undo: [{
          action: Action.NOOP,
          text: 'Create a new hash table to delete a value.',
        }],
      });
      return steps;
    }

    let address = this.hash(value);

    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = ${value} % ${this.rows.length} = ${address}`,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: `Address = value % ${this.rows.length} = ?`,
      }],
    });

    steps.push({
      do: [{
        action: Action.SET_POINTER,
        index: address,
      }],
      undo: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
    });

    const visitedAddresses = [];
    let counter = 0;
    let found = false;
    while (!found) {
      if (this.rows[address].occupied && this.rows[address].value === value) {
        // found value, remove, done.
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_INSERT_SEARCH_COMPLETED,
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_VALUE,
            index: address,
            value: null,
          }],
          undo: [{
            action: Action.SET_VALUE,
            index: address,
            value,
          }],
        });
        this.rows[address].value = undefined;
        this.rows[address].occupied = false;
        this.rows[address].extraCheck = true;
        steps.push({
          do: [{
            action: Action.SET_FILL_FACTOR,
            value: getFillFactor(this.rows),
          }],
          undo: [{
            action: Action.SET_FILL_FACTOR,
            value: this.fillFactor,
          }],
        });
        this.fillFactor = getFillFactor(this.rows);
        found = true;
        break;
      }

      // empty or wrong value, and no indication to continue search
      if (this.rows[address].extraCheck === false) {
        steps.push({
          do: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.HASH_COLLISION,
            text: 'No extra check.',
          }],
          undo: [{
            action: Action.SET_COLOR,
            index: address,
            color: Color.BASE_COLOR_HASH_TABLE,
            text: 'No extra check.',
          }],
        });
        break;
      }

      // check if we already visited the current address:
      // If we arrive at the same place, we are going to go in a circle forever, so we stop.
      if (visitedAddresses.includes(address)) {
        steps.push({
          do: [{
            action: Action.NOOP,
            text: 'Searched all possible addresses in the hash table.',
          }],
          undo: [{
            action: Action.NOOP,
            text: 'Searched all possible addresses in the hash table.',
          }],
        });
        break;
      }

      // increase iteration counter
      counter += 1;
      if (counter === this.rows.length) {
        // we looked at the entire hash table and found nothing, stop.
        steps.push({
          do: [{
            action: Action.NOOP,
            text: 'Searched the entire hash table.',
          }],
          undo: [{
            action: Action.NOOP,
            text: 'Searched the entire hash table.',
          }],
        });
        break;
      }

      // add current address to visited addresses
      visitedAddresses.push(address);

      // needed for undo action
      const prevAddress = address;

      // the double hashing trick
      address = this.hash(value, address);

      steps.push({
        do: [{
          action: Action.SET_HASH_CALCULATION,
          text: `Address = (${prevAddress} + (${this.prime} - (${value} % ${this.prime}))) % ${this.rows.length} = ${address}`,
        }],
        undo: [{
          action: Action.SET_HASH_CALCULATION,
          text: `Address = ${value} % ${this.rows.length} = ${prevAddress}`,
        }],
      });

      steps.push({
        do: [{
          action: Action.MOVE_POINTER,
          index: address,
        }],
        undo: [{
          action: Action.MOVE_POINTER,
          index: prevAddress,
        }],
      });
    }

    steps.push({
      do: [{
        action: Action.UNSET_POINTER,
        index: address,
      }],
      undo: [{
        action: Action.SET_POINTER,
        index: address,
      }],
    });

    if (found) {
      // found
      steps.push({
        do: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.BASE_COLOR_HASH_TABLE,
          text: `The value ${value} was found and removed from the hash table at address ${address}.`,
        }],
        undo: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.HASH_INSERT_SEARCH_COMPLETED,
          text: `The value ${value} was found and removed from the hash table at address ${address}.`,
        }],
      });
    } else {
      // not found
      steps.push({
        do: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.BASE_COLOR_HASH_TABLE,
          text: `The value ${value} was not found in the hash table.`,
        }],
        undo: [{
          action: Action.SET_COLOR,
          index: address,
          color: Color.HASH_COLLISION,
          text: `The value ${value} was not fund in the hash table.`,
        }],
      });
    }

    return steps;
  }

  contains(value) {
    for (let i = 0; i < this.rows.length; i += 1) {
      if (this.rows[i].value === value) {
        return true;
      }
    }
    return false;
  }
}
