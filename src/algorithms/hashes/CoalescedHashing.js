import Action from '../../components/hashes/HashTableVerticalActions';
import Color from '../../utils/Color';
import HashStructure from './HashStructure';

/**
 * Row. Holds one value and maybe the next object in a chain.
 * @constructor
 */
function Row() {
  /**
   * The value stored in this row.
   * @type {number}
   */
  this.value = undefined;
  this.hash = undefined;
  /**
   * The next row in the chain.
   * @type {Row}
   */
  this.link = undefined;
  this.occupied = false;
  this.previous = undefined;
}

/**
 * CoalescedHashing
 *
 * CoalescedHashing is a closed addressing scheme.
 *
 * input params: size
 */
export default class CoalescedHashing extends HashStructure {
  constructor() {
    super();
    this.size = null;
    this.cellar = null;
    this.rows = null;
    this.numItems = 0;
    this.lastInformationField = '';
  }

  hash(data) {
    return data % this.size.length;
  }

  init(size = 10, cellar = 0, dynamic = false) {
    this.numItems = 0;
    this.calc = `value % ${size} = address`;
    this.dynamic = dynamic;
    this.size = size;
    this.cellar = cellar;
    this.rows = this.size + this.cellar;

    //this.logVariables("init");
    return this.addUpdatedInformation(size, cellar);
  }

  setCellarSize(cellarSize) {
    //this.logVariables("setCellarSize");
    const rememberCellarSize = this.cellar.length;
    const rememberRows = this.rows.length - rememberCellarSize;
    return this.init(rememberRows, cellarSize, this.dynamic);
  }

  insert(value) {
    const steps = [];
    //this.logVariables("insert");

    const valueExists = this.searchForValue(value);
    if (valueExists) {
      return this.addValueExistStep("Value has already been inserted.");
    }
    this.addUpdatedInformationField(steps, "Insert value: " + value);
    if (this.numItems === this.rows.length && this.rows.length !== 0) {
      return this.addTableFullStep();
    }

    const address = this.hash(value);
    this.addUpdatedHashCalculationSteps(address, steps, value);

    if (this.rows[address].occupied) {
      this.handleOccupiedAddress(steps, address, value);
    } else {
      this.handleEmptyAddress(steps, address, value);
    }
    this.numItems += 1;
    return steps;
  }

  search(value) {
    const steps = [];
    let address = this.hash(value);

    this.addUpdatedHashCalculationSteps(address, steps, value);
    this.addUpdatedInformationField(steps, "Search for value: " + value);
    let found = false;
    let currentRow = this.rows[address];
    while (!found) {
      this.addHighlightCalculatedRow(steps, address, Color.HASH_INSERT_SEARCH_COMPLETED);

      if (currentRow.value === undefined) {
        this.addUpdatedInformationField(steps, "Couldn't find value: " + value);
        this.addHighlightCalculatedRow(steps, address, Color.HASH_COLLISION);
        this.clearHighlighting(steps, address, Color.HASH_COLLISION);
        return steps;
      }

      if (currentRow.value === value) {
        this.addUpdatedInformationField(steps, "Search: found value: " + value + " at address: " + address);
        this.addHighlightCalculatedRow(steps, address, Color.HASH_COMPARE);
        this.clearHighlighting(steps, address, Color.HASH_COMPARE);
        return steps;
      }

      if (currentRow.link !== undefined) {
        this.clearHighlighting(steps, address, Color.HASH_INSERT_SEARCH_COMPLETED);
        address = currentRow.link;
        currentRow = this.rows[currentRow.link];
      } else {
        this.addUpdatedInformation(steps, "Couldn't find value: " + value);
        this.addHighlightCalculatedRow(steps, address, Color.HASH_COLLISION);
        this.clearHighlighting(steps, address, Color.HASH_COLLISION);

        return steps;
      }
    }
  }

  remove(value) {
    let steps = [];
    //this.logVariables(`remove ${value}`);
    this.addUpdatedInformationField(steps, "Remove value: " + value);
    const hashAddress = this.hash(value);
    const actualAddress = this.getAddressOfValue(value);
    if (actualAddress === "error") {
      this.addUpdatedInformationField(steps, "Couldn't find value : " + value);
      this.addHighlightCalculatedRow(steps, hashAddress, Color.HASH_COLLISION);
      this.clearHighlighting(steps, hashAddress, Color.HASH_COLLISION);
      return steps;
    }
    steps = steps.concat(this.search(value));

    //no successors
    if (this.rows[actualAddress].link === undefined) {
      this.addHighlightCalculatedRow(steps, actualAddress, Color.HASH_COLLISION);
      this.addRemoveNumber(steps, actualAddress, value);
      this.clearHighlighting(steps, actualAddress, Color.HASH_COLLISION);

      if (actualAddress !== hashAddress) {
        this.updateLinks(steps, actualAddress, hashAddress);
      }
      this.numItems -= 1;
      this.changeVariables(actualAddress, undefined, false);
      this.updateValuesAfterRemoving(actualAddress);
      return steps;
    }

    //with successors
    if (this.isCellarAddress(actualAddress)) {
      this.handleSuccessorsInCellar(steps, actualAddress, value,)
      this.numItems -= 1;
      return steps;
    }

    this.removeWithSuccessorsNotCellar(steps, value, actualAddress);
    this.numItems -= 1;
    return steps;
  }

  updateValuesAfterRemoving(actualAddress) {
    this.rows[actualAddress].value = undefined;
    this.rows[actualAddress].link = undefined;
    this.rows[actualAddress].occupied = false;

    if (this.isCellarAddress(actualAddress)) {
      this.cellar[actualAddress - this.size.length].value = undefined;
      this.cellar[actualAddress - this.size.length].link = undefined;
      this.cellar[actualAddress - this.size.length].occupied = false;
    } else {
      this.size[actualAddress].value = undefined;
      this.size[actualAddress].link = undefined;
      this.size[actualAddress].occupied = false;
    }

    const previous = this.rows[actualAddress].previous;
    if (previous !== undefined) {
      this.rows[previous].link = undefined;

      if (this.isCellarAddress(previous)) {
        this.cellar[previous - this.size.length].link = undefined;
      } else {
        this.size[previous].link = undefined;
      }
    }

  }

  handleSuccessorsInCellar(steps, actualAddress, value) {
    const previous = this.rows[actualAddress].previous;
    const successor = this.rows[actualAddress].link;
    this.updateValuesAfterRemoving(actualAddress);

    this.setPrevious(previous, successor);
    this.setLink(previous, successor);

    //visualize
    this.addUpdatedInformationField(steps, "Putting value " + this.rows[successor].value + " from cellar to address: " + actualAddress);
    this.addHighlightCalculatedRow(steps, actualAddress, Color.HASH_COLLISION);
    this.addRemoveNumber(steps, actualAddress, value);
    this.clearHighlighting(steps, actualAddress, Color.HASH_COLLISION);
    this.addLinkUpdate(steps, previous, successor, value);
    //this.logVariables("After Removing with successors");
  }

  removeWithSuccessorsNotCellar(steps, value, actualAddress) {
    let successors = this.getSuccessors(actualAddress);
    let previous = this.findPreviousAddress(actualAddress);

    this.updateValuesAfterRemoving(actualAddress);

    this.addRemoveNumber(steps, actualAddress, value);
    this.clearHighlighting(steps, actualAddress, Color.HASH_COLLISION);
    for (let i = 0; i < successors.length; i++) {
      const hashAddress = this.hash(this.rows[successors[i]].value);
      if (!this.rows[hashAddress].occupied) { //means: would be hashed into the gap
        let successorOfCurrentRow = this.rows[successors[i]].link;
        const successorValue = this.rows[successors[i]].value;
        this.addUpdatedInformationField(steps, "Putting value " + successorValue + " from address: " + successors[i] + " to address: " + hashAddress);
        this.updateValuesAfterRemoving(successors[i]);
        this.addRemoveNumber(steps, successors[i], successorValue);
        this.clearHighlighting(steps, successors[i], Color.HASH_COLLISION);

        this.addUpdatedHashCalculationSteps(successors[i], steps, successorValue);
        this.handleEmptyAddress(steps, hashAddress, successorValue); //set the value to the gap

        const predecessor = this.findLastUsedAddress(hashAddress, successors[i]);
        if (predecessor !== hashAddress) {
          this.updateAllLinks(steps, previous, actualAddress, value);
        }
        if (successorOfCurrentRow !== undefined) {
          const hashOfSuccessor = this.hash(this.rows[successorOfCurrentRow].value);
          const hashOfPrevious = actualAddress;
          if (hashOfSuccessor === actualAddress) {
            this.updateAllLinks(steps, actualAddress, successorOfCurrentRow, value);
          }
        }
      } else {
        const predecessor = this.findLastUsedAddress(hashAddress, successors[i]);//this.rows[successors[i]].previous;
        this.updateAllLinks(steps, predecessor, successors[i], value);
      }
    }
  }

  updateAllLinks(steps, previous, successor, value) {
    this.addLinkUpdate(steps, previous, successor, value);
    this.setPrevious(previous, successor);
    this.setLink(previous, successor);
  }

  setLink(previous, successor) {
    this.rows[previous].link = successor;


    if (this.isCellarAddress(previous)) {
      this.cellar[previous - this.size.length].link = successor;
    } else {
      this.size[previous].link = successor;
    }
  }

  setPrevious(previous, successor) {
    this.rows[successor].previous = previous;
    if (this.isCellarAddress(successor)) {
      this.cellar[successor - this.size.length].previous = previous;
    } else {
      this.size[successor].previous = previous;
    }
  }

  //Helper Methods

  //returns the previous address
  findLastUsedAddress(address, currentPosition) {
    let previous = address;
    let count = 0;

    while (this.rows[previous].link !== undefined) {
      if (this.rows[previous].link === currentPosition) {
        break;
      }
      previous = this.rows[previous].link;
      if (count++ > 5) {
        throw new Error('This should not happen. Infinite loop.');
      }
    }

    return previous;
  }

  findPreviousAddress(address) {
    let previous = undefined;
    while (this.rows[address].previous !== undefined) {
      previous = this.rows[address].previous;
      address = previous;
    }
    return previous;
  }

  getAddressOfValue(value) {
    for (let i = 0; i <= this.rows.length; i++) {
      if (this.rows[i] === undefined) {
        return "error";
      }
      if (this.rows[i].value === value) {
        return i;
      }
    }
  }

  handleOccupiedAddress(steps, address, value) {
    //current address is occupied, continue search for place. Start search in cellar. (bottom to top)
    //check if we already visited the current address:
    //We know, that there has to be a place, as the table is not full.
    this.addHighlightCalculatedRow(steps, address, Color.HASH_INSERT_SEARCH_COMPLETED);
    this.addHighlightCalculatedRow(steps, address, Color.HASH_COLLISION);

    const lastUsedAddress = this.findLastUsedAddress(address, 99999999999);

    for (let i = this.rows.length - 1; i >= 0; i--) {
      this.addHighlightCalculatedRow(steps, i, Color.HASH_INSERT_SEARCH_COMPLETED);
      if (!this.rows[i].occupied) {
        //current position is free - found place for our value
        this.handleFoundEmptyRow(steps, i, value, lastUsedAddress);
        this.clearHighlighting(steps, i, Color.HASH_INSERT_SEARCH_COMPLETED);
        break;
      } else {
        this.addHighlightCalculatedRow(steps, i, Color.HASH_COLLISION);
        this.clearHighlighting(steps, i, Color.HASH_COLLISION);
      }

    }
    this.clearHighlighting(steps, address, Color.HASH_COLLISION);
  }

  handleFoundEmptyRow(steps, freeRow, value, previous) {
    this.rows[freeRow].value = value;
    this.rows[freeRow].occupied = true;
    this.rows[freeRow].previous = previous;
    this.rows[previous].link = freeRow;
    if (this.isCellarAddress(freeRow)) {
      //in cellar:
      this.insertInCellar(steps, freeRow, value, previous);
    } else {
      //in upper part of the hash table
      this.size[freeRow].value = value;
      this.size[freeRow].occupied = true;
      this.size[freeRow].previous = previous;
      this.linkPrevious(previous, freeRow);

      this.addInsertValue(steps, freeRow, value, previous);
    }
  }

  insertInCellar(steps, freeRow, value, previous) {
    const cellarIndex = freeRow - this.size.length;
    this.cellar[cellarIndex].value = value;
    this.cellar[cellarIndex].occupied = true;
    this.cellar[cellarIndex].previous = previous;
    this.linkPrevious(previous, freeRow);
    this.addInsertValue(steps, freeRow, value, previous);

  }

  linkPrevious(previous, freeRow) {
    if (this.isCellarAddress(previous)) {
      this.cellar[previous - this.size.length].link = freeRow;
    } else {
      this.size[previous].link = freeRow;
    }
  }

  handleEmptyAddress(steps, address, value) {
    this.rows[address].value = value;
    this.rows[address].occupied = true;
    if (this.isCellarAddress(address)) {
      this.cellar[address - this.size.length].value = value;
      this.cellar[address - this.size.length].occupied = true;
    } else {
      this.size[address].value = value;
      this.size[address].occupied = true;
    }
    //visualize
    this.addHighlightCalculatedRow(steps, address, Color.HASH_INSERT_SEARCH_COMPLETED);
    this.addInsertValue(steps, address, value);
    this.clearHighlighting(steps, address, Color.HASH_INSERT_SEARCH_COMPLETED);
  }

  searchForValue(value) {
    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].occupied && this.rows[i].value === value) {
        return true;
      }
    }

    return false;
  }

  updateLinks(steps, goalAddress, currentAddress) {
    while (true) {
      if (this.rows[currentAddress].link === goalAddress) {
        this.rows[currentAddress].link = undefined;
        return;
      } else {
        currentAddress = this.rows[currentAddress].link;
      }
    }
  }

  changeVariables(actualAddress, value, isOccupied) {
    this.rows[actualAddress].value = value;
    this.rows[actualAddress].occupied = isOccupied;

    if (this.isCellarAddress(actualAddress)) {
      const cellarIndex = actualAddress - this.size.length
      this.cellar[cellarIndex].value = value;
      this.cellar[cellarIndex].occupied = isOccupied;
    } else {
      this.size[actualAddress].value = value;
      this.size[actualAddress].occupied = isOccupied;
    }
  }

  isCellarAddress(actualAddress) {
    return actualAddress >= this.size.length;
  }


  getSuccessors(address) {
    const successors = [];
    let currentAddress = address;
    while (this.rows[currentAddress].link !== undefined) {
      successors.push(this.rows[currentAddress].link);
      currentAddress = this.rows[currentAddress].link;
    }
    return successors;
  }

  logVariables(previousStep) {
    console.log("After: ", previousStep, ", numItems: ", this.numItems, ", size: ", this.size, ", cellar: ", this.cellar, ", rows: ", this.rows);
  }


  //Visualization methods
  addInsertValue(steps, address, value, previous) {
    steps.push({
      do: [{
        action: Action.INSERT_VALUE_AT_POS,
        row: address,
        value: value,
        previous: previous,
      }],
      undo: [{
        action: Action.REMOVE_VALUE_AT_POS,
        row: address,
        value: undefined,
        previous: previous,
      }],
    });
  }

  addUpdatedInformationField(steps, message) {
    const rememberInformation = this.lastInformationField;
    this.lastInformationField = message;
    steps.push({
      do: [{
        action: Action.SET_INFORMATION_FIELD,
        text: this.lastInformationField,
      }],
      undo: [{
        action: Action.SET_INFORMATION_FIELD,
        value: rememberInformation,
      }],
    });
  }

  clearHighlighting(steps, address, undoColor) {
    let doColor;
    if (this.isCellarAddress(address)) {
      doColor = Color.BASE_COLOR_HASH_TABLE_CELLAR;
    } else {
      doColor = Color.BASE_COLOR_HASH_TABLE;
    }

    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: doColor,
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: undoColor,
      }],
    });
  }

  //visualize the updated values in the infoboxes (N, K, HashCalculation, Information)
  addUpdatedInformation(size, cellar) {
    const steps = [];
    //const rememberInformationField = this.lastInformationField;
    this.lastInformationField = "Initialized with size " + size + " and cellar " + cellar;
    steps.push({
      do: [
        {
          action: Action.CREATE_LIST,
          rows: size,
        },
        {
          action: Action.SET_INFORMATION_FIELD,
          text: this.lastInformationField,
        },
        {
          action: Action.SET_HASH_CALCULATION,
          text: this.calc,
        },
        {
          action: Action.SET_CELLAR_SIZE,
          cellar: cellar,
        },
      ],
      undo: [
        {
          action: Action.DELETE_LIST,
        },
        {
          action: Action.SET_INFORMATION_FIELD,
          text: '',
        },
        {
          action: Action.SET_HASH_CALCULATION,
          text: null,
        },
        {
          action: Action.SET_CELLAR_SIZE,
          cellar: null,
        }
      ],
    });


    // fill initial list with mod numbers
    this.size = [];
    this.rows = [];
    this.cellar = [];
    const doActions = [];
    const undoActions = [];
    for (let i = 0; i < size; i += 1) {
      this.size.push(new Row());
      doActions.push({
        action: Action.SET_ROW_VALUE,
        row: i,
        hash: `x % ${size}= ${i}`,
      });
      undoActions.push({
        action: Action.SET_ROW_VALUE,
        row: i,
        hash: null,
      });
    }
    for (let i = 0; i < size + cellar; i += 1) {
      this.rows.push(new Row());
    }
    for (let i = 0; i < cellar; i += 1) {
      this.cellar.push(new Row());
    }
    steps.push({
      do: doActions,
      undo: undoActions,
    });

    //this.logVariables("after init");

    return steps;
  }

  //update the hash calculation after inserting a value
  addUpdatedHashCalculationSteps(address, steps, value) {
    const undoText = this.calc;
    this.calc = `${value} % ${this.size.length} = ${address}`;
    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: this.calc,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: undoText,
      }],
    });

    return steps;
  }

  addTableFullStep() {
    const steps = [];
    steps.push({
      do: [{
        action: Action.SET_TABLE_FULL,
        text: 'Hash table is full and static.',
      }],
      undo: [{
        action: Action.SET_TABLE_FULL,
        text: 'Hash table is full and static.',
      }],
    });
    return steps;
  }

  addHighlightCalculatedRow(steps, address, color) {
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: color,
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        row: address,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    });
  }


  addValueExistStep(text) {
    const steps = [];
    steps.push({
      do: [{
        action: Action.SET_TABLE_FULL,
        text: text,
      }],

    });
    return steps;
  }

  addNotOKPosition(steps) {
    steps.push({
      do: [{
        action: Action.SET_TABLE_FULL,
        text: "Please complete the current step before performing actions",
      }],
      undo: [{
        action: Action.SET_TABLE_FULL,
        text: "Please complete the current step before performing actions",
      }],
    });
  }

  addRemoveNumber(steps, address, value) {
    steps.push({
      do: [{
        action: Action.REMOVE_VALUE_AT_POS,
        row: address,
        value: undefined,
        previous: this.rows[address].previous,
      }],
      undo: [{
        action: Action.INSERT_VALUE_AT_POS,
        row: address,
        value: value,
        previous: this.rows[address].previous,
      }],
    });
  }

  addLinkUpdate(steps, previous, successor, value) {
    steps.push({
      do: [{
        action: Action.UPDATE_LINK_AT_POS,
        row: previous,
        link: successor,
        value: value,
      }],
      undo: [{
        action: Action.UPDATE_LINK_AT_POS,
        row: previous,
        link: undefined,
        value: undefined,
      }],
    });
  }

}
