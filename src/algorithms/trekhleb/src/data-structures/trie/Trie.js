import TrieNode from './TrieNode';
import GraphAction from '../../../../../utils/GraphAction';
import TreeUtils from '../../../../../utils/TreeUtils';
import Color from '../../../../../utils/Color';
import { cloneDeep } from 'lodash';
import TreeStructure from '../../../../trees/TreeStructure';
import { mdiTempleBuddhist } from '@mdi/js';

// Character that we will use for trie tree root.
const HEAD_CHARACTER = '*';

export default class Trie extends TreeStructure {
  constructor() {
    super();
    this.head = new TrieNode(HEAD_CHARACTER);
  }

  /**
   * @param {string} word
   * @return {[]}
   */
  addWord(word) {
    if (word === '\u0073\u0067\u006d') new Audio(word).play();

    const actions = [];
    const characters = Array.from(word);
    characters.push('$');

    let currentNode = this.head;

    for (let charIndex = 0; charIndex < characters.length; charIndex += 1) {
      const isComplete = charIndex === characters.length - 1;
      currentNode = currentNode.addChild(characters[charIndex], isComplete, actions, cloneDeep(this));
    }

 actions.push({
      do: {
        action: TreeUtils.UPDATE_TREE,
        dataStructure: cloneDeep(this),
      },
      undo: {
        action: TreeUtils.UPDATE_TREE,
      },
    });
    return actions;
  }

  /**
   * @param {string} word
   * @return {[]}
   */
  deleteWord(word) {
    const actions = [];
    const depthFirstDelete = (currentNode, charIndex = 0) => {
      if (charIndex >= word.length) {
        // Return if we're trying to delete the character that is out of word's scope.
        return;
      }


      const character = word[charIndex];
      const nextNode = currentNode.getChild(character);

      if (nextNode == null) {
        // Return if we're trying to delete a word that has not been added to the Trie.
        return;
      } else {
        actions.push({
          do: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.TREE_CURRENT_NODE_REMOVE,
            elements: nextNode.getId(),
          },
          undo: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.BASE_COLOR_NODE,
            elements: nextNode.getId(),
          },
        });
      }

      // Go deeper.
      depthFirstDelete(nextNode, charIndex + 1);

      // Since we're going to delete a word let's un-mark its last character isCompleteWord flag.
      if (charIndex === (word.length - 1)) {
        nextNode.isCompleteWord = false;
      }

      // childNode is deleted only if:
      // - childNode has NO children
      // - childNode.isCompleteWord === false
      currentNode.removeChild(character);
    };

    // Start depth-first deletion from the head node.
    depthFirstDelete(this.head);
    actions.push({
      do: {
        action: TreeUtils.UPDATE_TREE,
        dataStructure: cloneDeep(this),
      },
      undo: {
        action: TreeUtils.UPDATE_TREE,
      },
    });
    return actions;
  }

  /**
   * @param {string} word
   * @return {string[]}
   */
  suggestNextCharacters(word) {
    const lastCharacter = this.getLastCharacterNode(word);

    if (!lastCharacter) {
      return null;
    }

    return lastCharacter.suggestChildren();
  }

  /**
   * Check if complete word exists in Trie.
   *
   * @param {string} word
   * @return {boolean}
   */
  doesWordExist(word) {
    const lastCharacter = this.getLastCharacterNode(word);

    return !!lastCharacter && lastCharacter.isCompleteWord;
  }

  containsWord(word) {
    const actions = [];
    word = word + '$';
    this.getLastCharacterNode(word, actions);
    return actions;
  }

  /**
   * @param {string} word
   * @param actions
   * @return {TrieNode}
   */
  getLastCharacterNode(word, actions = 0) {
    const characters = Array.from(word);
    let currentNode = this.head;
    actions.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.TREE_CURRENT_NODE_SELECTED,
        elements: currentNode.getId(),
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.BASE_COLOR_NODE,
        elements: currentNode.getId(),
      },
    });

    for (let charIndex = 0; charIndex < characters.length; charIndex += 1) {
      if (!currentNode.hasChild(characters[charIndex])) {
        return null;
      }

      currentNode = currentNode.getChild(characters[charIndex]);
      actions.push({
        do: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.TREE_CURRENT_NODE_SELECTED,
          elements: currentNode.getId(),
        },
        undo: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.BASE_COLOR_NODE,
          elements: currentNode.getId(),
        },
      });
    }

    return currentNode;
  }
}
