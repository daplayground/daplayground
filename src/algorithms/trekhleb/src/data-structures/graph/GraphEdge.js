export default class GraphEdge {
  /**
   * @param {GraphVertex} startVertex
   * @param {GraphVertex} endVertex
   * @param {number} [weight=1]
   */
  constructor(startVertex, endVertex, weight = 0) {
    this.source = startVertex;
    this.target = endVertex;
    this.weight = weight;
  }

  /**
   * @return {string}
   */
  getKey() {
    const startVertexKey = this.source.getKey();
    const endVertexKey = this.target.getKey();

    return `${startVertexKey}_${endVertexKey}`;
  }

  /**
   * @return {GraphEdge}
   */
  reverse() {
    const tmp = this.source;
    this.source = this.target;
    this.target = tmp;

    return this;
  }

  /**
   * change weight value
   */
  setWeight(value) {
    this.weight = value;
  }

  /**
   * @return {string}
   */
  toString() {
    return this.getKey();
  }
}
