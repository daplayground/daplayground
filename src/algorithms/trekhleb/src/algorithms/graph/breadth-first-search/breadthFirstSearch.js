import Queue from '../../../data-structures/queue/Queue';
import GraphAction from '../../../../../../utils/GraphAction';
import Color from '../../../../../../utils/Color'; 

/**
 * @typedef {Object} Callbacks
 *
 * @property {function(vertices: Object): boolean} [allowTraversal] -
 *   Determines whether DFS should traverse from the vertex to its neighbor
 *   (along the edge). By default prohibits visiting the same vertex again.
 *
 * @property {function(vertices: Object)} [enterVertex] - Called when BFS enters the vertex.
 *
 * @property {function(vertices: Object)} [leaveVertex] - Called when BFS leaves the vertex.
 */

/**
 * @param {Callbacks} [callbacks]
 * @returns {Callbacks}
 */
function initCallbacks(callbacks = {}) {
  const initiatedCallback = callbacks;

  const stubCallback = () => {};

  const allowTraversalCallback = (
    () => {
      const seen = {};
      return ({ nextVertex }) => {
        if (!seen[nextVertex.getKey()]) {
          seen[nextVertex.getKey()] = true;
          return true;
        }
        return false;
      };
    }
  )();

  initiatedCallback.allowTraversal = callbacks.allowTraversal || allowTraversalCallback;
  initiatedCallback.enterVertex = callbacks.enterVertex || stubCallback;
  initiatedCallback.leaveVertex = callbacks.leaveVertex || stubCallback;

  return initiatedCallback;
}

/**
 * @param {Graph} graph
 * @param {GraphVertex} startVertex
 * @param {Callbacks} [originalCallbacks]
 */
export default function breadthFirstSearch(graph, startVertex, originalCallbacks) {
  const callbacks = initCallbacks(originalCallbacks);
  const vertexQueue = new Queue();
  const actions = [];
  const visited = [];
  // Do initial queue setup.
  vertexQueue.enqueue(startVertex);

  actions.push([{
    do: {
      action: GraphAction.HIGHLIGHT_NODE,
      color: Color.GRAPH_HIGHLIGHTED_NODE,
      elements: startVertex.getKey(),
    },
    undo: {
      action: GraphAction.HIGHLIGHT_NODE,
      color: Color.BASE_COLOR_NODE,
      elements: startVertex.getKey(),
      text:'LAST HIGHLIGHT',
    },
  }]);

  let previousVertex = null;

  // Traverse all vertices from the queue.
  while (!vertexQueue.isEmpty()) {
    const action = [];
    const currentVertex = vertexQueue.dequeue();

    action.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.GRAPH_RESULT_TRUE,
        elements: currentVertex.getKey(),
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.GRAPH_HIGHLIGHTED_NODE,
        elements: currentVertex.getKey(),
        text: 'Highlight in rot',
      },
    });

    visited.push(currentVertex);


    callbacks.enterVertex({ currentVertex, previousVertex });

    // Add all neighbors to the queue for future traversals.
    // eslint-disable-next-line no-loop-func
    graph.getNeighbors(currentVertex).forEach((nextVertex) => {
      if (callbacks.allowTraversal({ previousVertex, currentVertex, nextVertex })) {
        vertexQueue.enqueue(nextVertex);

        if ((visited.find((item) => item.getKey() === nextVertex.getKey())) === undefined) {
          action.push({
            do: {
              action: GraphAction.HIGHLIGHT_NODE,
              color: Color.GRAPH_HIGHLIGHTED_NODE,
              elements: nextVertex.getKey(),
            },
            undo: {
              action: GraphAction.HIGHLIGHT_NODE,
              color: Color.BASE_COLOR_NODE,
              elements: nextVertex.getKey(),
              text: 'Highlight in türkis',
            },
          });
        }
      }
    });

    callbacks.leaveVertex({ currentVertex, previousVertex });
    actions.push(action);
    // Memorize current vertex before next loop.
    previousVertex = currentVertex;
  }
  return actions;
}
