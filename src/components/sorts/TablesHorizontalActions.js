/**
 * Actions supported by TablesHorizontal
 */
export default {
  CLEAR: 'clear',
  CREATE_TABLE: 'createTable',
  DELETE_TABLE: 'deleteTable',
  SET_ROW_COLOR: 'setRowColor',
  SET_COLUMN_COLOR: 'setColumnColor',
  SET_CELL_COLOR: 'setCellColor',
  SET_ROW_VALUE: 'setRowValue',
  SET_COLUMN_VALUE: 'setColumnValue',
  SET_CELL_VALUE: 'setCellValue',
  SET_ROW_STROKE: 'setRowStroke',
  SET_COLUMN_STROKE: 'setColumnStroke',
  SET_CELL_STROKE: 'setCellStroke',
};
