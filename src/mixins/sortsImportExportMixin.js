import EventBus from '../services/EventBus';
import ValueType from '../utils/ValueType';

export default {
  mounted() {
    EventBus.$on('loadValues', this.loadValues);
  },
  beforeDestroy() {
    EventBus.$off('loadValues', this.loadValues);
  },
  created() {
    this.$store.commit('setValueType', ValueType.INTEGERS);
  },
  methods: {
    loadValues(valueWrapper) {
      if (valueWrapper.type === ValueType.INTEGERS) {
        EventBus.$emit('valuesLoaded', valueWrapper.values);
      }
    },
  },
  watch: {
    values(to) {
      this.$store.commit('setValueList', to);
    },
  },
};
