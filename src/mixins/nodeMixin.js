import * as d3 from 'd3';
import Color from '../utils/Color';

export default {
  methods: {
    highlightNode(element, color) {
      d3.select(`#${element}`).select('circle').style('fill', color);
    },
    highlightLink(element, color) {
      d3.select(`#${element}`).selectAll('line').style('stroke', color);
    },
    clearNodes() {
      d3.selectAll('.nodes').select('circle').style('fill', Color.BASE_COLOR_NODE);
    },
    clearLinks() {
      d3.selectAll('.links').select('line').style('stroke', 'black');
    },
  },
};
