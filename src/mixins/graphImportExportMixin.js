import ValueType from '../utils/ValueType';
import EventBus from '../services/EventBus';
import Graph from '../algorithms/trekhleb/src/data-structures/graph/Graph';
import ValueWrapper from '../utils/ValueWrapper';

export default {
  data() {
    return {
      componentKey: 0,
    };
  },
  mounted() {
    EventBus.$on('loadValues', this.loadValues);
    EventBus.$on('setCurrentValues', this.setCurrentValues);
  },
  beforeDestroy() {
    EventBus.$off('loadValues', this.loadValues);
    EventBus.$off('setCurrentValues', this.setCurrentValues);
  },
  methods: {
    setCurrentValues() {
      const valueWrapper = new ValueWrapper();
      valueWrapper.setType(ValueType.MATRIX);
      valueWrapper.setK(0);
      valueWrapper.setValues(this.graph.getAdjacencyMatrix());
      this.$store.commit('setCurrentValues', valueWrapper);
    },
    setNewGraph(matrix) {
      this.graph = new Graph();
      this.graph.loadMatrix(matrix);
    },
    loadValues(valueWrapper) {
      if (valueWrapper.type === ValueType.MATRIX) {
        const matrix = valueWrapper.values;
        this.setNewGraph(matrix);
        this.$store.commit('setValueList', [...valueWrapper.values]);
      }
    },
  },
  watch: {
    graph(val) {
      // force child component to reload
      this.componentKey += 1;
    },
  },
};
