import Vue from 'vue';
import VueRouter from 'vue-router';

import appRoutes from './appRoutes';
import graphsRoutes from './graphsRoutes';
import sortsRoutes from './sortsRoutes';
import hashingRoutes from './hashingRoutes';
import treesRoutes from './treesRoutes';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    ...appRoutes,
    ...graphsRoutes,
    ...sortsRoutes,
    ...hashingRoutes,
    ...treesRoutes,
  ],
});
