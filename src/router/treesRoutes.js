/**
 * Routes for Tree Algorithms
 */
import BinarySearchTree from '../views/trees/BinarySearchTree.vue';
import PrefixTree from '../views/trees/Trie.vue';
import BPlusTree from '../views/trees/BPlusTree.vue';
import TwoThreeFourTree from '../views/trees/TwoThreeFourTree.vue';
import MinHeap from '../views/trees/MinHeap.vue';
import MaxHeap from '../views/trees/MaxHeap.vue';
import BPlusTreeWebAD from '../views/trees/BPlusTreeWebAD.vue';
import infoMaterialTrees from '../utils/infoMaterialTrees';

export default [
  {
    path: '/trees/binary-search-tree',
    name: 'Binary Search Tree',
    component: BinarySearchTree,
    meta: {
      infoMaterial: infoMaterialTrees.BinarySearchTree,
      group: 'Trees',
    },
  },
  // {
  //   path: '/trees/b+tree',
  //   name: 'B+Tree',
  //   component: BPlusTree,
  //   meta : {
  //     infoMaterial: infoMaterialTrees.BPlusTree,
  //     group: 'Trees',
  //   }
  // },
  {
    path: '/trees/b+treeWebAD',
    name: 'B+Tree',
    component: BPlusTreeWebAD,
    meta: {
      infoMaterial: infoMaterialTrees.BPlusTreeWebAD,
      group: 'Trees',
    },

  },
  {
    path: '/trees/2-3-4-tree',
    name: '2-3-4 Tree',
    component: TwoThreeFourTree,
    meta: {
      infoMaterial: infoMaterialTrees.TwoThreeFourTree,
      group: 'Trees',
    },
  },
  {
    path: '/trees/min-heap',
    name: 'Min Heap',
    component: MinHeap,
    meta: {
      infoMaterial: infoMaterialTrees.MinHeap,
      group: 'Trees',
    },
  },
  {
    path: '/trees/max-heap',
    name: 'Max Heap',
    component: MaxHeap,
    meta: {
      infoMaterial: infoMaterialTrees.MaxHeap,
      group: 'Trees',
    },
  },
  {
    path: '/trees/prefix-tree',
    name: 'Prefix Tree',
    component: PrefixTree,
    meta: {
      infoMaterial: infoMaterialTrees.PrefixTree,
      group: 'Trees',
    },
  },
];
