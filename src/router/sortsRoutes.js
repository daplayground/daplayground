/**
 * Routes for Sorting Algorithms
 */
import BogoSort from '../views/sorts/BogoSort.vue';
import BubbleSort from '../views/sorts/BubbleSort.vue';
import BucketSort from '../views/sorts/BucketSort.vue';
import CountingSort from '../views/sorts/CountingSort.vue';
import MergeSort from '../views/sorts/MergeSort.vue';
import QuickSort from '../views/sorts/QuickSort.vue';
import BinaryQuickSort from '../views/sorts/BinaryQuickSort.vue';
import RadixSort from '../views/sorts/RadixSort.vue';
import SelectionSort from '../views/sorts/SelectionSort.vue';
import infoMaterialSorts from '../utils/infoMaterialSorts';

export default [
  {
    path: '/sorting-algorithms/bogo-sort',
    name: 'Bogo Sort',
    component: BogoSort,
    meta: {
      infoMaterial: infoMaterialSorts.BogoSort,
      group: 'Sorting Algorithms',
    },
  },
  {
    path: '/sorting-algorithms/bubble-sort',
    name: 'Bubble Sort',
    component: BubbleSort,
    meta: {
      infoMaterial: infoMaterialSorts.BubbleSort,
      group: 'Sorting Algorithms',
    },
  },
  {
    path: '/sorting-algorithms/bucket-sort',
    name: 'Bucket Sort',
    component: BucketSort,
    meta: {
      infoMaterial: infoMaterialSorts.BucketSort,
      group: 'Sorting Algorithms',
    },
  },
  {
    path: '/sorting-algorithms/counting-sort',
    name: 'Counting Sort',
    component: CountingSort,
    meta: {
      infoMaterial: infoMaterialSorts.CountingSort,
      group: 'Sorting Algorithms',
    },
  },
  // {
  //   path: '/sorting-algorithms/merge-sort',
  //   name: 'Merge Sort',
  //   component: MergeSort,
  //   meta: {
  //     infoMaterial: infoMaterialSorts.MergeSort,
  //     group: 'Sorting Algorithms',
  //   }
  // },
  {
    path: '/sorting-algorithms/quick-sort',
    name: 'Quicksort',
    component: QuickSort,
    meta: {
      infoMaterial: infoMaterialSorts.QuickSort,
      group: 'Sorting Algorithms',
    },
  },
  {
    path: '/sorting-algorithms/binary-quick-sort',
    name: 'Binary Quicksort',
    component: BinaryQuickSort,
    meta: {
      infoMaterial: infoMaterialSorts.BinaryQuickSort,
      group: 'Sorting Algorithms',
    },
  },
  {
    path: '/sorting-algorithms/radix-sort',
    name: 'Radix Sort (LSD)',
    component: RadixSort,
    meta: {
      infoMaterial: infoMaterialSorts.RadixSort,
      group: 'Sorting Algorithms',
    },
  },
  {
    path: '/sorting-algorithms/selection-sort',
    name: 'Selection Sort',
    component: SelectionSort,
    meta: {
      infoMaterial: infoMaterialSorts.SelectionSort,
      group: 'Sorting Algorithms',
    },
  },
];
