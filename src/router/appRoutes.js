import Home from '../views/Home.vue';

/**
 * Routes for general views
 */
export default [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
];
