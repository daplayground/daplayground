/**
 * Routes for Graph Algorithms
 */
import BreadthFirstSearch from '../views/graphs/BreadthFirstSearch.vue';
import DepthFirstSearch from '../views/graphs/DepthFirstSearch.vue';
import Kruskal from '../views/graphs/Kruskal.vue';
import Prim from '../views/graphs/Prim.vue';
import Dijkstra from '../views/graphs/Dijkstra.vue';
import TopologicalSort from '../views/graphs/TopologicalSort.vue';
import infoMaterialGraphs from '../utils/infoMaterialGraphs';

export default [
  {
    path: '/graph-algorithms/breadth-first-search',
    name: 'Breadth-first search (BFS)',
    component: BreadthFirstSearch,
    meta: {
      infoMaterial: infoMaterialGraphs.BreadthFirstSearch,
      group: 'Graph Algorithms',
    },
  },
  {
    path: '/graph-algorithms/depth-first-search',
    name: 'Depth-first search (DFS)',
    component: DepthFirstSearch,
    meta: {
      infoMaterial: infoMaterialGraphs.DepthFirstSearch,
      group: 'Graph Algorithms',
    },
  },
  {
    path: '/graph-algorithms/kruskals-algorithm',
    name: 'Kruskal\'s Algorithm (Minimum Spanning Tree)',
    component: Kruskal,
    meta: {
      infoMaterial: infoMaterialGraphs.Kruskal,
      group: 'Graph Algorithms',
    },
  },
  {
    path: '/graph-algorithms/prims-algorithm',
    name: 'Prim\'s Algorithm (Minimum Spanning Tree)',
    component: Prim,
    meta: {
      infoMaterial: infoMaterialGraphs.Prim,
      group: 'Graph Algorithms',
    },
  },
  {
    path: '/graph-algorithms/dijkstras-algorithm',
    name: 'Dijkstra\'s Algorithm (Shortest Path Problem)',
    component: Dijkstra,
    meta: {
      infoMaterial: infoMaterialGraphs.Dijkstra,
      group: 'Graph Algorithms',
    },
  },
  {
    path: '/graph-algorithms/topological-sort',
    name: 'Topological Sort (DFS)',
    component: TopologicalSort,
    meta: {
      infoMaterial: infoMaterialGraphs.TopologicalSort,
      group: 'Graph Algorithms',
    },
  },
];
