const { Linter } = require("eslint");

const onlyProd = process.env.NODE_ENV === 'production' ? 'error' : 'off';

module.exports = {
  root: true,
  env: {
    node: true,
    jest: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': onlyProd,
    'no-debugger': onlyProd,
    'no-unused-vars': onlyProd,
    'no-unreachable': onlyProd,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  overrides: [
    {
      files: [
        '**/tests/unit/**/*.test.js',
        '**/*.test.js',
      ],
      env: {
        jest: true,
      },
    },
    {
      files: ['src/mixins/controlHandlers.js'],
      rules: {
        'max-len': ['error', { code: 180 }],
      },
    },
  ],
  ignorePatterns: ['src/algorithms/trekhleb', 
  'src/algorithms/trees/BPlusWebAD1.js', 
  'src/algorithms/trees/BPlusNodeWebAD.js', 
  'src/algorithms/unused',
  'src/algorithms/trees/BPlusNodeWebAD.js',
  'src/algorithms/trees/BPlusTreeWebAD.test.js',

],
};
